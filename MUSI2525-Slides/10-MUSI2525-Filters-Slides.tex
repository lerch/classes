% move all configuration stuff into one file so we can focus on the content
\input{common}


\subtitle{Part 10: Filters}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
    % generate title page
	\input{titlepage}


    \section[intro]{introduction}
        \begin{frame}{filters}{introduction}
            \begin{block}{filter --- broad description}
                system that amplifies or attenuates certain components/aspects of a signal
            \end{block}
            \pause
            \bigskip
            \begin{block}{filter --- narrow}
                linear time-invariant system for changing the magnitude and phases of specific frequency regions            
            \end{block}
            \pause
            \bigskip
            \begin{itemize}
                \item   example for other filter:
                \item[]   adaptive (time-variant) (e.g., denoising)
            \end{itemize}
        \end{frame}
        \begin{frame}{filters}{applications around audio}
            \vspace{-2mm}
            \begin{itemize}
                \item   \textbf{audio equalizing}: changing the sound quality
                    \begin{itemize}
                        \item   parametric EQs
                        \item   graphic EQs
                        \item   fixed transfer functions
                    \end{itemize}
                \smallskip
                \item<2->   \textbf{removal} of unwanted components
                    \begin{itemize}
                        \item   remove DC, rumble
                        \item   remove hum
                        \item   remove hiss
                    \end{itemize}
                \smallskip
                \item<3->   \textbf{emphasis/de-emphasis}
                    \begin{itemize}
                        \item   Vinyl
                        \item   old Dolby noise reduction systems
                    \end{itemize}
                \smallskip
                \item<4->   \textbf{weighting} function
                    \begin{itemize}
                        \item   dBA, dBC, etc
                    \end{itemize}
                \smallskip
                \item<5->   (parameter) \textbf{smoothing}
                    \begin{itemize}
                        \item   smooth sudden parameter changes
                    \end{itemize}
            \end{itemize}
        \end{frame}
        \begin{frame}{filters}{reminder: system theory}
            \begin{itemize}
                \item output of a system (filter) $y$ computed by \textbf{convolution} of input $x$ and impulse response $h$
                    \begin{equation*}
                        y(t) = x(t) \ast h(t)   
                    \end{equation*}
                \bigskip
                \item<2-> this is equivalent to a frequency domain multiplication 
                    \begin{eqnarray*}
                        Y(\jom) &=& X(\jom) \cdot H(\jom)\\
                        H(\jom) &=& \frac{Y(\jom)}{X(\jom)}
                    \end{eqnarray*}
                \bigskip
                \item<3-> \textbf{transfer function} $H(\jom)$ is complex, often represented as 
                    \begin{itemize}
                        \item \textbf{magnitude} $|H(\jom)|$ and 
                        \item phase $\Phi_\mathrm{H}(\jom)$
                    \end{itemize}
            \end{itemize}
        \end{frame}

    \section{transfer function shapes}
        \begin{frame}{transfer function}{basic magnitude shapes}
            \vspace{-2mm}
            \question{what are typical filters/spectral filter shapes}

            \begin{columns}
            \column{.4\linewidth}
            \begin{itemize}
                \item   very common:
                    \begin{itemize}
                        \item   low/high pass
                    \end{itemize}
                \smallskip
                \item<3->   common in non-audio apps:
                    \begin{itemize}
                        \item   band pass/band stop
                    \end{itemize}
                \smallskip
                \item<4->    common in audio apps:
                    \begin{itemize}
                        \item<4->   low/high shelving
                        \item<5->   peak filter
                        \item<6->   resonance/notch
                    \end{itemize}
            \end{itemize}
            \column{.65\linewidth}
                \vspace{-3mm}
                \only<2-3>{
                    \begin{figure}%
                        \includegraphics[width=.8\columnwidth]{generalfilters_overview}%
                    \end{figure}
                }
                \only<4>{
                    \begin{figure}%
                        \includegraphics[width=.4\columnwidth]{filter_shelving}%
                    \end{figure}
                }
                \only<5>{
                    \begin{figure}%
                        \includegraphics[width=.8\columnwidth]{filter_peak}%
                    \end{figure}
                }
                \only<6>{
                    \begin{figure}%
                        \includegraphics[width=.4\columnwidth]{filter_notch}%
                    \end{figure}
                }
            \end{columns}
        \end{frame}

        \begin{frame}{filter usage}{filter bank}
            \only<1>{
            \begin{figure}%
                \includegraphics[width=.6\columnwidth]{filterbank}%
            \end{figure}
            }
            \only<2>{
            \begin{figure}%
                \includegraphics[width=.8\columnwidth]{filterbank2}%
            \end{figure}
            }
        \end{frame}
        
        \section{parameters}
        \begin{frame}{filters}{filter parameters --- lowpass/highpass}
            \begin{columns}
            \column{.5\linewidth}
                \begin{itemize}
                    \item   \textbf{cut-off} frequency $f_\mathrm{c}$
                        \begin{itemize}
                            \item   frequency marking the transition of pass to stop band
                            \item   \unit[-3]{dB} of pass band level
                        \end{itemize}
                    \smallskip
                    \item   \textbf{slope}/steepness
                        \begin{itemize}
                            \item   measured in dB/Octave or dB/Decade
                            \item   typically directly related to filter order
                        \end{itemize}
                    \smallskip
                    \item   sometimes: \textbf{resonance}    
                        \begin{itemize}
                            \item   level increase in narrow band around cut-off frequency
                        \end{itemize}
                \end{itemize}
            \column{.5\columnwidth}
                \vspace{-3mm}
                \begin{figure}%
                    \includegraphics[width=1.1\columnwidth]{filter_parameters_lowpass}%
                \end{figure}
            \end{columns}
        \end{frame}
        \begin{frame}{filters}{filter parameters --- bandpass/bandstop}
            \begin{columns}
            \column{.5\linewidth}
                \begin{itemize}
                    \item   \textbf{center} frequency $f_\mathrm{c}$
                        \begin{itemize}
                            \item   frequency marking the center of the pass or stop band
                        \end{itemize}
                    \smallskip
                    \item   \textbf{bandwidth} $\Delta B$
                        \begin{itemize}
                            \item   width of the pass band
                            \item   at \unit[-3]{dB} of max pass band level
                        \end{itemize}
                    \smallskip
                    \item   possibly: \textbf{slope}    
                        \begin{itemize}
                            \item   typically directly related to filter order
                        \end{itemize}
                \end{itemize}
            \column{.5\columnwidth}
                \begin{figure}%
                    \includegraphics[width=1.1\columnwidth]{filter_parameters_bandpass}%
                \end{figure}
            \end{columns}
        \end{frame}
        \begin{frame}{filters}{filter parameters --- peak}
            \begin{columns}
            \column{.5\linewidth}
                \begin{itemize}
                    \item   \textbf{center} frequency $f_\mathrm{c}$
                        \begin{itemize}
                            \item   frequency marking the center of the peak
                        \end{itemize}
                    \smallskip
                    \item   \textbf{Q factor} or \textbf{bandwidth} $\Delta B$
                        \begin{itemize}
                            \item   width of the bell
                            \item   at \unit[-3]{dB} of max gain
                            \[Q = \frac{f_\mathrm{c}}{\Delta B}\]
                        \end{itemize}
                    \smallskip
                    \item   \textbf{gain}    
                        \begin{itemize}
                            \item   amplification/attenuation in dB
                        \end{itemize}
                \end{itemize}
            \column{.5\columnwidth}
                \vspace{-2mm}
                \begin{figure}%
                    \includegraphics[width=1\columnwidth]{filter_parameters_peak}%
                \end{figure}
            \end{columns}
        \end{frame}
        \begin{frame}{filters}{filter parameters --- overview}
            \begin{footnotesize}
            \begin{table}%
            \begin{tabular}{l|ccccc}
                \textit{parameter} & \textbf{lowpass} & \textbf{low shelving} & \textbf{band pass} & \textbf{peak} & \textbf{resonance}\\ \hline
                \textit{frequency} &cut-off&cut-off&center&center&center\\
                \textit{bandwidth/Q} &res.\ gain&---&$\Delta B$& $Q$ &---\\
                %slope &dB/Oct&---&dB/Oct&---&---\\
                \textit{gain} &---&yes&---&yes&---
            \end{tabular}
            \end{table}
            \end{footnotesize}
        \end{frame}

        \begin{frame}{filters}{filter usage: general rules of thumb}
            some selected statements about filtering:
            \begin{itemize}
                \item \textbf{general}:
                    \begin{itemize}
                        \item   don't allow too much frequency overlap between instruments
                    \end{itemize}
                \smallskip
                \item   \textbf{bass} instruments:
                    \begin{itemize}
                        \item   volume/boom: $>\unit[60-80]{Hz}$
                        \item   kick: $>\unit[2-4]{kHz}$
                        \item   brightness: $>\unit[4-6]{kHz}$
                    \end{itemize}
                \smallskip
                \item   \textbf{higher} instruments:
                     \begin{itemize}
                        \item   volume: $>\unit[100-300]{Hz}$
                        \item   presence: $>\unit[2-5]{kHz}$
                        \item   sharpness/brightness: $>\unit[5-8]{kHz}$
                    \end{itemize}
            \end{itemize}
        \end{frame}

        \section{1st order RC filter}
        \begin{frame}{filters}{first order low pass filter: voltages}
            \begin{columns}
            \column{.6\linewidth}
                \begin{eqnarray*}
                    V_\mathrm{out} &=& \frac{i(t)}{\jom C}\\
                    V_\mathrm{in} &=& \underbrace{V_\mathrm{R}}_{i(t)\cdot R} + V_\mathrm{out}\\
                    &=& i(t)\cdot \left(R + \frac{1}{\jom C}\right)\\
                    %\Rightarrow \frac{V_\mathrm{out}}{V_\mathrm{in}} &=& \frac{i(t)\cdot \left(R + \frac{1}{\jom C}\right)}{\frac{i(t)}{\jom C}}\\
                    %&=& \frac{1}{1 + \jom RC}
                \end{eqnarray*}
            \column{.4\linewidth}
                \begin{figure}%
                    \only<1->{\includegraphics[width=\columnwidth]{RCcircuit}}
                \end{figure}
            \end{columns}
        \end{frame}
        \begin{frame}{filters}{first order low pass filter: transfer function 1/3}
            \begin{eqnarray*}
                H(\jom) &=& \frac{V_\mathrm{out}}{V_\mathrm{in}}\\
                &=& \frac{\frac{i(t)}{\jom C}}{i(t)\cdot \left(R + \frac{1}{\jom C}\right)}\\
                \pause
                &=& \frac{1}{1 + \jom \underbrace{RC}_{:=\frac{1}{\omega_\mathrm{C}}}}\\
                &=& \frac{1}{1 + \jom/\omega_\mathrm{C}}\\
            \end{eqnarray*}
        \end{frame}
        \begin{frame}{filters}{first order low pass filter: transfer function 2/3}
        \vspace{-3mm}
            \begin{eqnarray*}
                H(\jom) &=& \frac{1}{1 + \jom/\omega_\mathrm{C}}\\
                \Rightarrow |H(\jom)| &=&  \left|\frac{1}{1+\jom /\omega_\mathrm{C}}  \right|\\
                &=&  \left|{\frac{1-\jom/\omega_\mathrm{C}}{(1+\jom/\omega_\mathrm{C})\cdot(1-\jom/\omega_\mathrm{C})}} \right| \\
                &=&  \frac{1}{\sqrt{1+\omega^2/\omega_\mathrm{C}^2}}  \\
            \end{eqnarray*}
            \pause
            \vspace{-1mm}
            \textbf{discussion}:
            \begin{itemize}
                \item   $\omega \rightarrow 0$: constant
                \item   $\omega \rightarrow \infty$: 0
                \item   $|H(\omega = \omega_\mathrm{C})| = \frac{1}{\sqrt{2}}$ 
            \end{itemize}
        \end{frame}
        \begin{frame}{filters}{first order low pass filter: transfer function 3/3}
            \vspace{-4mm}
            \begin{equation*}
                |H(\jom)| = \frac{1}{\sqrt{1+\omega^2/\omega_\mathrm{C}^2}}
            \end{equation*}
            \vspace{-2mm}
            \figwithmatlab{RcFilt}
        \end{frame}
        
        \begin{frame}{filters}{in class exercise: RC}
            derive the magnitude response of a first order high pass filter (RC)
        \end{frame}
        
        
        \begin{frame}{filters}{first order low pass filter}
            without derivation: 
            \[h(t) = \omega_\mathrm{C} \cdot e^{-\omega_\mathrm{C}t}\]
            
            \figwithmatlab{RcIr}
        \end{frame}
        
        \section{digital 1st order filter}
        \begin{frame}{filters}{discrete first order low pass filter --- derivation}
            \vspace{-7mm}
            \begin{eqnarray*}
                V_\mathrm{in} - V_\mathrm{out} &=& R\cdot\underbrace{i(t)}_{= \frac{dQ_c}{dt} = C\frac{dV_\mathrm{out}}{dt}}\\
                &=& \frac{1}{\omega_\mathrm{C}}\frac{dV_\mathrm{out}}{dt}\\
                \pause
                \smallskip
                x_i - y_i &=& \frac{1}{\omega_\mathrm{C}}\frac{y_i-y_{i-1}}{\Delta T}\\
                x_i &=& y_i + \frac{y_i}{\omega_\mathrm{C}\Delta T} - \frac{y_{i-1}}{\omega_\mathrm{C}\Delta T}\\
                \Rightarrow y_i &=& \frac{x_i + \frac{y_{i-1}}{\omega_\mathrm{C}\Delta T}}{1+\frac{1}{\omega_\mathrm{C}\Delta T}}\\
                \text{define: } \alpha &=& \frac{\omega_\mathrm{C}\Delta T}{1+\omega_\mathrm{C}\Delta T}\\
                \Rightarrow y_i &=& \alpha x_i + (1-\alpha)\cdot y_{i-1}
            \end{eqnarray*}
            \question{what is $\Delta T$}
            sampling interval
        \end{frame}
        \begin{frame}{filters}{discrete first order low pass filter --- discussion}
            \begin{itemize}
                \item   range of $\alpha$
                \item   behavior at boundaries of $\alpha$
                \item   definition sometimes with flipped alpha:
                \[y_i = (1-\alpha)\cdot x_i + \alpha y_{i-1}\]
                \item   \textbf{recursive} filter (impact on implementation)
            \end{itemize}
        \end{frame}
        \begin{frame}{filters}{in class exercise}
            \matlabexercise{implement 1st order filter}
            
            \begin{enumerate}
                \item   implement the filter equation
                \item   how do you verify the implementation
                \item   apply the filter to a square wave with different values for $\alpha$
                \item   apply the filter to an audio signal with different values for $\alpha$, visualize and listen to it
                \item   discuss your observations
            \end{enumerate}
        \end{frame}
        \begin{frame}{filters}{higher order filters}
            \begin{itemize}
                \item   typical audio filters are 1st or 2nd order, but higher orders are possible
                \bigskip
                \item   higher filter orders allow for more flexibility in magnitude transfer function design
                    \begin{itemize}
                        \item   high order filters can be represented as series of low order filters
                        \item   allowing for steepness, multiple local maxima/minima etc.
                        \item   e.g., a second order low pass can have a stop band attenuation of \unit[12]{dB/Oct}
                    \end{itemize}
            \end{itemize}
        \end{frame}
        
        \begin{frame}{filters}{second order low pass filter}
            \begin{figure}
                \includegraphics[width=.5\linewidth]{2ndorder_circ}\includegraphics[width=.5\linewidth]{2ndorder_mag}
            \end{figure}
            
            \addreference{https://www.electronics-tutorials.ws/filter/second-order-filters.html}
        \end{frame}

        \section{summary}
        \begin{frame}{filters}{summary}
            \vspace{-3mm}
            \begin{itemize}
                \item   filter (equalization) can be used for various tasks
                    \begin{itemize}
                        \item   changing the sound quality of a signal
                        \item   hiding unwanted frequency components
                        \item   smoothing
                        \item   processing for measurement and transmission
                    \end{itemize}
                \item<2->   most common audio filter types are
                    \begin{itemize}
                        \item   low/high pass
                        \item   peak
                        \item   shelving
                    \end{itemize}
                \item<3->   filter parameters include
                    \begin{itemize}
                        \item   frequency (mid, cutoff)
                        \item   bandwidth or Q
                        \item   gain
                    \end{itemize}
                \item<4->   filter orders
                    \begin{itemize}
                        \item   typical orders are 1st, 2nd, maybe 4th
                        \item   higher order give more flexibility wrt transfer function
                        \item   higher orders are difficult to design and control
                        \item   higher orders can be split into multiple low order filters
                    \end{itemize}
            \end{itemize}
        \end{frame}

\end{document}

