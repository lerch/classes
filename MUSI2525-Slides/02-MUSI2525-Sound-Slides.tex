% move all configuration stuff into one file so we can focus on the content
\input{common}


\subtitle{Part 2: Sound}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
    % generate title page
	\input{titlepage}

    \section[intro]{introduction}
        \begin{frame}{sound}{introduction}
            \question{what is sound}

            Several ways to look at sound:
            \begin{itemize}
                \item   \textbf{origin}: it originates in something the vibrates/oscillates
                \item   \textbf{transmission}: it travels through a medium such as air
                \item   \textbf{reception}: it is something that is converted by the ear into a perceptual event
            \end{itemize}
            
            \pause
            \begin{block}{wikipedia}
                In physics, sound is a vibration that propagates as a typically audible mechanical wave of pressure and displacement, through a medium such as air or water. In physiology and psychology, sound is the reception of such waves and their perception by the brain.
            \end{block}
        \end{frame}

    \section[generation]{generation of sound}
        \begin{frame}{generation of sound}{sources}
            \vspace{-3mm}
            \begin{itemize}
                \item	\textbf{typical sources}
                        \begin{itemize}
                            \item   swinging vocal chords
                            \item   strings, membrane (instrument \& speaker), oscillating air column
                            \item[$\rightarrow$] anything that vibrates   
                        \end{itemize}
                \smallskip
                \item<2->   \textbf{oscillation}
                    \begin{itemize}
                        \item   repetitive, periodic vibration
                        \item   example oscillation: sinusoidal
                    \end{itemize}
            \end{itemize}
            \only<3->{
                \vspace{-4mm}
                \figwithmatlab{Sinusoidal}
            }
            \vspace{50mm}
        \end{frame}

        \begin{frame}{in class exercise}{create a plot of a sinusoidal}
            \matlabexercise{sinusoidal plot}
            
                \begin{enumerate}
                    \item   create a matlab function with the input arguments (Amplitude, NumberOfPeriods)
                    \item   call this function and create different plots for different parametrizations
                \end{enumerate}
        \end{frame}

        \begin{frame}{generation of sound}{source description}
            any periodic oscillation can be described by
            \begin{itemize}
                \item   shape (e.g., sinusoidal or more complex)
                \item   amplitude (scaling)
                \item   length of one period $T_0\; [\unit{s}]$ or frequency $f_0 [\unit{Hz}]$
                \item   (phase)
            \end{itemize}
                \vspace{-4mm}
                \figwithmatlab{Sinusoidal}
        \end{frame}

    \section{frequency}
        \begin{frame}{periodic oscillation}{frequency}
            \question{what is the frequency and how does it relate to the period length}
            
            \begin{eqnarray*}
                f_0 &=& \frac{\text{number of oscillations}}{s}\\
                  &=& \frac{1}{T_0}\; [\unit{Hz}]
            \end{eqnarray*}
            
            \pause
            \question{what are the frequencies that typical humans can hear}
            \begin{itemize}
                \item   lower bound: 12--30 \unit{Hz}
                \item   upper bound: 16,000--20,000 \unit{Hz}
            \end{itemize}
        \end{frame}

        \begin{frame}{audio example}{frequency sweep}
            \begin{figure}
                \includegraphics{Sweeps}
            \end{figure}
            
            \begin{itemize}
                \item   sine sweep \includeaudio{Sweeps_sine}
                \item   pulse train with increasing frequency \includeaudio{Sweeps_puls} , 4--50\unit{Hz}: \includeaudio{clicktrain}
            \end{itemize}
        \end{frame}

        \begin{frame}{typical frequency ranges}{musical instruments 1/2}
            \vspace{-2mm}
            \figwithref{frequencyranges}{http://acousticslab.org/psychoacoustics/PMFiles/Module05.htm}
        \end{frame}

        \begin{frame}{typical frequency ranges}{musical instruments 2/2}
            \question{is the frequency range between 5--20\unit{kHz} useless as there are no fundamental frequencies}
            
            \begin{itemize}
                \item   level of higher frequency components impacts sound quality (e.g., brightness)
                \item   but: very high frequencies above 12--15\unit{kHz} are less important
            \end{itemize}
            
        \end{frame}

    \section[adding sines]{addition of sinusoidals}
        \begin{frame}{adding sinusoidals}{integer multiples}
            example: adding sinusoidals at integer multiples:
                \[x(t) = \sum{a_i\cdot \sin(2\pi f_i t)}\]
            \only<1>{
                    \begin{figure}
                        \centering
                        \includegraphics{graph/AdditiveSynthesisSaw-1}
                    \end{figure}
            }
            
            \setcounter{i}{1}
            \whiledo{\value{i}<6}	
            {
                \pause
                \only<\value{beamerpauses}>
                {
                    \begin{figure}
                        \centering
                        \includegraphics{graph/AdditiveSynthesisSaw-\arabic{i}}
                    \end{figure}
                    \audioautoplay{additivesynthesis_saw_\arabic{i}}
                    
                    \addreference{matlab source: matlab/displayAdditiveSynthesis.m}
                }
                \stepcounter{i} 
            }	
            
            \setcounter{i}{1}
            \whiledo{\value{i}<6}	
            {
                \pause
                \only<\value{beamerpauses}>
                {
                    \begin{figure}
                        \centering
                        \includegraphics{graph/AdditiveSynthesisRect-\arabic{i}.pdf}
                    \end{figure}
                    \audioautoplay{additivesynthesis_rect_\arabic{i}}
                    
                    \addreference{matlab source: matlab/displayAdditiveSynthesis.m}
                }
                \stepcounter{i} 
            }	
        \end{frame}
 
        \begin{frame}{adding sinusoidals}{integer multiples}
            youtube example --- mechanical additive synthesis:
            
            \url{http://youtu.be/8KmVDxkia_w}
         \end{frame}
 
        \begin{frame}{adding sinusoidals}{terminology}
            \begin{itemize}
                \item   \textbf{partials}: a set of frequencies comprising a (pitched) sound
                \item   \textbf{overtones}: as partials but without the fundamental frequency
                \item   \textbf{harmonics}: integer multiples of the fundamental frequency, including the fundamental frequency
            \end{itemize}
            
            \pause
            \bigskip
            \toremember{all periodic signals ...}
            \begin{itemize}
                \item   have fundamental frequency (even if weight might be zero)
                \item   consist  only of sinusoidals at integer multiples of fundamental frequency
            \end{itemize}
        \end{frame}
 
        \begin{frame}{adding sinusoidals}{non-integer ratios}
            \vspace{-6mm}
            \begin{footnotesize}
                \begin{eqnarray*}
                    y(t) &=& \underbrace{\sin\left(2\pi (f+\frac{\Delta f}{2})t\right)}_{\sin(2\pi f)\cos\left(2\pi t\frac{\Delta f}{2}\right) + \cos(2\pi f)\sin\left(2\pi t\frac{\Delta f}{2}\right)} + \underbrace{\sin\left(2\pi (f-\frac{\Delta f}{2})t\right)}_{\sin(2\pi f)\cos\left(-2\pi t\frac{\Delta f}{2}\right) + \cos(2\pi f)\sin\left(-2\pi t\frac{\Delta f}{2}\right)}\\
                  &=& 2\sin\left(2\pi f\right)\cdot \cos\left(2\pi\frac{\Delta f}{2}t\right)
                \end{eqnarray*}
            \only<1>{
                \vspace{-3mm}
                \figwithmatlab{Beating}
            }
            \only<2>{
            
            \bigskip
            audio examples: addition of sines
            \begin{table}
                \centering
                    \begin{tabular}{l|ccccccccc}
                        $500\unit{Hz}$ & $+1000$ & $+750$ & $+667$ & $+625$ & $+600$ &$+530$ &$+502$ &$+501$\\
                        \includeaudio{Beating_sine}& \includeaudio{Beating_1000}& \includeaudio{Beating_750}& \includeaudio{Beating_667}& \includeaudio{Beating_625}& \includeaudio{Beating_600}& \includeaudio{Beating_530}& \includeaudio{Beating_502}& \includeaudio{Beating_501} \\
                    \end{tabular}
            \end{table}
             }
            \end{footnotesize}
            \vspace{30mm}
        \end{frame}
 
        \begin{frame}{adding sinusoidals}{same frequency}
            \figwithmatlab{PhaseShift}
        \end{frame}
        \begin{frame}{adding sinusoidals}{phase shift}
            \vspace{-6mm}
            \begin{itemize}
                \item[]   example: saw, 20 harmonics
            \end{itemize}
            \vspace{-3mm}
            \begin{figure}
                \includegraphics[scale=.4]{sawphase}
            \end{figure}
            \vspace{-5mm}
            \begin{center}
                \includeaudio{saw0}
                \includeaudio{sawpi2}\\
                \includeaudio{sawpi}
                \includeaudio{sawrand} 
            \end{center}
        \end{frame}
 
    \section[generation]{strings and tubes}
        \begin{frame}{vibrating string}{fundamental \& harmonics}
            \begin{itemize}
                \item   string is fixed at both ends
                \item[$\Rightarrow$] always nodes on ends (standing wave)
                \item<2-> all modes are integer multiples of fundamental frequency
            \end{itemize}
            \begin{figure}%
                \includegraphics[scale=.5]{string}%
            \end{figure}
            \addreference{\url{http://www.open.edu/openlearn/science-maths-technology/engineering-and-technology/technology/creating-musical-sounds/content-section-5.4}}
        \end{frame}
        \begin{frame}{vibrating string}{modes}
            \url{https://youtu.be/-k2TuJfNQ9s}
        \end{frame}
        \begin{frame}{vibrating string}{modes}
                    \begin{figure}
                        \centering
                        \includegraphics[width=\linewidth]{graph/tubes}
                    \end{figure}
            \addreference{\url{https://opentextbc.ca/physicstestbook2/chapter/sound-interference-and-resonance-standing-waves-in-air-columns/}}
        \end{frame}
 
    \section[time]{sound events in time}
        \begin{frame}{sound events in time}{amplitude envelope model 1/2}
            \begin{itemize}
                \item   most signals are somewhere between sinusoidal and noise
                    \begin{itemize}
                        \item   \textbf{periodic}: total predictability
                        \item   \textbf{random}: total unpredictability
                    \end{itemize}
                \pause
                \bigskip
                \item   an event in music usually has a time-varying amplitude envelope
            \begin{figure}
                \includegraphics[scale=.4]{graph/adsr}
            \end{figure}
            \end{itemize}
        \end{frame}

        \begin{frame}{sound events in time}{amplitude envelope model 2/2}
            \begin{figure}
                \includegraphics[scale=.4]{graph/adsr}
            \end{figure}
            \begin{enumerate}
                \item   \textbf{attack}: initial transient, begin of energy transfer (pluck, hit, bow)
                \item   \textbf{sustain}: steady-state, periodic part of event
                \item   \textbf{release}: time to silence after excitation stops
            \end{enumerate}
        \end{frame}

    \section{summary}
        \begin{frame}{intro to sound}{summary}
            \begin{itemize}
                \item   \textbf{sound}: 
                    \begin{itemize}
                        \item   generated by vibration
                    \end{itemize}
                \bigskip
                \item<2->   periodic \textbf{oscillation}
                    \begin{itemize}
                        \item   period length or frequency
                        \item   amplitude
                        \item   harmonics ($\rightarrow$ waveform shape)
                        \item   phase
                    \end{itemize}
                \bigskip
                \item<3->   typical \textbf{frequency ranges} in music
                    \begin{itemize}
                        \item   fundamental frequencies: 30--4000\unit{Hz}
                        \item   harmonics: up to 16\unit{kHz}
                    \end{itemize}
                \bigskip
                \item<4->   differentiation between infinite signals and real-world signals
            \end{itemize}
        \end{frame}
\end{document}

