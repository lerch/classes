function dispRcIr()

    hFigureHandle = genFigure(10.8,5);
    
    [cPath, cName]  = fileparts(mfilename('fullpath'));
    cOutputFilePath = [cPath '/../graph/' strrep(cName, 'disp', '')];

    fTime           = linspace(0, .2, 4096);
    fCutoff         = 10*[.1 1 2 3 4 2*pi];

    for (i =1:length(fCutoff))
        h(i,:)= exp(-fCutoff(i)*fTime);
    end
    plot(fTime, h)
    ylabel('$h(t)$')
    text(.1,.6,'$\downarrow\omega_C$','Interpreter','latex')

    xlabel('time')
    axis([fTime(1) fTime(end) 0 1])
    writeFigure(hFigureHandle, cOutputFilePath)

end
