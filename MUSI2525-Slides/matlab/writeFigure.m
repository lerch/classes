function writeFigure(hFigureHandle, cOutputFilePath)
   print(hFigureHandle, '-dpng', strcat(cOutputFilePath,'.png'));
   print(hFigureHandle, '-depsc', '-tiff', '-r600', '-cmyk', strcat(cOutputFilePath,'.eps'));
   [a,b] = system(sprintf('epstopdf --gsopt=-dPDFSETTINGS=/prepress --outfile=%s.pdf %s.eps',cOutputFilePath,cOutputFilePath));
   if (~isempty(b))
       warning('writeFigure: eps to pdf conversion failed... ("%s")\n esp2pdf message: "%s"',cOutputFilePath,b);
   end
end