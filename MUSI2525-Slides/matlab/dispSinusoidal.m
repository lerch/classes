function dispSinusoidal()

    hFigureHandle = genFigure(10.8,4.5);
    
    [cPath, cName]  = fileparts(mfilename('fullpath'));
    cOutputFilePath = [cPath '/../graph/' strrep(cName, 'disp', '')];

    fFreqInHz       = 3;
    fAmplitude      = .5;
    fLengthInS      = 1;
    fSampleRateInHz = 44100;
    
    [x,t] = genSineWave(fFreqInHz, fAmplitude, fLengthInS, fSampleRateInHz);
    
    % plot data
    plot (t,x);

    % data formatting
    axis([t(1) t(end) -.75 .75]);
 
    xlabel('$t$ [s]');
    
    iFirstMax = fSampleRateInHz/fFreqInHz/4;
    [xp, yp] = ds2nfu([t(iFirstMax) t(iFirstMax)],[0 fAmplitude]);
    h = annotation('textarrow',xp,yp,'String','amplitude');
    h.FontSize = 7;
    h.Interpreter = 'latex';
    [xp, yp] = ds2nfu([t(iFirstMax*5) t(iFirstMax*9)],[.55 .55]);
    h = annotation('doublearrow',xp,yp);
    %[xp yp] = ds2nfu(.48, .3);
    text(.5,.6,'period length','FontSize',7);

    writeFigure(hFigureHandle, cOutputFilePath)

end
