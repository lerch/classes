function dispFracDelayIndex()

    hFigureHandle = genFigure(12,5);
    
    [cPath, cName]  = fileparts(mfilename('fullpath'));
    cOutputFilePath = [cPath '/../graph/' strrep(cName, 'disp', '')];

    fFreqInHz       = 0.5;
    fAmplitude      = 1;
    fLengthInS      = 20;
    fSampleRateInHz = 44100;
    
    [x,t] = genSineWave(fFreqInHz, fAmplitude, fLengthInS, fSampleRateInHz);
    
    % plot data
    subplot(121)
    plot(t,t,'Color',[0.8 0.8 0.8])
    hold on
    plot (t,t+x,'k');
    hold off
    title('modulation with decimal index')
    xlabel('$t$ [s]');
    ylabel('index');

    subplot(122)
    plot(t,t,'Color',[0.8 0.8 0.8])
    hold on
    plot (t,round(t+x),'k');
    hold off
    title('modulation with integer index')
    xlabel('$t$ [s]');
    ylabel('index');

    % data formatting
    axis([t(1) t(end) t(1) t(end)]);
 
    

    writeFigure(hFigureHandle, cOutputFilePath)

end
