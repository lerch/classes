function dispMicDisplacement()

    hFigureHandle = genFigure(10.8,4);
    
    [cPath, cName]  = fileparts(mfilename('fullpath'));
    cOutputFilePath = [cPath '/../graph/' strrep(cName, 'disp', '')];

    omega=linspace(0,2,1000);
    omega0 = 1;
    r = linspace(.3, 1.3, 3);

    eta = omega0*r;

    for (j = 1:3)
        y(j,:)= abs(1./(1-(omega/omega0).^2+i*eta(j)*omega/omega0));
    end

    plot(omega,20*log10(y(1,:)),'k',omega,20*log10(y(2,:)),'k',omega,20*log10(y(3,:)),'k','LineWidth',2),

    % data formatting
    axis([omega(1) omega(end) -15 12])
 
    xlabel('$\omega/ \omega_0$');
    ylabel('$20\log_{10}\left(\frac{x}{F}\right)$');
    set(gca,'XTick',[0 .5 1 1.5 2]),
    set(gca,'YTickLabel',[]),
    text(.85, 7, '$\downarrow \frac{c}{k}$','interpreter','latex')
   
    writeFigure(hFigureHandle, cOutputFilePath)

end
