function dispPhaseShift()

    hFigureHandle = genFigure(10.8,8);
    
    [cPath, cName]  = fileparts(mfilename('fullpath'));
    cOutputFilePath = [cPath '/../graph/' strrep(cName, 'disp', '')];

    fFreqInHz       = 2;
    fAmplitude      = .5;
    fLengthInS      = 1;
    fSampleRateInHz = 44100;
    fPhase          = [0 pi/4 pi/3 pi/2 2*pi/3 3*pi/4 pi];
    
    [x1,t] = genSineWave(fFreqInHz, fAmplitude, fLengthInS, fSampleRateInHz);
    
    for (i = 1:length(fPhase))
        [x2(i,:),t] = genSineWave(fFreqInHz, fAmplitude, fLengthInS, fSampleRateInHz, fPhase(i));
        
        subplot(7,3,3*(i-1)+1),plot(t,x1), axis([t(1) t(end) -1 1]),set(gca,'YTickLabel',''),set(gca,'XTickLabel','')
        annotation(hFigureHandle,'textbox',...
            [0.36 0.9-(i-1)*.12 0.04 0.06],...
            'String',{'+'},...
            'HorizontalAlignment','center',...
            'FontSize',20,...
            'FitBoxToText','off',...
            'EdgeColor','none');
        subplot(7,3,3*(i-1)+1+1),plot(t,x2(i,:)), axis([t(1) t(end) -1 1]),set(gca,'YTickLabel',''),set(gca,'XTickLabel','')
        annotation(hFigureHandle,'textbox',...
            [0.64 0.9-(i-1)*.12 0.04 0.06],...
            'String',{'='},...
            'HorizontalAlignment','center',...
            'FontSize',20,...
            'FitBoxToText','off',...
            'EdgeColor','none');
        subplot(7,3,3*(i-1)+1+2),plot(t,x1+x2(i,:)), axis([t(1) t(end) -1 1]),set(gca,'YTickLabel',''),set(gca,'XTickLabel','')
    end
    
    writeFigure(hFigureHandle, cOutputFilePath)

end
