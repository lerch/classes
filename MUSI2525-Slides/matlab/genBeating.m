function genBeating()

    [cPath, cName]  = fileparts(mfilename('fullpath'));
    cOutputFilePath = [cPath '/../graph/' strrep(cName, 'disp', '')];
    cAudioFilePath  = [cPath '/../audio/' strrep(cName, 'gen', '')];
    cAudioFilePath1 = [cPath '/../audio/' strrep(cName, 'gen', '') '_sine'];

    fFreqInHz1      = 1000/2;
    fFreqInHz2      = [2000 1500 1333 1250 1200 1000*2^(1/12) 1003 1001]/2;
    fAmplitude      = .5;
    fLengthInS      = 1;
    fSampleRateInHz = 44100;
 
    [x1,t] = genSineWave(fFreqInHz1, fAmplitude, fLengthInS, fSampleRateInHz);
    writeAudio(cAudioFilePath1, x1, fSampleRateInHz);
    for (i = 1:length(fFreqInHz2))
        [x2,t] = genSineWave(fFreqInHz2(i), fAmplitude, fLengthInS, fSampleRateInHz);
        writeAudio([cAudioFilePath '_' int2str(fFreqInHz2(i))], x1+x2, fSampleRateInHz);
    end
end
