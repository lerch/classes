function dispPanning()

    hFigureHandle = genFigure(10.8,5);
    
    [cPath, cName]  = fileparts(mfilename('fullpath'));
    cOutputFilePath = [cPath '/../graph/' strrep(cName, 'disp', '')];

    theta=linspace(0,1,1000);
    
    left = zeros(3,length(theta));
    right = zeros(size(left));

    left(1,:) = theta;
    right(1,:) = 1-left(1,:);
    
    left(2,:) = sin(theta * pi/2);
    right(2,:) = cos(theta * pi/2);
    
    left(3,:) = sqrt(theta);
    right(3,:) = fliplr(left(3,:));

    plot(theta,left(1,:),'k')
    hold on
    plot(theta,left(2,:),'b')
    plot(theta,left(3,:),'r')
    plot(theta,right(1,:),'k')
    plot(theta,right(2,:),'b')
    plot(theta,right(3,:),'r')
    h = legend('$r = x$',...
        '$r = \sin(x \pi/2)$',...
        '$r = \sqrt{x}$')
    set(h,'Interpreter','latex')
 
    xlabel('panning angle ($x = 0\ldots 1$)');
    ylabel('channel attenuation');
    set(gca,'XTick',[0 0.5 1]),
    set(gca,'XTickLabel',{'left', 'mid', 'right'}),
   
    writeFigure(hFigureHandle, cOutputFilePath)

end
