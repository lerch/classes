function genSweeps()

    hFigureHandle = genFigure(10.8,4.5);
    
    [cPath, cName]  = fileparts(mfilename('fullpath'));
    cOutputFilePath = [cPath '/../graph/' strrep(cName, 'gen', '')];
    cAudioFilePath1 = [cPath '/../audio/' strrep(cName, 'gen', '') '_sine'];
    cAudioFilePath2 = [cPath '/../audio/' strrep(cName, 'gen', '') '_puls'];

    fSampleRateInHz = 44100;
    fStart = 1;
    fStop  = 20000;
    t   = linspace(0,10,fSampleRateInHz*10);
    x1  = chirp(t, fStart, 10, fStop,'logarithmic',pi/2);
    f   = fStart*(fStop/fStart).^(t./t(end));
    
    period      = logspace(log10(1/(5*fStart)), log10(1/fStop),410);
    t_pulse     = cumsum(period);
    amplitude   = ones(1,numel(t_pulse)); % vector of amplitudes
    x2          = pulstran(t,[t_pulse;amplitude]','gauspuls',10e3,0.5);
    
    % plot data
    plot (t,f);

    % data formatting
    axis([t(1) t(end) 0 20000]);
 
    xlabel('$t$ [s]');
    ylabel('$f$ [Hz]');

    writeFigure(hFigureHandle, cOutputFilePath)
    
    % write output audio
    writeAudio(cAudioFilePath1, x1, fSampleRateInHz);
    writeAudio(cAudioFilePath2, x2, fSampleRateInHz);

end
