function dispMicDirection2()

    hFigureHandle = genFigure(10.8,7.5);
    
    [cPath, cName]  = fileparts(mfilename('fullpath'));
    cOutputFilePath = [cPath '/../graph/' strrep(cName, 'disp', '')];

    name = [    'omnidirectional'
                'bidirectional  '
                'cardoid        '
                'subcardoid     '
                'supercardoid   '
                'hypercardoid   '];
    W = [ 1 -1 0 .3 -.3 -.5];

    theta   = linspace(0,2*pi,360);
    cardiod(1,:) = .5 + .5 * cos(theta);
    cardiod(2,:) = .5 - .5 * cos(theta);
    
    for (i = 1:length(W))
       direct(i,:) = cardiod(1,:) + W(i)*cardiod(2,:);
       
       subplot(2,3,i)
       polar(theta, abs(direct(i,:)));
       view(90,-90)
       ylabel([deblank(name(i,:)) '  w=' sprintf('%1.2f',W(i))])
    end

    writeFigure(hFigureHandle, cOutputFilePath)

end
