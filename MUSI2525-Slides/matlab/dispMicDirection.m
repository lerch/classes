function dispMicDirection()

    hFigureHandle = genFigure(10.8,7.5);
    
    [cPath, cName]  = fileparts(mfilename('fullpath'));
    cOutputFilePath = [cPath '/../graph/' strrep(cName, 'disp', '')];

    name = [    'omnidirectional'
                'bidirectional  '
                'cardoid        '
                'subcardoid     '
                'supercardoid   '
                'hypercardoid   '];
    A = [ 1 0 .5 .71 .37 .25];
    
    theta   = linspace(0,2*pi,360);
    for (i = 1:length(A))
       direct(i,:) = A(i) + (1-A(i)).* cos(theta);
       
       subplot(2,3,i)
       polar(theta, abs(direct(i,:)));
       view(90,-90)
       ylabel([deblank(name(i,:)) '  A=' sprintf('%1.2f',A(i))])
    end

    writeFigure(hFigureHandle, cOutputFilePath)

end
