function dispRcFilt()

    hFigureHandle = genFigure(10.8,6);
    
    [cPath, cName]  = fileparts(mfilename('fullpath'));
    cOutputFilePath = [cPath '/../graph/' strrep(cName, 'disp', '')];

    fFreq           = linspace(0, 20000, 4096);
    fCutoff         = [100 1000 10000 18000];

    for (i =1:length(fCutoff))
        H(i,:)= 1./(1+i*fFreq/fCutoff(i));
    end
    subplot(221)
    plot(fFreq, abs(H))
    ylabel('$|H(\j\omega)|$')
    text(8500,.45,'$\uparrow\omega_C$','Interpreter','latex')
    subplot(222)
    plot(fFreq, 20*log10(abs(H)))
    ylabel('$|H(\j\omega)|$ [dB]')
    axis([fFreq(1) fFreq(end) -35 0])
    subplot(223)
    semilogx(fFreq, abs(H))
    xlabel('frequency')
    ylabel('$|H(\j\omega)|$')
    axis([fFreq(1) fFreq(end) 0 1])
    subplot(224)
    semilogx(fFreq, 20*log10(abs(H)))
    ylabel('$|H(\j\omega)|$ [dB]')
    xlabel('frequency')
    axis([fFreq(1) fFreq(end) -35 0])
    writeFigure(hFigureHandle, cOutputFilePath)

end
