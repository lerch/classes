function [x,t] = genSineWave(fFreq, fAmplitude, fLengthInS, fSampleRateInHz, fPhaseInRad)

    if (nargin < 5)
        fPhaseInRad = 0;
    end
    [m n]   = size(fFreq);
    if (min(m,n)~=1)
        error('illegal frequency dimension')
    end
    if (m<n)
        fFreq   = fFreq';
    end
    
    t = linspace(0,fLengthInS-1/fSampleRateInHz,fSampleRateInHz*fLengthInS);
    
    x = fAmplitude* sin(2*pi*fFreq*t + fPhaseInRad);
end