function dispPhaseShift()

    hFigureHandle = genFigure(10.8,5);
    
    [cPath, cName]  = fileparts(mfilename('fullpath'));
    cOutputFilePath = [cPath '/../graph/' strrep(cName, 'disp', '')];

    fFreqInHz       = 2;
    fAmplitude      = .5;
    fLengthInS      = 1;
    fSampleRateInHz = 44100;
    
    [x1,t] = genSineWave(fFreqInHz1, fAmplitude, fLengthInS, fSampleRateInHz);
    [x2,t] = genSineWave(fFreqInHz2, fAmplitude, fLengthInS, fSampleRateInHz);
    
    % plot data
    subplot(221),plot(t,x1),title(sprintf('%i Hz',fFreqInHz1)), axis([t(1) t(end) -1 1]), ylabel('$x_1(t)$')
    subplot(222),plot(t,x1),title(sprintf('%i Hz',fFreqInHz2)), axis([t(1) t(end) -1 1]), ylabel('$x_2(t)$')
    subplot(212)
    plot (t,x1+x2), ylabel('$x_1(t)+x_2(t)$')

    % data formatting
    axis([t(1) t(end) -1 1])
 
    xlabel('$t$ [s]');
    
    writeFigure(hFigureHandle, cOutputFilePath)

end
