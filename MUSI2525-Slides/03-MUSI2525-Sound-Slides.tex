% move all configuration stuff into one file so we can focus on the content
\input{common}


\subtitle{Part 3: Sound Propagation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
    % generate title page
	\input{titlepage}

    \section[intro]{introduction}
        \begin{frame}{sound}{recollection}
            \begin{itemize}
                \item   previous module:
                    \begin{itemize}
                        \item   \textbf{origin} of sound: 
                            \begin{itemize}
                                \item   vibration and oscillation
                                \item   frequency, amplitude, phase, harmonics, ...
                            \end{itemize}
                    \end{itemize}
                \bigskip
                \item<2->   this module:
                    \begin{itemize}
                        \item   \textbf{transmission/propagation} of sound
                    \end{itemize}
                \bigskip
                \item<3->   next module:
                    \begin{itemize}
                        \item   \textbf{reception/perception} of sound
                    \end{itemize}
            \end{itemize}
        \end{frame}

    \section[propagation]{propagation of sound}
        \begin{frame}{propagation of sound}{introduction}
            \begin{columns}[c]
                \column{0.4\textwidth}
                    \begin{enumerate}
                        \item   sound originates from  the \textbf{motion or vibration of an object}
                        \smallskip
                        \item   motion is \textbf{impressed upon surrounding medium} (air)
                        \smallskip
                        \item   \textbf{change in atmospheric pressure} creates sound
                    \end{enumerate}
                \column{0.6\textwidth}
                    \vspace{-5mm}\flushright{\includegraphics[scale=0.1]{belllabs-sound}}
            \end{columns}
        \end{frame}
    \section[pressure]{sound pressure}
        \begin{frame}{propagation of sound}{sound pressure 1/3}
            \begin{itemize}
                \item   wave travels away from source as \textbf{longitudinal wave}
                
                    \url{https://youtu.be/7cDAYFTXq3E}
                \smallskip
                \item   \textbf{molecules do not advance} with wave (vibration around resting place)
                \smallskip
                \item   molecule motion provokes \textbf{compression \& rarefaction}
            \end{itemize}
            \begin{figure}
                \includegraphics[scale=.4]{sound-wave}
            \end{figure}
            \addreference{\url{physics.tutorvista.com/waves/sound-reflection.html}}
        \end{frame}
        \begin{frame}{the nature of sound}{sound pressure 2/3}
            \vspace{-5mm}
            \begin{itemize}
                \item   \textbf{compression}: masses are pushed together
                \item   \textbf{rarefaction}: masses are pulled apart
                \item   masses form the propagating medium
            \end{itemize}
            \begin{figure}
                \centering
                \animategraphics[height=2in,loop]{19}{graph/sound_propagation/Naval-Research-Lab-}{00}{19}        
            \end{figure}
            \addreference{\href{http://acoustics.org/pressroom/httpdocs/151st/Lindwall.html}{acoustics.org/pressroom/httpdocs/151st/Lindwall.html}}
        \end{frame}
        \begin{frame}{the nature of sound}{sound pressure 3/3}
            \begin{itemize}
                \item   molecule motion can be modeled with spring-mass model
                    \begin{figure}
                        \includegraphics[scale=.5]{graph/springmass}
                    \end{figure}
                \item[$\Rightarrow$] sinusoidal oscillation: \url{https://youtu.be/P-Umre5Np_0}
            \end{itemize}
        \end{frame}
        \begin{frame}{propagation of sound}{medium}
            \url{https://youtu.be/_ckjttBin58}
        \end{frame}
    \section[speed]{speed of sound}
        \begin{frame}{propagation of sound}{speed}
            \begin{itemize}
                \item   approximation for dry air
                    \begin{equation*}
                        c_{air} = \underbrace{331.3}_{\text{speed at $0\degree$}} + 0.606\cdot\underbrace{\vartheta}_{temperature}\; \left[\unit{\frac{m}{s}}\right]
                    \end{equation*}
            \vspace{-5mm}
            \begin{columns}
                \column{.4\textwidth}
                    \item[]   approximation fails for 
                        \begin{itemize}
                            \item   low pressure
                            \item   high temperatures
                            \item   short wavelengths
                        \end{itemize}
                \column{.4\textwidth}
                    \begin{figure}
                        \includegraphics[scale=.25]{graph/speed-of-sound}
                    \end{figure}
            \end{columns}
            \end{itemize}
            \addreference{\url{en.wikipedia.org/wiki/File:Speed_of_sound_in_dry_air.svg}}
            \pause
            \url{https://youtu.be/QX04ySm4TTk}
        \end{frame}
    \section[wave length]{wave length}
        \begin{frame}{propagation of sound}{wavelength \& frequency}
            \question{how do wave length and frequency relate to each other}
            
            \begin{eqnarray*}
                f &=& \frac{c}{\lambda}\\
                \lambda &=& \frac{c}{f}
            \end{eqnarray*}
        \end{frame}
    \section[distance]{pressure change with distance from source}
        \begin{frame}{propagation of sound}{pressure with distance from source}
            \begin{itemize}
                \item   sound pressure follows $\frac{1}{r}$ law:
                \item[]<2-> doubling the distance halves the sound pressure
                \bigskip
                \item<3->   only true in free/direct field
            \end{itemize}
            \begin{figure}%
                \includegraphics[scale=.35]{DistanceLaw}%
            \end{figure}
            \addreference{\url{http://www.sengpielaudio.com/calculator-distance.htm}}
        \end{frame}
    \section[summary]{summary}
        \begin{frame}{propagation of sound}{summary}
            \url{https://youtu.be/GkNJvZINSEY}
        \end{frame}
    \section[questions]{questions}
        \begin{frame}{propagation of sound}{questions}
           \begin{enumerate}
                \item   In what time does sound cover a length of \unit[10]{m}?
                \only<2>{
                \begin{equation*}
                    t = \frac{\Delta l}{c} = \unit[29]{ms}
                \end{equation*}
                }
                \item<3->   What is the difference between wave length and period length?
                \only<4>{
                \begin{itemize}
                    \item   wave length: minimal distance of identical oscillation state at a fixed point in time
                    \item   period length: time between identical oscillation state at a fixed point in space
                \end{itemize}
                }
                \item<5->   What are the wave lengths for \unit[16]{Hz},\unit[1000]{Hz},\unit[10000]{Hz}
                \only<6>{
                \begin{eqnarray*}
                    \lambda_{16} &=& \unit[21.25]{m}\\
                    \lambda_{1000} &=& \unit[0.34]{m}\\
                    \lambda_{10000} &=& \unit[3.4]{cm}
                \end{eqnarray*}
                }
                \item<7->   What is the frequency of a pitch A4 (assumption: 440Hz @ 14.35\degree C) for the following temperatures: 16, 19, 22\degree C
                    \begin{itemize}
                        \item   compute speed
                        \item   compute wave length at 14.35\degree C
                        \item   compute frequencies
                    \end{itemize}
                \only<8>{
                \begin{eqnarray*}
                    f_{16} &=& \unit[442.82]{Hz}?\\
                    f_{19} &=& \unit[445.12]{Hz}?\\
                    f_{22} &=& \unit[447.39]{Hz}?
                \end{eqnarray*}
                }
            \end{enumerate}
            \end{frame}
\end{document}

