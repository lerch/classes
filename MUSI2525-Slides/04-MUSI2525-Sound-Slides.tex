% move all configuration stuff into one file so we can focus on the content
\input{common}


\subtitle{Part 4: Sound Perception}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
    % generate title page
	\input{titlepage}

    \section[intro]{introduction}
        \begin{frame}{sound}{recollection}
            \begin{itemize}
                \item   previous classes:
                    \begin{itemize}
                        \item   \textbf{origin} of sound: 
                            \begin{itemize}
                                \item   vibration and oscillation
                                \item   frequency, amplitude, phase, harmonics, ...
                            \end{itemize}
                        \item   \textbf{transmission/propagation} of sound
                            \begin{itemize}
                                \item   longitudinal wave
                                \item   speed of sound
                                \item   wavelength and frequency
                            \end{itemize}
                    \end{itemize}
                \item   this class:
                    \begin{itemize}
                        \item   \textcolor{gtgold}{\textbf{reception/perception}} of sound
                    \end{itemize}
            \end{itemize}
        \end{frame}

    \section[reception]{sound reception}
        \begin{frame}{sound reception}{receiver}
            \begin{columns}
            \column{.6\linewidth}
            \begin{itemize}
                \item   reception apparatus
                \item[$\Rightarrow$]   transforms changes in sound pressure to other domain
                \bigskip
                    \begin{itemize}
                        \item<1->   \textbf{microphone}:\\ translates sound pressure or velocity into \textit{voltage changes}
                        \smallskip
                        \item<2->   \textbf{ear}:\\ translates sound pressure into \textit{nerve impulses}
                    \end{itemize}
            \end{itemize}
            \column{.4\linewidth}
                \only<1>{
                \begin{figure}%
                    \includegraphics[width=\columnwidth]{microphone}%
                \end{figure}}
                \only<2>{
                \begin{figure}%
                    \includegraphics[width=\columnwidth]{ear}%
                \end{figure}}
            \end{columns}
        \end{frame}
        
    \section{ear}
        \begin{frame}{physiology of the ear}{overview}
            \vspace{-5mm}
            \begin{figure}%
                \includegraphics[width=.8\columnwidth]{ear_complete}%
            \end{figure}
        \end{frame}
        \begin{frame}{physiology of the ear}{middle ear}
            \vspace{-5mm}
            \begin{figure}%
                \includegraphics[width=.7\columnwidth]{ear_middle}%
            \end{figure}
        \end{frame}
        \begin{frame}{physiology of the ear}{inner ear 1/2}
            \vspace{-5mm}
            \begin{figure}%
                \includegraphics[width=.6\columnwidth]{ear_inner}\includegraphics[width=.3\columnwidth]{cochlea-1}%
            \end{figure}
        \end{frame}
        \begin{frame}{physiology of the ear}{inner ear 2/2}
            \vspace{-5mm}
            \begin{columns}
            \column{.7\linewidth}
            \vspace{-5mm}
            \begin{figure}%
                \includegraphics[width=\columnwidth]{cochlea-2}%
            \end{figure}
            \column{.3\linewidth}
            \vfill
            \href{https://youtu.be/PeTriGTENoc}{youtu.be/PeTriGTENoc}
            \end{columns}
        \end{frame}

    \section[pitch]{pitch}
        \begin{frame}{pitch}{introduction}
            \question{what is pitch}
            
            \begin{itemize}
                \item   \textbf{perception}:\\ ``That attribute of \textbf{auditory sensation} in terms of which sounds may be ordered on a scale extending from low to high'' (ANSI definition)
                \bigskip
                \item   \textbf{music}:\\ ``The particular quality of a sound (e.g. an individual musical note) that fixes its position in the \textbf{scale}.'' (Grove Music Online)
            \end{itemize}
        \end{frame}
        \begin{frame}{pitch}{perceptual pitch 1/2}
            \begin{itemize}
                \item   temporal variations in pitch give rise to a sense of melody
                \item   closely related to frequency, but \textbf{subjective}
                \pause
                \bigskip
                \item[$\Rightarrow$]   assigning a pitch value to a sound means \textbf{specifying the frequency of a pure tone have the same subjective pitch} as the sound
            \end{itemize}
        \end{frame}
        \begin{frame}{pitch}{perceptual pitch 2/2}
            \vspace{-3mm}
            \textbf{2 dimensions of musical pitch}
            \begin{itemize}
                \item   \textbf{tone height}: monotonic relationship to frequency (increasing frequency $\Rightarrow$ increasing pitch)
                            \begin{figure}
                                \centering
                                    \includegraphics[scale=.5]{mel}
                            \end{figure}
                \pause
                \item   \textbf{tone chroma}: two tones separated by an octave sound similar (same \textit{pitch class})
                    \begin{figure}
                        \centering
                            \includegraphics[scale=.4]{pitchhelix}
                            \includegraphics[scale=.4]{shepard}
                    \end{figure}
            \end{itemize}
        \end{frame}
        \begin{frame}{pitch}{models of perceptual pitch}
            \vspace{-5mm}
            \begin{figure}
                \centering
                    \includegraphics[scale=.7]{mel}
            \end{figure}
            \vspace{-3mm}
            \begin{itemize}
                \item   Fant:
                \begin{footnotesize}
                \[  \mathfrak{m}_\mathrm{F}(f) = 1000\cdot \log_{2}\left(1 + \frac{f}{\unit[1000]{Hz}} \right)\]
                \end{footnotesize}
                \item O'Shaughnessy:
                \begin{footnotesize}
                \begin{eqnarray*}
                    \mathfrak{m}_\mathrm{S}(f) &=& 2595\cdot \log_{10}\left(1 + \frac{f}{\unit[700]{Hz}} \right)\nonumber\\
                    \pause
                    \mathfrak{m}_\mathrm{S}(f) &=& 1127\cdot \log\left(1 + \frac{f}{\unit[700]{Hz}} \right)\nonumber
                \end{eqnarray*}
                \end{footnotesize}
            \end{itemize}
        \end{frame}
        \begin{frame}{pitch}{MIDI: model of musical pitch}
            \vspace{-3mm}
            \begin{itemize}
                \item   frequency $\rightarrow$ MIDI pitch
                \[ \mathfrak{m}_\mathrm{M}(f) = 69 + 12\cdot\log_2\left(\frac{f}{f_\mathrm{A4}}\right)\]
                \bigskip
                \item   MIDI pitch $\rightarrow$ frequency
                \[ f(\mathfrak{m}) = f_\mathrm{A4}\cdot 2^{\frac{\mathfrak{m}-69}{12}}\]
            \end{itemize}
        \end{frame}
        \begin{frame}{pitch}{MIDI: pitch distance}
            \question{what is the frequency ratio between two semitones (equal temperament)}
                \begin{eqnarray*}
                    \frac{f(\mathfrak{m+1})}{f(\mathfrak{m})} &=&  \frac{f_\mathrm{A4}\cdot 2^{\frac{\mathfrak{m}+1-69}{12}}}{f_\mathrm{A4}\cdot 2^{\frac{\mathfrak{m}-69}{12}}}\nonumber\\
                    \pause
                    &=& 2^{\frac{1}{12}} \approx 1.06
                \end{eqnarray*}
        \end{frame}
        
        
        
        \begin{frame}{pitch}{complex tones: phenomenon of the missing fundamental}
            \vspace{-3mm}
                \begin{figure}
                    \includegraphics[scale=.3]{virtualpitchillustration}
                \end{figure}
            \begin{itemize}
                \item   basilar membrane location \textbf{does not explain} the pitch perception of complex tones
                \pause
                \bigskip
                \item   \textbf{example 1}: missing fundamental \includeaudio{zwicker-track-17}
                    \begin{itemize}
                        \item   $f_0 = \unit[120]{Hz}$, 33 harmonics, with(out) bandpass \unit[300-2400]{Hz}
                    \end{itemize}
                \pause
                \item   \textbf{example 2}: missing fundamental \includeaudio{zwicker-track-18}
                    \begin{itemize}
                        \item   speech $f_0 \approx \unit[100]{Hz}$, with(out) bandpass \unit[300-4000]{Hz}
                    \end{itemize}
                \pause
                \bigskip
                \item[$\rightarrow$] \textbf{virtual pitch}, \textbf{residue pitch}
            \end{itemize}
        \end{frame}
        \begin{frame}{pitch}{acoustic hallucination: shepard tones}
            \url{https://youtu.be/BzNzgsAE4F0}
        \end{frame}

    \section[loudness]{level \& loudness}
        \begin{frame}{sound pressure level}{introduction}
           \begin{itemize}
                \item       increasing the amplitude of the excitation vibration increases the loudness
                \item       but
                    \begin{enumerate}
                        \item<2->   it cannot be the instantaneous amplitude of the sound pressure $p(t)$ (we don't hear the short time changes)
                        \item<2->[$\Rightarrow$]   sound pressure: \[p_\mathrm{rms} = \sqrt{\frac{1}{T_\mathrm{int}}\int\limits_{T_\mathrm{int}}{p(t)^2}}\]
                        \smallskip
                        \item<3->   relation is not linear
                        \item<3->[$\Rightarrow$]   sound pressure level \[L_\mathrm{SPL} = 20\log_{10}\left(\frac{p_\mathrm{rms}}{p_\mathrm{ref}}\right)\]
                    \end{enumerate}
           \end{itemize}
        \end{frame}
        \begin{frame}{sound pressure level}{decibel properties}
            \[L_\mathrm{SPL} = 20\log_{10}\left(\frac{p_\mathrm{rms}}{p_\mathrm{ref}}\right)\]
            
            \begin{itemize}
                \item   $10\unit{deciBel = dB} = 1\unit{Bel}$
                \item   \unit{dB} is based on ratio $\rightarrow$ \textbf{requires reference} (e.g., $p_\mathrm{ref,SPL} = \unit[20]{\mu Pa}$)!
                \item   \unit[10]{dB} represent a ratio of 10:1
                \item   sound pressure level is based on \textbf{temporal integration}, not instantaneous measurements
            \end{itemize}
        \end{frame}
        \begin{frame}{sound pressure level}{examples}
            \vspace{-3mm}
            \begin{figure}%
                \includegraphics[scale=.6]{soundpressurelevels}%
            \end{figure}
            \addreference{\url{thehearingblog.com/archives/3278}}
        \end{frame}
        \begin{frame}{deciBel}{other dB measures}
            \question{Now we know $\unit{dB_\mathrm{SPL}}$. What other dB measures are out there}
            
            \begin{itemize}
                \item   $\unit{dB_\mathrm{FS}}$
                \item   $\unit{dB_\mathrm{A/B/C}}$
                \item   $\unit{dB_\mathrm{V}}$ (\unit[1]{V})
                \item   $\unit{dB_\mathrm{u}}$ (\unit[0.775]{V})
            \end{itemize}
            \pause
            \question{What does dB without any subindex mean}
            
            \begin{itemize}
                \item   it's only a relative measure of \textbf{difference}, not an absolute value
            \end{itemize}
        \end{frame}
        
    \begin{frame}{sound pressure level}{addition of two sources}
        \begin{equation*}
            z(t) = x(t) + y(t)
        \end{equation*}
        \begin{itemize}
            \item   SPL is a function of RMS, not amplitude
                \begin{itemize}
                    \item   signals identical: 
                        \begin{equation*}
                            z_\mathrm{RMS} = 2\cdot x_\mathrm{RMS}
                        \end{equation*}
                    \item   signals uncorrelated: 
                        \begin{equation*}
                            z_\mathrm{RMS} = \sqrt{2}\cdot x_\mathrm{RMS}
                        \end{equation*}
                \end{itemize}
        \end{itemize}
        \begin{figure}
            \includegraphics[scale=.3]{leveladd}
        \end{figure}
    \end{frame}
    \begin{frame}{sound pressure level}{addition of multiple sources}
        \begin{itemize}
            \item   multiple uncorrelated sources
                \begin{equation*}
                    z_\mathrm{rms} = \sqrt{\sum_i{x^2_\mathrm{i,rms}}} 
                \end{equation*}
            \pause
                \begin{equation*}
                    L_z = 10log_{10}\left(\frac{\sum_i{x^2_\mathrm{i,rms}}}{p_0}\right)
                \end{equation*}
        \end{itemize}
    \end{frame}
    \begin{frame}{sound pressure level}{exercise}
        \question{SPL decreases with $1/r$ with distance. Two microphones record the same source at \unit[60]{cm} and \unit[180]{cm}, respectively. What is the level difference}

        \begin{equation*}
            20\log_{10}\left(\frac{\nicefrac{1}{60}}{\nicefrac{1}{180}}\right) = \unit[9]{dB}
        \end{equation*}

        \bigskip
        \pause
        \question{What is the level increase between having 5 and 10 violins (playing at exactly the same level}

        \begin{equation*}
            10\log_{10}\left(10\right) - 10\log_{10}\left(5\right) = \unit[3]{dB}
        \end{equation*}
    \end{frame}        
       
    \begin{frame}{loudness}{introduction}
            \question{what is loudness}
            
            \begin{block}{definition (Moore)}
                loudness is that attribute of auditory sensation in terms of which sound can be ordered on a scale extending from quiet to loud
            \end{block}
            \smallskip
            \pause
            \begin{itemize}
                \item   loudness is \textbf{subjective quality} $\rightarrow$ cannot be measured directly!
                \item   dB is not a loudness model 
                \begin{itemize}
                    \item   scale: \unit[80]{dBSPL} not twice as loud as \unit[40]{dBSPL}
                    \item   frequency dependence
                \end{itemize}
            \end{itemize}
    \end{frame}
    \begin{frame}{loudness}{equal loudness contours}
        \only<1>{
        \begin{figure}%
            \includegraphics[scale=.4]{EqualLoudnessContours}%
        \end{figure}
        }
        \only<2->{
            \begin{block}{loudness level}
                the loudness level (in phon) of stimulus is equal to the $\unit{dB_\mathrm{SPL}}$ of a \unit[1]{kHz} sinusoidal of the same perceived loudness.
            \end{block}
            \begin{columns}
                \column{0.5\columnwidth}
                    \begin{itemize}
                        \item   highest sensitivity at \unit[3--5]{kHz}
                        \item   similar shape for all levels, flatter at high levels
                        \item   relative loudness changes as function of overall level
                        \item   frequency balance changes with reproduction level
                    \end{itemize}
                \column{0.5\columnwidth}
                    \begin{flushright}%
                        \includegraphics[scale=.25]{EqualLoudnessContours}%
                    \end{flushright}
            \end{columns}
        }
        \addreference{\url{en.wikipedia.org/wiki/Equal-loudness\_contour\#/media/File:Lindos1.svg}}
    \end{frame}
    \begin{frame}{loudness}{loudness model}
        \begin{figure}%
            \includegraphics[scale=.4]{stevensdoubleloudness}
        \end{figure}

        \begin{itemize}
            \item   phone is \textit{not a loudness scale}!
            \item   experiments indicate that a \unit[10]{phon} increase doubles the loudness for many people
            \item   definition: \unit[1]{sone} equals loudness of pure tone at \unit[1]{kHz} @ \unit[40]{dBSPL}
        \end{itemize}
        \addreference{Stevens, The Measurement of Loudness, JASA 27(5), 1955}
    \end{frame}

    \section[summary]{summary}
        \begin{frame}{sound reception}{summary}
            \begin{itemize}
                \item   ear transforms the sound pressure level changes into nerve impulses
                \item   relation of frequency (physical entity) and pitch (perceptual entity)
                \item   relation of sound pressure, sound pressure level (physical) and loudness (perceptual)
            \end{itemize}
        \end{frame}
\end{document}

