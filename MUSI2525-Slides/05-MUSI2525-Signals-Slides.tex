% move all configuration stuff into one file so we can focus on the content
\input{common}


\subtitle{Part 5: Signals}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
    % generate title page
	\input{titlepage}

    \section[intro]{introduction}
        \begin{frame}{signals}{introduction}
            \question{what is an audio signal}
            
            \begin{itemize}
                \item   a representation of sound (speech, music, etc.)
                \item   usually sound pressure variations modeled by voltage variations
                \item   usually containing frequencies between \unit[20]{Hz}--\unit[20]{kHz}
            \end{itemize}
            
            \pause
            \question{what is the difference between an analogue and a digital signal}
            
            \begin{itemize}
                \item   amplitude continuity
                \item   time continuity
                \pause
                \item   here: analogue signals
            \end{itemize}
        \end{frame}
    \section[properties]{signal properties}
        \begin{frame}{signals}{properties of real-world signals}
            \begin{itemize}
                \item   \textbf{real-valued}:
                    \begin{itemize}
                        \item   real-world signals are usually real-valued.
                    \end{itemize}
                \item<2->   \textbf{finite}:
                    \begin{itemize}
                        \item   amplitude: $max|x(t)|<\infty$ 
                        \item<3->   energy or power:
                            \begin{eqnarray*}
                                E &=& \int\limits_{-\infty}^{\infty}{x^2(t) dt}\\
                                P &=& \lim_{T \rightarrow \infty}\frac{1}{2T}\int\limits_{-T}^{T}{x^2(t) dt}
                            \end{eqnarray*}
                    \end{itemize}
                \item<4->   \textbf{smooth}:
                    \begin{itemize}
                        \item  no ``abrupt'' changes $\rightarrow$ finite bandwidth 
                    \end{itemize}
            \end{itemize}
        \end{frame}
        \begin{frame}{signals}{properties of theoretical signals}
            \toremember{signal properties}
            while the above restrictions apply for real-world signals, they are not necessarily true for theoretical signals
            \begin{itemize}
                \item   real-valued:\\ use of complex numbers as a mathematical tool
                \smallskip
                \item   finite:\\ energy is violated frequently, power not so much
                \smallskip
                \item   smoothness:\\ many theoretical signals are not smooth
            \end{itemize}
        \end{frame}
        %\begin{frame}{signals}{description}
            %\begin{itemize}
                %\item   energy and power
                %\item   mean - DC
                %\item   pulse train
            %\end{itemize}
        %\end{frame}
    \section[categorization]{categorization}
        \begin{frame}{audio signals}{categorization}
            \begin{itemize}
                \item	\textbf{deterministic signals}:\\
                        \textit{predictable}: future shape of the signal can be known (example: sinusoidal)
                \bigskip		
                \item<2->	\textbf{random signals}:\\
                        \textit{unpredictable}: no knowledge can help to predict what is coming next (example: white noise)
            \end{itemize}
            \bigskip
            \only<3->{
            
            Every ``real-world'' audio signal can be modeled as a time-varying combination of 
            \begin{itemize}
                \item	(quasi-)periodic parts
                \item	(quasi-)random parts
            \end{itemize}
            }
            \vspace{50mm}
        \end{frame}

        \begin{frame}{audio signals}{periodic signals 1/3}
            \vspace{-3mm}
            periodic signals most prominent examples of deterministic signals: 
            \begin{eqnarray}
                x(t) 	&=& x(t+T_0)\nonumber\\
                f_0 	&=& \frac{1}{T_0}\nonumber\\
                \omega_0&=& \frac{2\pi}{T_0}\nonumber
            \end{eqnarray}
            \vspace{-10mm}
            \pause
            
            \begin{figure}
                \centering
                    \includegraphics[height=5cm,width=\textwidth]{periodic}
            \end{figure}
        \end{frame}
        \begin{frame}{signals}{periodic signals --- examples}
            \question{what periodic signals have we seen so far}
            \begin{itemize}
                \item   \textbf{sinusoidal}
                    \[x(t) = \sin(\underbrace{2\pi f}_{\omega} t {\color{gray}{+ \Phi}})\]
                \item<3->   \textbf{sawtooth}
                    \begin{equation*}
                        x(t) = 2\left(\frac{t}{T_0}-\floor\left(\frac{1}{2}+\frac{t}{T_0}\right)\right)
                    \end{equation*}
                \item<4->   \textbf{square wave}
                    \[x(t) = \sign\left(\sin(\omega t)\right)\]
            \end{itemize}
        \end{frame}
        \begin{frame}{signals}{non-periodic deterministic signals --- examples}
            \begin{itemize}
                \item   \textbf{DC}
                    \[ x(t) = 1\]
                \item<2->   \textbf{impulse}
                    \begin{equation*}
                        \delta(t) = 
                            \begin{cases}
                                    \infty & t = 0 \\
                                    0   & t \neq 0
                            \end{cases}
                    \end{equation*}
                    \begin{equation*}
                        \delta(t) = \int\limits_{-\infty}^{\infty}{\delta(t) dt = 1}
                    \end{equation*}
                \item<3->   \textbf{exponential}
                    \[ x(t) = \e^{-\alpha t}\]
            \end{itemize}
        \end{frame}
        \begin{frame}{signals}{non-periodic \& non-deterministic signals --- noise}
            \begin{itemize}
                \item   white noise
                    \begin{itemize}
                        \item   completely random: no possibility of predicting the event
                        \item<2-> but: if noise is stationary (doesn't change over time), then signal properties are predictable
                            \begin{itemize}
                                \item mean \[ m = \int\limits_{\infty}^{\infty}{x(t) dt}\]
                                \item   power
                            \end{itemize}
                    \end{itemize}
            \end{itemize}
        \end{frame}
    \section[manipulation]{signal manipulation}
        \begin{frame}{signals}{signal manipulation}
            \question{what are the mathematical formulations of the operations below}
            \begin{itemize}
                \item   scaling
                \item<2->[] $x(t) \rightarrow a\cdot x(t)$
                \item   flipping/reversing
                \item<2->[] $x(t) \rightarrow x(-t)$
                \item   time shift
                \item<2->[] $x(t) \rightarrow x(t-\tau)$               
                \item   time scaling
                \item<2->[] $x(t) \rightarrow x(a\cdot t)$
            \end{itemize}
        \end{frame}

    \section[summary]{summary}
        \begin{frame}{signals}{summary}
            \begin{itemize}
                \item   deterministic vs.\ random signals
                    \begin{itemize}
                        \item   periodic and non-periodic
                        \item   noise
                    \end{itemize}
                \item   real-world signal properties
                    \begin{itemize}
                        \item   finite
                        \item   real
                        \item   smooth
                    \end{itemize}
                \item   basic signal description
                    \begin{itemize}
                        \item   mean
                        \item   energy and power
                        \item   amplitude
                    \end{itemize}
            \end{itemize}
        \end{frame}
\end{document}

