% move all configuration stuff into includes file so we can focus on the content
\input{include}


\subtitle{coding style}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
    % generate title page
	\input{include/titlepage}


\section{introduction}
    \begin{frame}\frametitle{coding style}\framesubtitle{introduction}
        coding style
        \begin{itemize}
            \item   defines a consistent set of conventions that increase
                \begin{itemize}
                    \item   \textbf{readability}
                    \item   \textbf{comprehensibility}
                    \item   \textbf{maintainability}
                \end{itemize}
                \pause
                \bigskip
            \item   covers
                \begin{itemize}
                    \item   code \textbf{layout}
                    \item   \textbf{naming} conventions
                    \item   code \textbf{structure}
                    \item   \textbf{interface} conventions
                    \item   elimination of dangerous practice
                \end{itemize}
        \end{itemize}
    \end{frame}
    
\section{code layout}
    \begin{frame}\frametitle{coding style}\framesubtitle{code layout \& formatting}
        \begin{block}{theorem of formatting} 
            good visual layout shows the logical structure of the program
        \end{block}
        \begin{itemize}
            \item   code hierarchy
            \item   grouping of similar variables/methods/operations
        \end{itemize}
        \pause
        \bigskip
        \addsrc{human\_vs\_compiler}
    \end{frame}

    \begin{frame}\frametitle{coding style}\framesubtitle{code layout \& formatting: indentation}
        \vspace{-3mm}
        \begin{itemize}
            \item<1->  example of problematic indentation
                \only<1>{\addsrc{indent\_error}}
            \item<2-> indentation examples
                \only<2>{\addsrc{indent\_styles}}
            \item<3-> common indentation styles
                \begin{itemize}
                    \item<3->   Kernel style
                    \only<3>{\addsrc{one\_true\_brace}}
                    \item<4->   Whitesmiths style
                    \only<4>{\addsrc{whitesmiths\_style}}
                    \item<5->   Gnu style
                    \only<5>{\addsrc{Gnu\_style}}
                    \item<6->   Allman style
                    \only<6>{\addsrc{allman\_style}}
                \end{itemize}
            \item<7-> indent: tabs vs.\ spaces
        \end{itemize}
    \end{frame}
    \begin{frame}\frametitle{coding style}\framesubtitle{code layout \& formatting: vertical alignment}
        \addsrc{vertical\_alignment}
        \pause
        \begin{itemize}
            \item   \textbf{advantages}
                \begin{itemize}
                    \item   grouping of related statements
                \end{itemize}
            \item   \textbf{disadvantages}
                \begin{itemize}
                    \item   adding a new statement with a long variable may require changing the column width
                    \item   refactoring may break the formatting 
                    \item   reliance on fixed-width font
                \end{itemize}
        \end{itemize}
    \end{frame}
    \begin{frame}\frametitle{coding style}\framesubtitle{code layout \& formatting: spaces}
        \addsrc{spaces}
    \end{frame}

\section{naming conventions}
    \begin{frame}\frametitle{coding style}\framesubtitle{naming conventions 1/2}
        \addsrc{descriptive\_variable\_names}
        \pause
        \bigskip
        \begin{itemize}
            \item   \textbf{descriptiveness vs.\ length}
                \begin{itemize}
                    \item   pro descriptive
                        \begin{itemize}
                            \item   longer name/multi-word name conveys more information
                        \end{itemize}
                    \item   pro short
                        \begin{itemize}
                            \item   faster to type (but: autocomplete)
                            \item   visual clutter
                        \end{itemize}
                \end{itemize}
        \end{itemize}
    \end{frame}
    \begin{frame}\frametitle{coding style}\framesubtitle{naming conventions 2/2}
        \begin{itemize}
            \item   \textbf{naming consistency}: conventions for naming of
                \begin{itemize}
                    \item   non-trivial variables (piVariableName : \textit{typeMultiWord})
                    \item   members (m\_fMyMember)
                    \item   functions (computeFft : \textit{verbNoun})
                    \item   classes (CMyClass : \textit{CMultiWord})
                    \item   constants (kParamDelay : \textit{kMultiWord})
                    \item   pre-processor variables (WITH\_SNDLIB : \textit{MULTI\_WORD})
                \end{itemize}
            \bigskip
            \item<2->    \textbf{concatenation} conventions
                \begin{itemize}
                    \item   CamelCase
                    \item   snake\_case
                \end{itemize}
        \end{itemize}
    \end{frame}
    
    \begin{frame}\frametitle{coding style}\framesubtitle{naming conventions: hungarian notation 1/2}
        \begin{itemize}
            \item   \textbf{principle}: prefix encodes the actual data type
                \begin{itemize}
                    \item  examples
                        \begin{itemize}
                            \item   bBusy (boolean)
                            \item   pFoo (pointer to Foo)
                            \item   nSize (integer)
                            \item   szString (zero-terminated string)
                            \item   fnFunction (function name)
                        \end{itemize}
                \end{itemize}
        \end{itemize}
            Linus Torvalds:
                \begin{quote}
                    Encoding the type of a function into the name (so-called Hungarian notation) is brain damaged --- the compiler knows the types anyway and can check those, and it only confuses the programmer.
                \end{quote}
    \end{frame}
    
    \begin{frame}\frametitle{coding style}\framesubtitle{naming conventions: hungarian notation 2/2}
        \begin{itemize}
            \item   \textbf{advantages}
                \begin{itemize}
                    \item   variable type can be seen from its name
                    \item   consistent variable names
                    \item   avoids naming collisions
                \end{itemize}
            \pause
            \bigskip
            \item   \textbf{disadvantages}
                \begin{itemize}
                    \item   redundant (type checking done by compiler)
                    \item   IDE can display variable type (usually mouse over)
                    \item   danger of inconsistency when code is ported/refactored
                    \item   slightly longer variable names
                \end{itemize}
        \end{itemize}
        
    \end{frame}

\section{code structure}
    \begin{frame}\frametitle{coding style}\framesubtitle{signatures}
        \begin{itemize}
            \item   \textbf{structural conventions}
                \begin{itemize}
                    \item   directories (src \& incl, diverse projects, lib vs. executable, etc.)
                    \pause
                    \item   files (file names, classes per file, etc.)
                    \pause
                    \item   order
                        \begin{itemize}
                            \item   pre-processor always at the begin of the cpp file
                            \pause
                            \item   include order: C++ headers, 3rd party headers, your project headers
                            \pause
                            \item   static const variables after pre-processor
                            \pause
                            \item   local variable declarations at begin of the scope
                            \pause
                            \item   class declaration clearly structured
                            \addsrc{class\_order}
                        \end{itemize}
               \end{itemize}
        \end{itemize}
    \end{frame}
    \begin{frame}\frametitle{coding style}\framesubtitle{signatures etc.}
        \begin{itemize}
            \item   conventions for \textbf{signatures}
                        \begin{itemize}
                            \item   return error or value or void
                            \item   order of arguments: output, input, length?
                            \item   by pointer or by reference
                        \end{itemize}
            \bigskip
            \item   conventions for \textbf{error processing}
                        \begin{itemize}
                            \item   error codes
                            \item   exceptions
                        \end{itemize}
            \bigskip
            \item   conventions for \textbf{memory handling}
                        \begin{itemize}
                            \item   symmetry in allocation/deallocation
                            \item   do not use 'alien' memory
                        \end{itemize}
        \end{itemize}
    \end{frame}

\section{elimination of dangerous practice}
    \begin{frame}\frametitle{coding style}\framesubtitle{elimination of dangerous practice}
        \vspace{-3mm}
        \begin{itemize}
            \item   use \textbf{named constants} instead of hard-coded numbers
                \begin{itemize}
                    \item   maintainability: changes of the value
                    \item   readability: meaning
                \end{itemize}
            \pause
            \item   \textbf{avoid redundant code}
                \begin{itemize}
                    \item   maintainability: double bug fixes
                \end{itemize}
            \pause
            \item   write \textbf{short functions}
                \begin{itemize}
                    \item   readability
                \end{itemize}
            \pause
            \item   \textbf{minimize scope} (avoid global variables)
                 \begin{itemize}
                    \item   maintainability: better control over who reads and writes
                \end{itemize}
           \pause
            \item   \textbf{prohibit multiple statements} in one line
                \begin{itemize}
                    \item   readability, debugging problems
                \end{itemize}
            \pause
            \item   \textbf{initialize} pointers to 0
            \pause
            \item   use a type identifier for each variable
            \addsrc{declaration\_confusion}
        \end{itemize}
    \end{frame}
    
    \begin{frame}\frametitle{coding style}\framesubtitle{helpful conventions}
        \begin{itemize}
            \item   \#define guard in header files
            \pause
            \item   avoid unnecessary includes (forward declarations)
            \pause
            \item   if using error codes, avoid doing complex initialization in constructor (google styleguide)
        \end{itemize}
        \pause
        \bigskip
        \textbf{useful references}
        \begin{itemize}
            \item   JUCE coding standards:\\ \url{https://juce.com/discover/stories/coding-standards}
            \item   Google style guide:\\ \url{https://google.github.io/styleguide/cppguide.html}
        \end{itemize}
    \end{frame}

\end{document}
