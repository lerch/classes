% move all configuration stuff into includes file so we can focus on the content
\input{include}


\subtitle{testing}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
    % generate title page
	\input{include/titlepage}

\section{introduction}
    \begin{frame}\frametitle{testing}\framesubtitle{introduction 1/2}
        \begin{block}{\textbf{why tests}}
            %\begin{center}
                {The degree to which you know how your software behaves is the degree to which you have accurately tested it.}
            
            \flushright{\tiny Code Simplicity: The Fundamentals of Software}
        \end{block}
        
        \bigskip
        \pause
        \begin{itemize}
            \item programmers' lore:
                \begin{itemize}
                    \item   \textbf{law of conservation of bugs}:\\ number of bugs in a large system is proportional the the number of fixed bugs
                    \item   \textbf{ripple effect}:\\ high probability that the efforts to remove defects add new defects
                \end{itemize}
        \end{itemize}
    \end{frame}
    
    \begin{frame}\frametitle{testing}\framesubtitle{introduction 2/3}
        \begin{block}{hunt for defects early}
            \begin{center}
                \textbf{The earlier you identify defects, the less expensive is the fix.}
            \end{center}
            \begin{figure}
                \centering
                    \includegraphics[scale=.75]{graph/relative_cost_to_fix}
                \label{fig:fix}
            \end{figure}
        \end{block}
    \end{frame}
    
    \begin{frame}\frametitle{testing}\framesubtitle{introduction 2/3}
        \begin{figure}
            \centering
                \includegraphics[scale=.4]{graph/computer_problems}
            \label{fig:computer_problems}
        \end{figure}
    \end{frame}
    
    \begin{frame}\frametitle{testing}\framesubtitle{introduction 3/3}
        \vspace{-3mm}
        \begin{figure}
            \centering
                \includegraphics[scale=.1]{graph/red-build-determinism}
        \end{figure}
    \end{frame}

\section{definitions}
    \begin{frame}\frametitle{testing}\framesubtitle{test types}
        \vspace{-3mm}
        \begin{itemize}
            \item   \textbf{static testing} (code is analyzed)
                \begin{itemize}
                    \item   static code analysis (low cost, low benefit)
                    \item   code reviews/walk-throughs/inspections most helpful for
                        \begin{itemize}
                            \item   difficult API/algorithm
                            \item   inexperienced programmers
                            \item   area with catastrophic results in case of defects
                        \end{itemize}
                \end{itemize}
            \bigskip

            \item<2->   \textbf{dynamic testing} (program is executed)
                \begin{itemize}
                    \item   \textit{unit test}: functionality of section of code (class level) 
                    \pause
                    \item   \textit{integration test}: interaction between integrated components (interface level)
                    \pause
                    \item   \textit{system test}: overall specifications/requirements (system level)
                    \pause
                    \item   \textit{regression test}: finding defects after code changes
                    \pause
                    \item   \textit{alpha test}: simulated or actual operational testing by potential users
                    \pause
                    \item   \textit{beta test}: user acceptance testing
                    \pause
                    \item   (\textit{acceptance testing}: performed by the customer)
                \end{itemize}
        \end{itemize}
    \end{frame}

    \begin{frame}\frametitle{testing}\framesubtitle{test categories}
        \vspace{-3mm}
        \begin{itemize}
            \item   \textbf{functional testing}
                    \begin{itemize}
                        \item   \textit{verification}: have we built the software right?
                        \pause
                        \item   \textit{validation}: have we built the right software?
                     \end{itemize}  
            \bigskip

            \item<2->   \textbf{non-functional testing}
                \begin{itemize}
                    \item   \textit{stability testing}:
                        \begin{itemize}
                            \item   destructive testing: intention of making the software crash/misbehave --- invalid inputs
                            \item   long-term testing: run for extended periods of time
                            \item   scalability testing: how does the software behave with many users/on slow machines, what about workload peaks
                        \end{itemize}
                    \pause
                    \item   \textit{security testing}:\\ how accessible are data/transmissions/internal states
                    \pause
                    \item   \textit{usability testing}:\\ how easy to use and understand is the interface
                    \pause
                    \item   internationalization/localization
                \end{itemize}    
        \end{itemize}
    \end{frame}

    \begin{frame}\frametitle{testing}\framesubtitle{testing methods}
        \vspace{-3mm}
        \begin{itemize}
            \item   \textbf{black box}: testers outside $\rightarrow$ focus on \textit{input and output}
                \begin{itemize}
                    \item   functionality
                    \item   user input validation
                    \item   output results (numerical values)
                    \item   state transitions
                    \item   disadvantage: unknown coverage
                    
                \end{itemize}
            \bigskip

            \item<2->   \textbf{white box}: tester inside $\rightarrow$ focus on \textit{implementation}
                \begin{itemize}
                    \item   coverage: all different branches
                    \item   error handling
                    \item   handling of resource constraints
                \end{itemize}
            \bigskip

            \item<3->   \textbf{gray box}: tester can peek under the covers  $\rightarrow$ focus on \textit{architecture}
                \begin{itemize}
                    \item   protocols and transmission data
                    \item   system-added information (checksums, hashes, time stamps)
                    \item   memory leaks, temporary files, etc.
                    
                \end{itemize}
        \end{itemize}
    \end{frame}

\section{rules and procedures}
    \begin{frame}\frametitle{testing}\framesubtitle{concepts \& procedures 1/2}
        \vspace{-3mm}
        \begin{itemize}
            \item   \textbf{specification}
                \begin{itemize}
                    \item   avoid \textit{trivial} tests
                    \item<2->   keep tests \textit{short and simple}
                    \item<3->   avoid inter-test \textit{dependencies}
                    \item<4->   ask a very \textit{precise questions} and verify a very precise answer
                    \item<5->   avoid non-reproducible tests: internal state has to be identical each run
                    \item<6->   avoid \textit{inaccurate} tests: must not imply that software behaves properly when it is not
                \end{itemize}
            \bigskip

            \item<7->   \textbf{concepts}
                \begin{itemize}
                    \item   try to cover as much of your code as possible (\textbf{coverage})
                    \item<8->   remember: test code at least as buggy as real code
                    \item<9->   rule of thumb: ``every line of code takes 3--5 lines of test code''
                    \item<10->   failing tests have highest priority
                \end{itemize}
         \end{itemize}
    \end{frame}

    \begin{frame}\frametitle{testing}\framesubtitle{concepts \& procedures 2/2}
        \vspace{-3mm}
        \begin{itemize}
            \item   \textbf{targets}
                \begin{itemize}
                    \item   functionality/output
                    \item<2->   invalid API parameters, data, and initialization
                    \item<3->   boundary and unusual cases (channel config, block lengths)
                    \item<4->   race conditions: events do not happen in the anticipated order
                    \item<5->   (defects in handling stress)
                \end{itemize}
        \end{itemize}
        \bigskip
        
        \visible<6->{
        \begin{block}{but keep in mind}
            \begin{center}
                \textbf{Successful testing is no guarantee that the system will work!}
            \end{center}
        \end{block}
        }
    \end{frame}

    \begin{frame}\frametitle{testing}\framesubtitle{execution}
        \vspace{-3mm}
        \begin{itemize}
            \item \textbf{when}
                \begin{itemize}
                    \item   execute after each code modification
                \end{itemize}
            \bigskip

            \item<2->   \textbf{where}
                \begin{itemize}
                    \item   different compilers
                    \item   different hardware/OS
                    \item   different environments (e.g., plugin hosts)
                \end{itemize}
            \bigskip

            \item<3->   \textbf{how}
                        \begin{itemize}
                            \item   \textit{manually}
                                \begin{itemize}
                                    \item   slow 
                                    \item   error prone/incomplete
                                    \item   difficult to repeat
                                \end{itemize}
                            \item<2->   \textbf{automatic}
                                \begin{itemize}
                                    \item   weekly/daily/hourly
                                    \item   continuous
                                    \item   more development effort
                                \end{itemize}
                        \end{itemize}
        \end{itemize}
    \end{frame}
    
    \begin{frame}\frametitle{testing}\framesubtitle{execution}
        \begin{figure}
            \centering
                \includegraphics[scale=.75]{graph/Test-management}
        \end{figure}
    \end{frame}

\section{TDD}
    \begin{frame}\frametitle{testing}\framesubtitle{test-driven development}
        \vspace{-3mm}
        \begin{itemize}
            \item   \textbf{procedure}
                \begin{enumerate}
                    \item   break down the task (specification) into sub-tasks
                    \item<2->   specify \& write tests for all sub-tasks
                    \item<3->   ensure your tests fail
                    \item<4->   write the code to make your test pass
                    \item<5->   refactor
                    \item<6->   task completion: you have all the tests you need, and they all pass
                \end{enumerate}
        \end{itemize}

        \bigskip
        \visible<7->{
        \begin{block}{tdd rules and assumptions}
                \begin{enumerate}
                    \item   all tests should \textbf{fail} before your implement the code
                    \item   anything beyond the tested functionality is \textbf{unimportant}
                    \item   always implement the \textbf{simplest code possible} to make your tests pass
                \end{enumerate}
        \end{block}
        }
        
    \end{frame}
\end{document}
