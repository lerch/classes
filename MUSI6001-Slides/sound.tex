\subsection[sound]{nature of sound}
\begin{frame}{the nature of sound}{sound 1/2}
    \begin{columns}[c]
        \column{0.6\textwidth}
            \begin{enumerate}
                \item   sound originates from  the \textbf{motion or vibration of an object}
                \uncover<2->{
                \item   \textbf{motion is impressed upon surrounding medium} (air)
                }
                \uncover<3->{
                \item   \textbf{change in atmospheric pressure} creates sound
                }
                \uncover<4>{
                \item   sound \textbf{cannot propagate without medium}
                }
            \end{enumerate}
        \column{0.5\textwidth}
            \vspace{-5mm}\flushright{\includegraphics[scale=0.5]{graph/belllabs-sound}}
    \end{columns}
\end{frame}

\begin{frame}{the nature of sound}{sound 2/2}
    \begin{figure}
        \centering
        \includevideo{video/bellinvacuum.mp4}
    \end{figure}
\end{frame}
\begin{frame}{the nature of sound}{sound propagation}
    \begin{itemize}
        \item   wave travels away from source: \textbf{longitudinal wave}
        \only<1>{ \includevideo{video/longitudinalwaves.mp4}}
        
        \pause
        \item   \textbf{molecules do not advance} with wave but vibrate around resting place
        \item   motion provokes \textbf{compression/rarefaction} of air molecules
        
        \vspace{50mm}
    \end{itemize}
\end{frame}
\begin{frame}{the nature of sound}{sound pressure 1/3}
    \vspace{-5mm}
    \begin{itemize}
        \item   \textbf{compression}: masses are pushed together
        \item   \textbf{rarefaction}: masses are pulled apart
        \item   masses form the propagating medium
    \end{itemize}
        \begin{figure}
            \centering
            \animategraphics[height=2in,autoplay,loop]{19}{graph/sound_propagation/Naval-Research-Lab-}{00}{19}        
        \end{figure}
\end{frame}
\begin{frame}{the nature of sound}{sound pressure 2/3}
    \begin{figure}
        \centering
        \includegraphics[scale=0.5]{graph/sinusoidalpressure}
    \end{figure}
\end{frame}
\begin{frame}{the nature of sound}{sound pressure 3/3}
    \vspace{-5mm}
    \begin{itemize}
        \item   molecules can be modeled by spring-mass model 
        \item[$\Rightarrow$] sinusoidal oscillation
         \includevideo{video/simpleHarmonicMotion.mp4}
    \end{itemize}
\end{frame}
\begin{frame}{the nature of sound}{speed of sound}
    \vspace{-5mm}\begin{itemize}
        \item   practical formula for dry air
            \begin{eqnarray}
                c_\mathrm{air} &=& 331.3\frac{m}{s} \cdot \sqrt{\frac{\vartheta + 273.15}{273.15}}\\
                c_\mathrm{air} &\approx& 331.3 + 0.6\cdot\vartheta \quad [\frac{m}{s}]
            \end{eqnarray}
        \pause
        \vspace{-5mm}
        \begin{columns}
        \column{.4\textwidth}
            \item[]   approximation fails for 
                \begin{itemize}
                    \item   low pressure
                    \item   high temperatures
                    \item   short wavelengths
                \end{itemize}
        \column{.4\textwidth}
            \begin{figure}
                \includegraphics[scale=.25]{graph/speed-of-sound}
            \end{figure}
        \end{columns}
    \end{itemize}
\end{frame}
\begin{frame}{the nature of sound}{questions}
    \begin{enumerate}
        \item   In what time does sound cover a length of \unit[10]{m}?
        \pause
        \only<2>{
        \begin{equation}
            t = \frac{\Delta l}{c} = \unit[29]{ms}
        \end{equation}
        }
        \pause
        \item   What is the difference between wave length and period length?
        \pause
        \only<4>{
        \begin{itemize}
            \item   wave length: minimal distance of identical oscillation state at a fixed point in time
            \item   period length: time between identical oscillation state at a fixed point in space
        \end{itemize}
        }
        \pause
        \item   What are the wave lengths for \unit[16]{Hz},\unit[1000]{Hz},\unit[10000]{Hz}
        \pause
        \only<6>{
        \begin{eqnarray}
            \lambda_{16} &=& \unit[21.25]{m}\\
            \lambda_{1000} &=& \unit[0.34]{m}\\
            \lambda_{10000} &=& \unit[3.4]{cm}
        \end{eqnarray}
        }
        \pause    
        \item   What is the frequency of a pitch A4 (440Hz @ 14.35C) for the following temperatures: 16, 19,22 C
        \pause
        \begin{itemize}
            \item   compute speed
            \item   compute wave length at 14.35C
            \item   compute frequencies
        \end{itemize}
        \pause
        \begin{eqnarray}
            f_{16} &=& \unit[442.82]{Hz}\\
            f_{22} &=& \unit[445.12]{Hz}\\
            f_{22} &=& \unit[447.39]{Hz}
        \end{eqnarray}
    \end{enumerate}
\end{frame}

\begin{frame}{the nature of sound}{sine wave}
    \begin{itemize}
        \item   very fundamental type of sound 
        \item   three properties
            \begin{itemize}
                \item   period length / wave length
                \item   amplitude
                \item   phase
            \end{itemize}
    \begin{figure}
        \centering
        \includegraphics[scale=0.4]{graph/sinusoidal}
    \end{figure}
    \end{itemize}
\end{frame}
\begin{frame}{the nature of sound}{sine property: frequency}
    \vspace{-5mm}
    \only<1>{
    \begin{figure}
        \centering
        \includegraphics[scale=0.4]{graph/sinusoidal}
    \end{figure}
    }
    \vspace{-1mm}
    \textbf{frequency}: number of oscillations per time
    \begin{equation}
        f = \frac{1}{T}
    \end{equation}
    \pause
    \vspace{-2mm}
    \begin{table}[h]
        \begin{center}
        \begin{footnotesize}{\begin{tabular}{l|l|l} % \rule[-2mm]{0mm}{7mm}
        \textbf{frequency [\unit{Hz}]} & \textbf{pitch} & \textbf{instrument} \\ 
        \hline 
        $16.5$ & $C_0$ & pipe organ low C pedal \\ 
        \hline 
        $33$ & $C_1$ & five-stringed double bass: C-string \\
        \hline 
        $66$ & $C_2$ & violoncello: C-string  \\ 
        \hline 
        $131$ & $C_3$ & viola: C-string\\ 
        \hline 
        $262$ & $C_4$ & violin: lowest C \\ 
        \hline 
        $524$ & $C_5$ & tenor: high C \\ 
        \hline 
        $1047$ & $C_6$ & soprano: high C \\ 
        \hline 
        $2093$ & $C_7$ & violin: high C\\ 
        \hline 
        $4185$ & $C_8$ & piccolo: high C\\ 
        \end{tabular}}\end{footnotesize}
        \end{center}
    \end{table}
    \begin{itemize}
        \item 33: \includeaudio{audio/sine33.mp3}
        \item 4185:  \includeaudio{audio/sine4185.mp3}
    \end{itemize}
    
\end{frame}
\begin{frame}{the nature of sound}{sine property: amplitude}
    \only<1>{
    \begin{figure}
        \centering
        \includegraphics[scale=0.4]{graph/sinusoidal}
    \end{figure}
    }
    \textbf{amplitude}: highest value of a sinusoidal
    \begin{itemize}
        \item   not clearly defined for non-sinusoidals
        \item   (...the higher, the louder)
    \end{itemize}
\end{frame}
\begin{frame}{the nature of sound}{sine property: phasor representation}
        \begin{figure}
            \centering
            \animategraphics[height=1.5in]{5}{graph/phasor/frame_}{001}{013}        
        \end{figure}
\end{frame}
\begin{frame}{the nature of sound}{complex periodic sounds: addition of sinusoidals 1/2}
    \vspace{-5mm}
    \begin{itemize}
        \item   addition of two sinusoidals of 
        \begin{itemize}
            \item equal frequency
            \pause
            \item   different frequency (trigonometric identities)
                \begin{footnotesize}\begin{eqnarray}
                    x(t) &=& \sin\big(2\pi f_1 t\big) + \sin\big(2\pi (f_2) t\big)\\
                    &=& 2\sin\left(\frac{2\pi (f_1 + f_2) t }{2}\right) \cdot \cos\left(\frac{2\pi (f_1 - f_2) t }{2}\right)
                \end{eqnarray}\end{footnotesize}
                \vspace{-5mm}
                \begin{figure}
                    \centering
                        \includegraphics[height = 35mm, width=70mm]{graph/sine-addition}
                \end{figure}
        \end{itemize} 
    \end{itemize}
\end{frame}
\begin{frame}{the nature of sound}{complex periodic sounds: addition of sinusoidals}
	reconstruction of periodic signals with a limited number of sinusoidals:
	\begin {equation}
		\hat{x}(t) = \sum\limits_{k=-\mathcal{K}}^{\mathcal{K}} a(k) e^{j\omega_0kt}
	\end {equation}
	\only<1>{
	\begin{figure}
		\centering
			\includegraphics[height=4cm,width=\textwidth]{graph/additivesynthesis-1}
	\end{figure}
	}
	
    \vspace{-5mm}
	\setcounter{i}{1}
	\whiledo{\value{i}<6}	
	{
		\pause
		\only<\value{beamerpauses}>
		{
			\begin{figure}
			\centering
                %\includemovie[poster=graph/additivesynthesis_saw_\arabic{i}.pdf,autoplay=true]{\textwidth}{4cm}{audio/additivesynthesis_saw_\arabic{i}.mp3}
                \includegraphics[width=\textwidth, height=4cm]{graph/additivesynthesis_saw_\arabic{i}.pdf}
			\end{figure}
            \vspace{-5mm}
			\begin{figure}
			\flushright
                \includemedia[
                    addresource=audio/additivesynthesis_saw_\arabic{i}.mp3,
                    width=.3\textwidth,
                    height=3mm,
                    activate=pageopen,
                    flashvars={
                        source=audio/additivesynthesis_saw_\arabic{i}.mp3  
                        &autoPlay=true
                    }]
                    {}
                    {APlayer.swf}
			\end{figure}
		}
		\stepcounter{i} 
	}	
	\setcounter{i}{1}
	\whiledo{\value{i}<6}	
	{
		\pause
		\only<\value{beamerpauses}>
		{
			\begin{figure}
			\centering
                %\includemovie[poster=graph/additivesynthesis_rect_\arabic{i}.pdf,autoplay=true]{\textwidth}{4cm}{audio/additivesynthesis_rect_\arabic{i}.mp3}
                \includegraphics[width=\textwidth, height=4cm]{graph/additivesynthesis_rect_\arabic{i}.pdf}
 			\end{figure}
            \vspace{-5mm}
            \begin{figure}
			\flushright
               \includemedia[
                    addresource=audio/additivesynthesis_rect_\arabic{i}.mp3,
                    width=.3\textwidth,
                    height=3mm,
                    activate=pageopen,
                    flashvars={
                        source=audio/additivesynthesis_rect_\arabic{i}.mp3  
                        &autoPlay=true
                    }]
                    {}
                    {APlayer.swf}

 			\end{figure}
		}
		\stepcounter{i} 
	}	
\end{frame}
\begin{frame}{the nature of sound}{complex periodic sounds: vibrating string 1/2}
    \only<1>{
    \begin{figure}
        \includevideo{video/stringstandingwaves.mp4}
    \end{figure}
    }
    \pause
    \begin{itemize}
        \item   fixed at both ends 
        \item   reflections back and forth
%        \item   combination of reflections create standing waves
        \pause
        \begin{figure}
            \centering
            \animategraphics[height=1in,autoplay,loop]{8}{graph/string/o_9ca19d2fcb185461-}{0}{15}        
        \end{figure}
        \pause
        \item   nodes and antinodes (peaks)
        \item   any vibrations must have nodes on ends $\rightarrow$ standing waves
        \item   all modes are multiples and fundamental frequency
    \end{itemize}
\end{frame}
\begin{frame}{the nature of sound}{complex periodic sounds: vibrating string 2/2}
    \begin{figure}
        \includegraphics[scale=.4]{graph/stringstandingwaves}
    \end{figure}
\end{frame}

\begin{frame}{the nature of sound}{harmonics and timbre}
    \begin{itemize}
        \item   terminology: 
            \begin{itemize}
                \item   \textbf{overtone}: components higher than fundamental frequency
                \item   \textbf{partials}: both fundamental and overtones
                \\item  \textbf{harmonics}: partials with frequencies at integer multiples of the fundamental frequency
            \end{itemize}
        \pause
        \bigskip
        \item   partials define \textit{part} of the sound quality: number, amplitude, frequency position
        \item   energy of partials comprises \textbf{spectral envelope}
    \end{itemize}
    \vspace{10mm}
\end{frame}
\begin{frame}{the nature of sound}{harmonic series and phase}
    \vspace{-6mm}
    \begin{itemize}
        \item[]   example: saw, 20 harmonics
    \end{itemize}
    \vspace{-3mm}
    \begin{figure}
        \includegraphics[scale=.4]{graph/sawphase}
    \end{figure}
    \vspace{-5mm}
    \begin{center}
        \includeaudio{audio/saw0.mp3}
        \includeaudio{audio/sawpi2.mp3}\\
        \includeaudio{audio/sawpi.mp3}
        \includeaudio{audio/sawrand.mp3} 
    \end{center}
\end{frame}
\begin{frame}{the nature of sound}{inharmonicity and noise}
    \begin{itemize}
        \item   some structures vibrate in a more complex way $\rightarrow$ modes are not harmonics
        \item[] \includeaudio{audio/freftri.mp3} 
        \pause
        \item   a composition of many unrelated frequencies is perceived as \textbf{noise}
        \pause
        \item   all frequencies uniformly distributed $\Rightarrow$ \textbf{white noise}
        \pause
        \item   some drums will 
            \begin{itemize}
                \item   give a pitched sensation due to a few harmonically related modes (timpani), or
                \pause
                \item   give a sensation of low and high due to band-limiting
            \end{itemize} 
    \end{itemize}
\end{frame}
\begin{frame}{the nature of sound}{real-world signals}
    \begin{itemize}
        \item   most signals are somewhere between sinusoidal and noise
            \begin{itemize}
                \item   \textbf{periodic}: total predictability
                \item   \textbf{random}: total unpredictability
            \end{itemize}
        \pause
        \bigskip
        \item   an event in music usually has a time-varying amplitude envelope
    \begin{figure}
        \includegraphics[scale=.4]{graph/adsr}
    \end{figure}
    \end{itemize}
\end{frame}

\begin{frame}{the nature of sound}{ADSR envelope model}
    \begin{figure}
        \includegraphics[scale=.4]{graph/adsr}
    \end{figure}
    \begin{enumerate}
        \item   \textbf{attack}: initial transient, begin of energy transfer (pluck, hit, bow)
        \item   \textbf{sustain}: steady-state, periodic part of event
        \item   \textbf{release}: time to silence after excitation stops
    \end{enumerate}
\end{frame}

\begin{frame}{the nature of sound}{magnitude spectrum examples 1/2}
    \begin{figure}
        \includegraphics[scale=.4]{graph/spectrumexamples}
    \end{figure}
\end{frame}

\begin{frame}{the nature of sound}{magnitude spectrum examples 2/2}
    \begin{figure}
        \includegraphics[scale=.2]{graph/spectrumwindowed}
    \end{figure}
\end{frame}

\begin{frame}{the nature of sound}{spectrogram}
    \begin{figure}
        \includegraphics[scale=.8]{graph/specgram}
    \end{figure}
    \vspace{5mm}
    \includeaudio{audio/alto-sax.mp3}
\end{frame}

\begin{frame}{the nature of sound}{sound pressure level: inverse square law}
    \begin{itemize}
        \item   microphones and measurement instruments pick up \textbf{changes in air pressure}
        \item   but: typical metric is the \textbf{intensity}: sound energy transmitted per second through a unit area
        \pause
        \item   point source:\\ sound propagates spherical ($4\pi r^2$)
                \only<2>{
                \vspace{-12mm}
                \begin{flushright}
                        \includegraphics[scale=.1]{graph/inversesquarelaw}
                \end{flushright}}
        \pause
        \bigskip
        \begin{block}{inverse square law} 
            intensity is inversely proportional to the square of the distance\\
            (pressure is inversely proportional to the distance)
        \end{block}
        \pause
        \smallskip
        \item   side note: intensity is established metric, but ear is \textbf{pressure receiver}
    \end{itemize}
    \vspace{20mm}
\end{frame}

\begin{frame}{the nature of sound}{sound pressure level: weber-fechner law}
    \begin{itemize}
        \item   auditory system can deal with a huge range of sound intensities
        \item   intensity perception follows Weber-Fechner law:
        \begin{block}{Weber-Fechner}
            \item[]   Weber: just-noticeable difference between two stimuli is proportional to the magnitude of the stimuli
            \begin{equation}
                \frac{\Delta I}{I} = \text{change in sensation} = \Delta S
            \end{equation}
            \item[]   Fechner: subjective sensation is proportional to the logarithm of the stimulus intensity
            \begin{equation}
                S = c\cdot \log(I) + C
            \end{equation}
        \end{block}
    \end{itemize}
\end{frame}

\begin{frame}{the nature of sound}{sound pressure level: bel and decibel}
    \begin{itemize}
        \item   Bel: logarithm of ratios with reference intensity $I_0$
        \item   deciBel: 10 decibel are one Bel
    \end{itemize}
    \pause
        \begin{eqnarray}
            SPL &=& 10\cdot \log_{10}\left(\frac{I}{I_0}\right)\\
             &=& 20\cdot \log_{10}\left(\frac{p_\mathrm{rms}}{p_{0,\mathrm{rms}}}\right)
        \end{eqnarray}
        
        \pause
        \begin{itemize}
            \item   decibel (dB) is based on ratio $\rightarrow$ \textbf{requires reference}!
            \item   \unit[10]{dB} represent a ratio of $10:1$
            \item   the sound pressure level is based on \textbf{temporal integration}, not instantaneous measurements
        \end{itemize}
\end{frame}

\begin{frame}{the nature of sound}{sound pressure level: reference value}
    \begin{itemize}
        \item   common reference level: 
            \begin{eqnarray}
                I_0 &=& 10^{-12} \frac{Watts}{m^2} \\
                p_0 &=& 2\cdot 10^{-5} \frac{N}{m^2} = 20 \mu Pa
            \end{eqnarray}
        \pause
        \item   note: average sea level pressure \unit[101,325]{Pa}
    \end{itemize}
\end{frame}

\begin{frame}{the nature of sound}{sound pressure level: examples}
    \begin{figure}
        \includegraphics[scale=.3]{graph/levelexamples}
    \end{figure}
\end{frame}
\begin{frame}{the nature of sound}{other dB measures}
    \begin{flushright}
         \includegraphics[scale=.08]{Graph/question-mark}
    \end{flushright}
    \vspace{-10mm}
    \textbf{What other dB values do you know except dBSPL?}
    \pause
    \begin{itemize}
        \item   dBA/B/C
        \item   dBFS (full scale)
        \item   dBV (1V)
        \item   dBu (0.775V)
    \end{itemize}
    \pause
    \bigskip
    \textbf{What does dB without index mean?}
    \pause
    \begin{itemize}
        \item   level difference
    \end{itemize}
    
\end{frame}
\begin{frame}{the nature of sound}{sound pressure level: two sources}
    \begin{equation}
        z(t) = x(t) + y(t)
    \end{equation}
    \begin{itemize}
        \item   SPL is a function of RMS, not amplitude
            \begin{itemize}
                \item   signals identical: 
                    \begin{equation}
                        z_\mathrm{rms} = 2\cdot x_\mathrm{rms}
                    \end{equation}
                \item   signals uncorrelated: 
                    \begin{equation}
                        z_\mathrm{rms} = \sqrt{2}\cdot x_\mathrm{rms}
                    \end{equation}
            \end{itemize}
            \begin{figure}
                \includegraphics[scale=.3]{graph/leveladd}
            \end{figure}
    \end{itemize}
\end{frame}
\begin{frame}{the nature of sound}{sound pressure level: identical source phase shifted}
    \begin{figure}
        \includegraphics[scale=.4]{graph/signaladd}
    \end{figure}
\end{frame}
\begin{frame}{the nature of sound}{sound pressure level: multiple sources}
    \begin{itemize}
        \item   multiple uncorrelated sources
            \begin{equation}
                z_\mathrm{rms} = \sqrt{\sum_i{x^2_\mathrm{i,rms}}} 
            \end{equation}
        \pause
            \begin{equation}
                L_z = 10log_{10}\left(\frac{\sum_i{x^2_\mathrm{i,rms}}}{p_0}\right)
            \end{equation}
    \end{itemize}
\end{frame}
\begin{frame}{the nature of sound}{sound pressure level: questions}
    \begin{flushright}
         \includegraphics[scale=.08]{Graph/question-mark}
    \end{flushright}
    \vspace{-8mm}
    \begin{itemize}
        \item   two microphones (60cm and 180cm distance)\\ What is the level difference?
        \pause
        \begin{equation}
            20\log_{10}\left(\frac{\nicefrac{1}{60}}{\nicefrac{1}{180}}\right) = 9 dB
        \end{equation}
        \item   level increase between 5 and 10 violins (same level)
        \pause
        \begin{equation}
            10\log_{10}\left(10\right) - 10\log_{10}\left(5\right) = 3 dB
        \end{equation}
    \end{itemize}
\end{frame}
