\subsection{introduction}
\begin{frame}{masking}{introduction}
    \vspace{-7mm}
    \begin{flushright}
         \includegraphics[scale=.08]{Graph/question-mark}
    \end{flushright}
    \vspace{-10mm}
    \textbf{What is masking? Name examples.}
    \pause
    \begin{itemize}
        \item   masking is an everyday phenomenon:
            \begin{itemize}
                \item   conversation (maskee) on street with loud traffic (masker)
                \item   music (maskee) listening with A/C on (masker)
                \item   in music: instrumentation so that one instrument cannot be recognized but changes sound quality
            \end{itemize}
        \pause
        \item   terminology
            \begin{itemize}
                \item   masked threshold: SPL of a test sound necessary to be just audible in the presence of a masker
                    \begin{itemize}
                        \item   always above ATH
                        \item   identical with ATH if masker and maskee are very different
                    \end{itemize}
                \item   partial masking 
                    \begin{itemize}
                        \item   reduces the loudness of a test stimulus but does not mask it completely (e.g.: conversation)
                    \end{itemize}
                \item   simultaneous masking vs. temporal masking
                    \begin{itemize}
                        \item   simultaneous: masker and maskee appear at the same time
                        \item   temporal: masker and maskee occur at different times
                    \end{itemize}
            \end{itemize}
    \end{itemize}
\end{frame}

\subsection{simultaneous masking}
\begin{frame}{simultaneous masking}{concepts of masking}
    \begin{itemize}
        \item   swamping: masker produces so much activity that activity of maskee remains undetected
        \bigskip
        \pause
        \item   suppression: masker suppresses maskee activity (two tone suppression)
            \begin{itemize}
                \item   stimulus at characteristic frequency, second tone just outside the excitatory area
                \item[$\Rightarrow$] reduction of CF firing rate
            \end{itemize}
        \pause
        \bigskip
        \item   note: most masking phenomena more likely related to swamping than suppression
    \end{itemize}
\end{frame}

\begin{frame}{simultaneous masking}{pure tones masked by broad-band noise}
    \vspace{-10mm}
    \begin{columns}
        \column{.65\textwidth}
            \begin{figure}
                \includegraphics[scale=.4]{graph/maskingtonebynoise}
            \end{figure}
        \column{.35\textwidth}
            \begin{flushright}\includeaudio{audio/zwicker-track-09.mp3}\end{flushright}
            \begin{itemize}
                \item   stimulus: white noise (\unit[63]{dB}) + triplets of pure tones (\unit[500]{Hz})
                \item   6 examples, triplets at \unit[51,37,46,34,43,40]{dB}
            \end{itemize}
    \end{columns}
    \pause
    \bigskip
    \begin{itemize}
        \item   description:
        \begin{itemize}
            \item   horizontal until about \unit[500]{Hz} (\unit[17]{dB} above density level)
            \item   thresholds rise with increasing frequency 
            \item   increasing the density level shifts up masked threshold linearly
        \end{itemize}
    \end{itemize}
\end{frame}
\begin{frame}{simultaneous masking}{pure tones masked by narrow-band noise 1/2}
    \vspace{-10mm}
    \begin{columns}
        \column{.6\textwidth}
            \begin{figure}
                \includegraphics[scale=.4]{graph/maskingtonebynarrownoise}
            \end{figure}
        \column{.4\textwidth}
            \begin{itemize}
                \item   stimulus 1: narrow-band noise (smaller than CB)
                \item   stimulus 2:\\ pure tone
            \end{itemize}
    \end{columns}
    \pause
    \begin{itemize}
        \item   frequency dependence of the masked threshold
        \item   maximum lower for higher center frequencies of the masker (-2dB at 250Hz, -5dB at 4khz)
        \item   very steep rise: 50--100dB per octave independent of level
    \end{itemize}
\end{frame}
\begin{frame}{simultaneous masking}{pure tones masked by narrow-band noise 2/2}
    \vspace{-10mm}
    \begin{columns}
        \column{.6\textwidth}
            \begin{figure}
                \includegraphics[scale=.4]{graph/maskingtonebynarrownoise2}
            \end{figure}
        \column{.4\textwidth}
            \begin{flushright}\includeaudio{audio/zwicker-track-10.mp3}\end{flushright}
            \begin{itemize}
                \item   stimulus: narrow-band noise (\unit[1]{kHz},\unit[70]{dB}) 
                \item   3 series of triplets (\unit[75,60,40]{dB}) at \unit[.6,.8,1,1.3,1.7,2.3]{kHz}
            \end{itemize}
    \end{columns}
    \pause
    \begin{itemize}
        \item   maximum always \unit[3]{dB} below  level of masking noise
        \item   level dependence towards high frequencies indicates non-linearity
        \item   dips: nonlinear effects (audible difference noises between test tone and noise)
    \end{itemize}
\end{frame}
\begin{frame}{simultaneous masking}{pure tones masked by pure tones 1/2}
    \vspace{-10mm}
    \only<1>{
    \begin{columns}
        \column{.6\textwidth}
            \begin{figure}
                \includegraphics[scale=.4]{graph/maskingtonebytoneproblem}
            \end{figure}
        \column{.4\textwidth}
            \begin{itemize}
                \item   beating (also at nonlinear multiples)
                \item   difference tones: test-tone at \unit[1.4]{kHz} $\rightarrow$ difference tone at $2f_m-f_t = \unit[600]{Hz}$
            \end{itemize}
    \end{columns}
    \vspace{70mm}
    }
    \uncover<2->{
        \begin{figure}
            \includegraphics[scale=.4]{graph/maskingtonebytone}
        \end{figure}
        \begin{itemize}
            \item   slope towards lower frequencies changes for decreasing masker level
            
            \item   maximum is lower compare to noise masker: \unit[-12]{dB}
        \end{itemize}
    }
\end{frame}
\begin{frame}{simultaneous masking}{pure tones masked by pure tones 2/2}
    \includevideo{video/simultaneousmasking.mp4}
    \url{http://youtu.be/2HDka1hYiCk}
\end{frame}
\begin{frame}{simultaneous masking}{pure tones masked by complex tones}
        \begin{figure}
            \includegraphics[scale=.6]{graph/maskingtonebycomplextone}
        \end{figure}
\end{frame}

\subsection{temporal masking}
\begin{frame}{temporal masking}{overview}
    \vspace{-5mm}
    \begin{figure}
        \includegraphics[scale=.4]{graph/maskingtemporal}
    \end{figure}
    \begin{itemize}
        \item   \textbf{post-masking (forward masking)}: probe follows the masker
            \begin{itemize}
                \item   not adaptation or fatigue because masker is short
                \item   possible explanations:
                    \begin{enumerate}
                        \item response of BM continues after excitation
                        \item   short-term adaptation/fatigue
                        \item   neural activity somehow persists in the higher auditory system and masks the probe
                    \end{enumerate}
            \end{itemize}
        \item   \textbf{pre-masking (backward masking)}: probe precedes the masker
            \begin{itemize}
                \item   poorly understood
                \item   effect decreases with practice
            \end{itemize}
    \end{itemize}

%zwicker 78
%moore129
%audio zwicker 12
\end{frame}
\begin{frame}{temporal masking}{description}
    \vspace{-7mm}
    \begin{figure}
        \includegraphics[scale=.4]{graph/maskingtemporal2}
    \end{figure}
    \vspace{-3mm}
    \begin{itemize}
        \item   \textbf{experiment}:
            \begin{itemize}
                \item   noise masker (\unit[10-50]{dB}) and \unit[4]{kHz} probe
                \item   left: results app.\ on straight line if delay on logarithmic scale
                \item   right: thresholds as function of masker level at specific delays
            \end{itemize}
        \item   \textbf{observations}:
            \begin{itemize}
                \item   forward masking increases with decreasing distance in time
                \item   rate of recovery is greater for higher masker levels
                \item   increments in masker level do not produce equal increments of threshold
                \item   amount of forward masking increases with masker duration up to \unit[20-50]{ms}
            \end{itemize}
    \end{itemize}

%zwicker 78
%moore129
%audio zwicker 12
\end{frame}


\begin{frame}{other phenomena}{pulsation threshold}
        \vspace{-5mm}
        \begin{figure}
            \includegraphics[scale=.4]{graph/pulsationthreshold}
        \end{figure}
        \begin{itemize}
            \item   \textbf{experiment}:
                \begin{itemize}
                    \item   two sounds presented alternately
                    \item[$\rightarrow$]   one sound can be perceived continuous
                    \item   not really masking effect but impression of continuity
                \end{itemize}
            \item   \textbf{audio example} \includeaudio{audio/zwicker-track-14.mp3}
                \begin{itemize}
                    \item   two sounds (music, noise) presented alternately
                    \item   \unit[5]{s} music, \unit[8]{s} gated music, \unit[8]{s} music and noise
                \end{itemize}
        \end{itemize}
\end{frame}

\subsection{masking models}
\begin{frame}{psycho-acoustic models}{MPEG-2 AAC suggested model: overview}
    \begin{enumerate}
        \item   frequency transformation:
            \begin{itemize}
                \item   FFT (2048/256), 50\% overlap, Hann window
            \end{itemize}
       \item unpredictability measure
            \begin{itemize}
                \item   predict magnitude and phase
                \item   derive a predictability measure from the residual
            \end{itemize}
        \item   transform spectrum and unpredictability to bark scale
        \item   convolve spectrum and unpredictability with spreading function
        \item   convert unpredictability to tonality index
        \item   compute the required SNR per band depending on tonality
        \item   apply THQ
        \item   compute the SMR
    \end{enumerate}
\end{frame}

%\subsection{masking phenomena}
%\begin{frame}{masking phenomena}{cocktail party effect}
%\end{frame}
