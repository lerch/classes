\begin{frame}{auditory system}{the ear}
    \begin{figure}
        \includegraphics[scale=.3]{graph/auditorysystem}
    \end{figure}
\end{frame}
\subsection{outer and middle ear}
\begin{frame}{auditory system}{outer ear}
    \vspace{-8mm}
    \begin{flushright}
        \includegraphics[scale=.3]{graph/outerear}
    \end{flushright}
    \vspace{-24mm}
    \begin{itemize}
        \item   \textbf{pinna}: filters the sound
            \pause
            \begin{itemize}
                \item   changes mostly high frequencies
                \item   location-dependent transfer function
                \item   unique
            \end{itemize}
        \pause
        \bigskip
        \item   \textbf{meatus}: sound travels to eardum/tympanic membrane
            \pause
            \begin{itemize}
                \item   damage protection: ear drum and middle ear protected
                \pause
                \item   signaling time optimization: inner ear closer to the brain
                \pause
                \item   resonance filter: 2--5kHz (model: tube 2--3cm)
                    \vspace{-1mm}
                    \begin{figure}
                        \includegraphics[scale=.25]{graph/meatusresonance}
                    \end{figure}
            \end{itemize}
    \end{itemize}
\end{frame}
\begin{frame}{auditory system}{middle ear 1/2}
    \vspace{-8mm}
    \begin{flushright}
        \includegraphics[scale=.35]{graph/middleear}
    \end{flushright}
    \vspace{-20mm}
    
    \begin{itemize}
        \item   \textbf{function}:\\ transmit vibration from outer to inner ear
        \pause
        \item   \textbf{transfer function}:\\ pronounced @ 500--\unit[4000]{Hz}
        \pause
            \begin{flushright}
                \includegraphics[scale=.4]{graph/ossicles}
            \end{flushright}
            \vspace{-43mm}
        \item   \textbf{tympanic membrane}: 
            \begin{itemize}
                \item pressure receiver
            \end{itemize}
        \pause
        \item   \textbf{ossicles}: hammer, anvil, stirrup
            \begin{itemize}
                \item   transmit vibrations to oval window
                \item   lever system
                \item   impedance matching (air --- fluid)
                \item   smallest bones in the body
            \end{itemize}
        \pause
        \item   \textbf{oval window}
            \begin{itemize}
                \item   15--20$\times$ smaller than eardrum
            \end{itemize}
    \end{itemize}
    \vspace{20mm}
\end{frame}
\begin{frame}{auditory system}{middle ear 2/2}
            \begin{flushright}
                \includegraphics[scale=.4]{graph/ossicles}
            \end{flushright}
            \vspace{-40mm}
    \begin{itemize}
        \item   \textbf{muscles}: protection
            \begin{itemize}
                \item   eardrum and ossicles
                \item   tighten eardrum,\\ pull stirrup from window
                \item   does not work for impulsive sounds
                \item   attenuation: $\approx$\unit[12]{dB}
                \item   activation time: \unit[60--120]{ms}
            \end{itemize}
        \pause 
        \bigskip
        \item   \textbf{eustachian tube}: 
            \begin{itemize}
                \item   connected to upper throat region
                \item   opens when swallowing, yawning
                \item   regulates pressure to environment pressure
            \end{itemize}
    \end{itemize}
    \vspace{50mm}
\end{frame}

\subsection{inner ear}
\begin{frame}{auditory system}{inner ear: function and physiology}
    \vspace{-5mm}
    \begin{columns}
    \column{.7\textwidth}
    \begin{itemize}
        \item   \textbf{vestibular function}:
            \begin{itemize}
                \item   balance and orientation
            \end{itemize}
        \uncover<2->{
        \item   \textbf{cochlear function}:
            \begin{itemize}
                \item   convert pressure to neural impulses
                \item   send to various brain centers
                \item   ``frequency analyzer''
            \end{itemize}
        }    
        \uncover<3->{
        \item   \textbf{cochlea physiology}:
            \begin{itemize}
                \item   very hard bone
                \item   spiral tube: 
                    \begin{itemize}
                        \item   2.5 turns
                        \item   length \unit[3--3.5]{cm}
                        \item   \unit[10]{mm} at base
                    \end{itemize}
                \item   filled with fluid
                \item   2 membranes
                    \begin{itemize}
                        \item   Reissner (scalae vestibuli \& media)
                        \item   basilar (scalae media \& tympani)
                    \end{itemize}
            \end{itemize}
        }
    \end{itemize}
    \column{.5\textwidth}
    %\vspace{-30mm}
    \includegraphics[scale=.25]{graph/innerear}
    \end{columns}
\end{frame}
\begin{frame}{auditory system}{inner ear: processing steps}
    \vspace{-5mm}
    \begin{columns}
    \column{.7\textwidth}
    \begin{enumerate}
        \item  oscillation of stapes is transmitted to fluid in scala vestibuli
        \uncover<2->{
        \bigskip
        \item   basilar membrane oscillates due to travelling waves
        }    
        \uncover<3->{
        \bigskip
        \item   movement of the membrane is picked up by haircells
        }
    \end{enumerate}
    \column{.5\textwidth}
    %\vspace{-30mm}
    \includegraphics[scale=.25]{graph/innerear}
    \end{columns}
\end{frame}
\begin{frame}{auditory system}{inner ear: basilar membrane overview}
    \begin{columns}
    \column{.5\textwidth}
    \begin{itemize}
        \item   oscillates due to traveling wave
        \uncover<2->{
        \item   particular points have characteristic frequency
        \item   high freqs near oval window
        }
        \uncover<3->{
        \item   base: stiffer than apex
        \item[$\rightarrow$] absolute maximum depends on frequency of stimulation
        \item   nonlinear: vibration not directly proportional to input magnitude
        }
        \uncover<4->{
        \item   dimensions: 0.1mm $\times$ 0.5mm (base $\times$ apex)
        }
    \end{itemize}
    \column{.5\textwidth}
        \includegraphics[scale=.25]{graph/basilarmembrane}
    \end{columns}
\end{frame}
\begin{frame}{auditory system}{inner ear: basilar membrane 1/2}
    \begin{columns}
    \column{.5\textwidth}
    \begin{itemize}
        \item   topographical mapping: characteristic frequency (CF)
        \begin{figure}
            \includegraphics[scale=.25]{graph/bmfreq2place}
        \end{figure}
        
        \uncover<2->{
            \begin{center}
            \animategraphics[height=.4in,autoplay,loop]{10}{graph/bm2kHz/o_db63c02cfe2c2601-}{0}{15}        
            
            \animategraphics[height=.4in,autoplay,loop]{10}{graph/bm6kHz/o_66c6bd404a16a681-}{0}{15}  
            \end{center}
        }
    \end{itemize}
    \column{.5\textwidth}
        %\vspace{40mm}
%        \includegraphics[scale=.2]{graph/basilarmembrane}

        \begin{tikzpicture}[remember picture,overlay]
            \node[anchor=north east,yshift=-110pt,xshift=-25pt] at (current page.north east) {\includegraphics[scale=.2]{graph/basilarmembrane}};
            \node[anchor=north east,yshift=-50pt] at (current page.north east) {\includegraphics[scale=.2]{graph/bm-freqplacement}};
        \end{tikzpicture}
    \end{columns}
    \uncover<3->{
    \begin{itemize}
        \item   stimulus two sinusoids
            \begin{itemize}
                \item   separated frequencies: 2 vibration patterns
                \item   close frequencies: non-sinusoidal vibration
            \end{itemize}
    \end{itemize}
        }
\end{frame}
\begin{frame}{auditory system}{inner ear: basilar membrane 2/2}
    \begin{columns}
    \column{.5\textwidth}
        \begin{itemize}
            \item   BM can be approximated as set of bandpass filters
            \uncover<2->{
            \item   frequency position is roughly logarithmic
            }
            \uncover<3->{
            \item   impulse response at each freq point 
                \begin{itemize}
                    \item   roughly sinusoidal
                    \item   envelope roughly like gamma distribution
                \end{itemize}
                \begin{figure}
                    \includegraphics[scale=.4]{graph/gammatone}
                \end{figure}
            }
        \end{itemize}
    \column{.5\textwidth}
            \includegraphics[scale=.4]{graph/AuditoryFreqResp}
            \uncover<2->{
            \includegraphics[scale=.5]{graph/bmfreq}
            }
    \end{columns}
\end{frame}
\begin{frame}{auditory system}{gammatone filterbank}
    \begin{figure}
        \includegraphics[scale=.8]{graph/gammatonefilterbank}
    \end{figure}

    \begin{itemize}
        \item   auditory filters modeled by cascade of second order filters
    \end{itemize}
\end{frame}
\begin{frame}{auditory system}{inner ear: organ of Corti}
    \begin{columns}
    \column{.5\textwidth}
    \begin{itemize}
        \item   organ of Corti is placed on basilar membrane
        \uncover<2->{
        \item   vibration of BM creates shearing force between tectorial membrane and organ of corti
        }
        \begin{center}\animategraphics[height=.5in,autoplay,loop]{10}{graph/corti/o_640812aebaace32d-}{0}{11}  \end{center}
        \uncover<3->{
        \item   haircells are stimulated and send out impulses
        }
    \end{itemize}
    \column{.5\textwidth}
    \includegraphics[scale=.75]{graph/corti}

    \end{columns}
\end{frame}
\begin{frame}{auditory system}{inner ear: haircells}
        %\begin{figure}
            \vspace{-5mm}\includegraphics[scale=.25]{graph/haircells}
            %
            \hspace{20mm}\includegraphics[scale=.8]{graph/stereocilia}
            
            \begin{flushright}\includegraphics[scale=.3]{graph/haircellmodel}\end{flushright}
        \vspace{-32mm}
    \begin{itemize}
        \item   \textbf{inner haircells}
            \begin{itemize}
                \item   about 3500 with 40--60 hairs
                \item   one row
                \item   contacted by about 20 neurons each
            \end{itemize}
        \item   \textbf{outer haircells }
            \begin{itemize}
                \item   about 25000 with 100--140 hairs
                \item   3--5 rows
                \item   might be able to actively influence cochlea mechanics (tuning)
                \item   likely under control of higher centers
            \end{itemize}
    \end{itemize}
\end{frame}
\begin{frame}{auditory system}{inner ear: outer haircell amplification}
        \vspace{-8mm}
        \begin{itemize}
            \item   outer hair cells supply mechanical amplification
            \item   video: outer hair cell of a guinea pig with attached electrode
        \end{itemize}
    \includevideo{video/dancinghaircell.mp4}
    \url{http://youtu.be/Xo9bwQuYrRo}
\end{frame}
\begin{frame}{auditory system}{summary: video animation}
    \includevideo{video/auditorytransduction.mp4}
    \url{youtu.be/PeTriGTENoc}
\end{frame}

\begin{frame}{auditory system}{evoked otoacoustic emissions}
    also referred to as \textbf{cochlear echoes}
            \begin{itemize}
                \item   low-level click is \textbf{played back} by the ear (\unit[5--60]{ms} later)
                \item   must result in cochlea (delay length) 
                \pause
                \smallskip
                \item   non-linear level relationship
                \item   only specific frequencies 
                \item   usually below threshold of hearing
                \pause
                \bigskip
                \item[$\Rightarrow$] \textbf{active }biological processes
            \end{itemize}
\end{frame}

\begin{frame}{auditory system}{spontaneous otoacoustic emissions}
    spontaneous means: \textbf{no stimulus}
    \begin{figure}
        \includegraphics[scale=.2]{graph/otoacousticemission}
    \end{figure}
    \pause
    \begin{itemize}
        \item   more than \unit[50]{\%} of ears exhibit this
        \item   levels usually between \unit[-20-- -5]{dBSPL}
        \pause
        \item   levels can change over day, but frequencies are relatively stable
        \pause
        \item   emissions indicate \textbf{good hearing} in their frequency range
        \pause
        \item   tinnitus may be spontaneous emission, but there are many other causes
    \end{itemize}
\end{frame}
\begin{frame}{auditory system}{tuning curves}
    \vspace{-5mm}
    tuning curves visualize frequency selectivity of nerve fibers
    \pause
    \begin{columns}
    \column{.55\textwidth}
            \includegraphics[scale=.5]{graph/tuningcurves}
    \column{.45\textwidth}
    \begin{itemize}
        \item   plots \textbf{minimum stimulus level} required for firing
        \pause
        \item   one curve per neuron
        \pause
        \item   characteristic frequency at minimum level
        \item   place representation of frequency on BM is preserved in auditory nerve
    \end{itemize}
    \end{columns}
\end{frame}
\begin{frame}{auditory system}{iso-intensity contours}
    \begin{figure}
        \includegraphics[scale=.3]{graph/isointensitycontours}
    \end{figure}
    \begin{itemize}
        \item   plots firing rates as function of frequency
        \pause
        \item   note that frequency of maximal firing varies with level
    \end{itemize}

\end{frame}
\begin{frame}{auditory system}{firing of haircells}
    \begin{columns}
    \column{.4\textwidth}
    \includegraphics[scale=.5]{graph/haircellfiring}
    \column{.6\textwidth}
    \begin{itemize}
        \item   spontaneous firing rate:\\ 0.5 to \unit[250]{Hz}
        \pause
        \bigskip
        \item   high spontaneous firing rate indicates sensitivity
        \pause
        \bigskip
        \item   firing threshold varies by \unit[80]{dB} and more
    \end{itemize}
    \end{columns}
\end{frame}
\begin{frame}{auditory system}{phase locking}
    \vspace{-5mm}
    \begin{itemize}
        \item   not only \textbf{rate of firing} depends on stimulus
        \pause
        \item   time intervals between firings are integral multiples of the waveform period
        \begin{figure}
            \includegraphics[scale=.5]{graph/phaselocking}
        \end{figure}
        \pause
        \item   phase locking only occurs up to about \unit[4--5]{kHz}
        \\ above the firing rate is in the same range as the period length
    \end{itemize}
\end{frame}
\begin{frame}{auditory system}{neural encoding}
    \begin{itemize}
        \item   3 types of information
        \pause
            \begin{itemize}
                \item   firing rate
                \pause
                \item   temporal pattern (phase locking)
                \pause
                \item   total number of cells firing
            \end{itemize}
        \pause
        \bigskip
        \item   properties
        \pause
            \begin{itemize}
                \item   all or nothing firing (polarity change -70 to \unit[40]{mV})
                \pause
                \item   nerve conduction velocity: $\unit[2-120]{\frac{m}{s}}$
                \pause
                \item   maximum discharge rate: $\unit[500-1000]{\frac{spikes}{s}}$
            \end{itemize}
    \end{itemize}
\end{frame}
\begin{frame}{auditory system}{hair cell damage 1/2}
    \vspace{-5mm}
        \begin{figure}
            \includegraphics[scale=.7]{graph/haircelldamage}
        \end{figure}
\end{frame}
\begin{frame}{auditory system}{hair cell damage 2/2}
    \vspace{-5mm}
        \begin{itemize}
            \item   age-related hearing loss (presbycusis)
            \only<1>{
            \begin{figure}
                \includegraphics[scale=.7]{graph/agehearingloss}
            \end{figure}
            }
            \pause
            \item   very loud sounds can damage temporarily (stereocilia) or permanently (haircells)
                \begin{itemize}
                    \item   explosions, loud impulsive sounds
                        \only<2>{
                        \begin{figure}
                            \includegraphics[scale=.4]{graph/gunhearingloss}
                        \end{figure}
                        }
                    \pause
                    \item   exposure time $\times$ loudness
                \end{itemize}
        \end{itemize}
        \only<3>{
        \vspace{-5mm}
        \begin{figure}
            \includegraphics[scale=.3]{graph/dailynoiselimits}
        \end{figure}
        }
\end{frame}
\begin{frame}{auditory system}{other reasons for hearing loss}
    \begin{itemize}
        \item   rupture of tympanic membrane
            \begin{itemize}
                \item   causes: gun fire, sports (diving, martial arts, ...)
                \item   more than \unit[80]{\%} recover in 2--4 weeks
            \end{itemize}
        \pause
        \item   inherited genetic
        \pause
        \item   illness (menengitis, measles, mumps, strokes, ...)
        \pause
        \item   medications and chemicals
        \pause
        \item   physical trauma (ear or head injury)
    \end{itemize}
\end{frame}

\subsection{the musical brain}
\begin{frame}{auditory system}{auditory cortex}
    \begin{figure}
        \includegraphics[scale=.7]{graph/auditorycortex}
    \end{figure}
\end{frame}

\begin{frame}{auditory system}{lateralization}
    \begin{figure}
        \includegraphics[scale=.4]{graph/lateralization}
    \end{figure}
    \vspace{-5mm}
    \begin{itemize}
        \item   \textbf{left}: temporal analysis, lyrics, language
        \item   \textbf{right}: frequency, intonation, pitch contour
    \end{itemize}
\end{frame}

\begin{frame}{auditory system}{brain: structure and function}
    \begin{figure}
        \includegraphics[scale=.5]{graph/brainstructure}
    \end{figure}
  
\end{frame}

\begin{frame}{auditory system}{the musical brain}
    \vspace{-5mm}
    \begin{figure}
        \includegraphics[scale=.6]{graph/musicalbrain}
    \end{figure}
\end{frame}

