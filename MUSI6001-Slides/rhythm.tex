
\subsection{rhythm}
\begin{frame}{temporal processing}{rhythm --- introduction}
    \vspace{-5mm}
    \begin{block}{\textbf{generic definition} (Grove Music)}
       a movement marked by the regulated succession of strong or weak elements
    \end{block}
    \smallskip
    \begin{itemize}
        \item   rhythm is one of the \textbf{primary parameters of musical structure} ($+$ pitch)
            \begin{itemize}
                \item   changes in timbre, dynamics are understood as \textit{different arrangements of the same work}
                \item   changes in rhythm and pitch \textit{may} result in a different work
            \end{itemize}
        \pause
        \item   rhythm generally refers to regular temporal patterns, mostly duration and accents
        \pause
        \bigskip
        \item   \textbf{rhythm vs. musical structure/form}
            \begin{itemize}
                \item   rhythm is often understand as a quality within the span of the \textit{perceptual present}
                \item   structure involves \textit{long-term memory and musical knowledge}
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{temporal processing}{rhythm and duration}
    \vspace{-5mm}\begin{itemize}
        \item   perceptual \textbf{time interval ranges}
            \begin{itemize}
                \item   1--20$\unit{ms}$: detection of asynchronous events
                \item   20--50\unit{ms}: basic differentiation of events
                \item   $>100\unit{ms}$: duration with impact on rhythm
                \item   $>2\unit{s}$: two separated events without clear duration perception
                \item   $<5\unit{s}$: rhythm grouping must occur in short-term memory
            \end{itemize}
        \smallskip
        \pause
        \item   duration perception example: compare audio and score \\ listen to the length of pauses and notes \includeaudio{audio/zwicker-track-41.mp3}
        \only<2>{
            \begin{figure}
                \includegraphics[scale=.3]{graph/rhythmduration1}
            \end{figure}
            }
        \pause
        \only<3>{
            \begin{figure}
                \includegraphics[scale=.3]{graph/rhythmduration2}
            \end{figure}
            }
    \end{itemize}
    \vspace{50mm}
\end{frame}

\begin{frame}{temporal processing}{inter-onset intervals}
    \vspace{-5mm}
    \begin{itemize}
        \item   musical duration almost exclusively recognized from note \textbf{onset to onset}
            \begin{figure}
                \includegraphics[scale=.5]{graph/rhythmduration}
            \end{figure}
        \item[$\Rightarrow$]   IOI: time between neighboring onsets
        \pause
        \bigskip
        \item   \textit{perceived} IOI may differ from \textit{physical} IOI
            \begin{itemize}
                \item   \textsl{stacatto}: perceptually lengthened
                \item   \textbf{general tendency}: short intervals are overestimated and long intervals are underestimated
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{temporal processing}{rhythm and meter}
    \vspace{-5mm}
    \begin{itemize}
        \item   there is \textbf{no regular rhythm without meter}
        \item   meter and rhythm occur in most cases simultaneously and \textit{depend on each other}
        
        \smallskip
        \pause
        \item   \textbf{rhythm}: pattern of durations present in music
            \begin{quote}
                'that to which we attend'
            \end{quote}
         \item   \textbf{meter}: allows perception and \textit{anticipation} of rhythmic pattern
            \begin{quote}
                'the mode of attending'
            \end{quote}
            \vspace{-5mm}
            \begin{itemize}
                \item   group with a usually regular pulse (tactus: isochronous spacing of beats)
                \item   periodic alternation of strong and weak beats in a metrical hierarchy of two or more levels of beats
                \item   analogy: meter is to rhythm perception what scale is to pitch perception
                \item   strongly predictive
            \end{itemize}
        \pause
        \item  beat-keeping abilities are not much affected by musical training
    \end{itemize}
\end{frame}

\begin{frame}{follow-up}{beat-keeping abilities}
    \begin{quote}
        The most basic performance tests simply ask (...) to tap along to a simple stimulus. Such tests establish the upper and lower bounds for the production and maintenance of a beat, rate of tempo drift and so forth. Interestingly, they reveal \textbf{little effect of musical training on basic beat-keeping abilities}. In more complex contexts, however, including the perception and performance of polyrhythms (...) \textbf{musically skilled subjects perform significantly better than non-musicians}.
    \end{quote}
    
    Justin London, \url{http://www.oxfordmusiconline.com/subscriber/article/grove/music/45963pg3}
\end{frame}

\begin{frame}{follow-up: meter and tactus as adaptive process}
    \includevideo{video/coupledoscillators.mp4}
    
    \url{http://youtu.be/W1TMZASCR-I}
\end{frame}

\begin{frame}{temporal processing}{tempo and memory}
    \vspace{-5mm}
    \begin{figure}
        \includegraphics[scale=.5]{graph/temporeproduction}
    \end{figure}
    \vspace{-2mm}
    \begin{itemize}
        \item   subjects are asked to sing their favorite song
        \item   errors distribute around 10\% of the real value
        \pause
        \item[$\Rightarrow$] \textbf{tempo is (partly?) coded in an absolute way}
        \pause
        \item   JND:  listeners can detect about \unit[15]{ms} change in spacing at \unit[100]{BPM} (2.5\%)

    \end{itemize}
\end{frame}
\begin{frame}{temporal processing}{tempo}
    \vspace{-5mm}
    \begin{itemize}
        \item   tempo and meter allow listener to have \textbf{precise expectations as to when subsequent musical events will occur}
        \pause
        \item   tempo requires \textbf{awareness of beat/pulse} (range: \unit[100]{ms}--\unit[2]{s})
        \pause
        \item   tempo perception is not simply based on shortest/longest IOI
            \begin{itemize}
                \item   depends on total metric hierarchy
                \item    is usually between 60--150\unit{BPM}
            \end{itemize}
%        \item   perception of tempo is not simply based on the shortest/longest durations, it is usually between 60--150\unit{BPM} and depends on total metric hierarchy
            \begin{figure}
                \includegraphics[scale=.3]{graph/temporange}
            \end{figure}
    \end{itemize}
\end{frame}

\begin{frame}{temporal processing}{metric level 1/2}
    \vspace{-5mm}
    \begin{itemize}
        \item   metric event is accented depending on the number of levels on which it occurs \includeaudio{audio/beethoven5metriclevel1.mp3}
                    \includeaudio{audio/beethoven5metriclevel2.mp3}
    \end{itemize}
    \begin{figure}
        \includegraphics[scale=.4]{graph/metricaccents}
    \end{figure}
\end{frame}

\begin{frame}{temporal processing}{metric level 2/2}
    \begin{figure}
        \includegraphics[scale=.4]{graph/metricalhierarchy}
    \end{figure}
\end{frame}

\begin{frame}{temporal processing}{rhythm patterns and grouping 1/2}
    \vspace{-5mm}
    \begin{itemize}
        \item   two or more musical durations may cohere into a \textbf{rhythmic group}
        \pause
        \item   group boundaries are usually influenced by
            \begin{itemize}
                \item   \textbf{proximity}: note spacing
                \item   \textbf{similarity}: timbre, pitch, timbre, distance
                    \begin{figure}
                        \includegraphics[scale=.6]{graph/notegrouping}
                    \end{figure}
                \pause
                \item[$\rightarrow$] \textbf{proximity is of greater salience than similarity}
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{temporal processing}{rhythm patterns and grouping 2/2}
    \vspace{-5mm}
    \begin{itemize}
                \item   \textbf{simple duration relationships} are preferred (1:1, 1:2, 1:3)
                    \begin{itemize}
                        \item   easier to remember, categorize, and produce (subjects produce isochronous patterns even when asked to produce arrhythmic patters)
                    \end{itemize}
                \pause
                \item   rhythmic grouping is hierarchical (compare metrical level)
                    \begin{figure}
                        \includegraphics[scale=.7]{graph/rhythmhierarchical}
                    \end{figure}
                \pause
                \item   grouping and metric position have higher impact on form perception than harmonization, pitch
    \end{itemize}
\end{frame}

\begin{frame}{temporal processing}{accents 1/2}
    \vspace{-5mm}
    \begin{itemize}
        \item   accent articulated by \textit{durational}, \textit{dynamic}, and \textit{tonal} processes
        \pause
        \item   different categorizations
            \begin{itemize}
                \item   metric accent vs. rhythmic accent
                    \begin{itemize}
                        \item   \textbf{metric accent}: marks a beat as strong or salient (downbeat)
                        \item   \textbf{rhythmic accent}: marks one element in a series of durations the \textit{focal member}
                    \end{itemize}
                \item   Lehrdahl
                    \begin{itemize}
                        \item   \textbf{phenomenal accent} (immediate perception): emphasis of stress to a moment in the musical flow (e.g., \textsl{sforzandi})
                        \item   \textbf{metrical accent} (requires some time for perception): hierarchical metric beats
                        \item   \textbf{structural accent} (requires exposure to musical culture): caused by melodic, harmonic, ... \textit{points of gravity}
                    \end{itemize}
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{temporal processing}{accents 2/2}
    \vspace{-5mm}
    \begin{figure}
        \includegraphics[scale=.35]{graph/accents}
    \end{figure}
\end{frame}

\begin{frame}{temporal processing}{rhythm \& meter ambiguity 1/2}
    \vspace{-5mm}
    \begin{itemize}
        \item   rhythmic grouping is sometimes ambiguous
        \vspace{3mm}
             \begin{figure}
                \includegraphics[scale=.7]{graph/rhythmgroupambig}
            \end{figure}
            \pause
            \begin{figure}
                \includegraphics[scale=.5]{graph/syncopation0}
            \end{figure}

    \end{itemize}
\end{frame}

\begin{frame}{temporal processing}{rhythm \& meter ambiguity 2/2}
    \begin{itemize}
        \item   \textbf{syncopation}: placement of rhythmic stresses or accents where they wouldn't be expected \includeaudio{audio/holstsyncopation.mp3}
        \item   \textbf{hemiola}: 2:3 beats
    \end{itemize}
\bigskip
    \begin{figure}
        \includegraphics[scale=.45]{graph/syncopation}   
    \end{figure}
\end{frame}

\begin{frame}{temporal processing}{metrical tension}
    \vspace{-5mm}\begin{figure}
        \includegraphics[scale=.25]{graph/metricaltension}   
    \end{figure}
\end{frame}

\begin{frame}{temporal processing}{terminology summary}
    \begin{itemize}
        \item   \textbf{onset}: \only<2->{single musical event}
        \item   \textbf{tactus}: \only<3->{pulse of regularly recurring/expected events (beats)}
        \item   \textbf{beat}: \only<4->{point in time that builds tactus pulse}
        \item   \textbf{tempo}: \only<5->{rate of tactus def. by beat distance}
        \item   \textbf{accent}: \only<6->{increased weight of beat or onset}
        \item   \textbf{meter}: \only<7->{organization of beats into a cyclically repeating equidistant pattern with weak and strong events}
        \item   \textbf{downbeat}: \only<8->{strongly accented beat in a metrical cycle}
        \item   \textbf{measure}: \only<9->{western notation for meter}
        \item   \textbf{rhythm}: \only<10->{pattern of time intervals and accented events}
    \end{itemize}
\end{frame}

\subsection{performance analysis}
\begin{frame}{temporal processing}{expressive timing}
    \vspace{-5mm}
    \begin{itemize}
        \item   \textbf{macro-level}: variations in \textit{tempo} (\includeaudio{audio/abq.mp3} vs. \includeaudio{audio/hsq.mp3} vs. \includeaudio{audio/yq.mp3})
            \only<1>{
            \begin{figure}
                \includegraphics[scale=.7]{graph/upcibi128-m}
            \end{figure}
            \begin{figure}
                \includegraphics[scale=.3]{graph/beethovenscore}
            \end{figure}
            }
            \begin{itemize}
                \item   common to all performances are:
                \begin{itemize}
                    \item   \textit{deviations at phrase boundaries} and other grouping units
                    \item   \textit{similar tempo patterns for similar scores}
                    \item   \textit{distinct tempo patterns for distinct segments}
                \end{itemize}
            \end{itemize}
        \pause
        \item   \textbf{micro-level}: variations in \textit{timing}
            \begin{figure}
                \includegraphics[scale=.3]{graph/expressivetiming}
            \end{figure}
            \vspace{-2mm}
            \begin{itemize}
                \item   small deviations from protoypical beat positions used as \textit{expressive tool}
                \item   event can deviate up to a distance of 1/8 of the time interval of the basic pulse and still be considered on the beat
                \item   temporal micro-structure important characteristic of music performance
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{temporal processing}{timing accuracy of repetitions}
            \begin{figure}
                \includegraphics[scale=.35]{graph/repetitionsimilarity}
            \end{figure}
	
\end{frame}

\begin{frame}{temporal processing}{other performance visualizations}
    \vspace{-8mm}\begin{columns}
    \column{.22\textwidth}
        \begin{itemize}
            \item   BQ \includeaudio{audio/bq.mp3} 
            \item   QV1 \includeaudio{audio/qv1.mp3}
            \item   EQ \includeaudio{audio/eq.mp3} 
            \item   AQ \includeaudio{audio/aq.mp3}
            \item   LEQ \includeaudio{audio/leq.mp3} 
            \item   HSQ \includeaudio{audio/hsq.mp3} 
        \end{itemize}
    \column{.78\textwidth}
        \begin{figure}
            \centering
            \includegraphics[scale=0.6]{Graph/overallscatter}
        \end{figure}
    \end{columns}
\end{frame}
\begin{frame}{temporal processing}{other score-related comparisons}
            \begin{figure}
                \includegraphics[scale=.5]{graph/ppgui_loud}
            \end{figure}
\end{frame}

%\begin{frame}{temporal processing}{GTTM}
    %\begin{itemize}
        %\item   hypothesis: ``listener attempts to organize events into a signal coherent structure, such as they are all heard in a hierarchy of relative importance''
    %\end{itemize}
%\end{frame}
%\begin{frame}{temporal processing}{rhythm and short time memory}
%\end{frame}
%\begin{frame}{temporal processing}{inter onset interval}
%\end{frame}
%\begin{frame}{temporal processing}{rhythmic pattern}
%\end{frame}
%\begin{frame}{temporal processing}{pulse/meter}
%\end{frame}
%\begin{frame}{temporal processing}{tempo}
%\end{frame}
%\begin{frame}{temporal processing}{tempo and memory}
%\end{frame}
%\begin{frame}{temporal processing}{accent}
%\end{frame}
%\begin{frame}{temporal processing}{metrical hierarchy}
%\end{frame}
%\begin{frame}{temporal processing}{metrical tension}
%\end{frame}
%\begin{frame}{temporal processing}{temporal category}
%\end{frame}
%\begin{frame}{temporal processing}{expressive timing}
%\end{frame}
%
%%GTTM?
%\begin{frame}{temporal processing}{GTTM?}
%\end{frame}
%

