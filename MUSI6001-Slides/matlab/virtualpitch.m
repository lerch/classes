f     = [1800 2000 2200];
amplitude   = 1/(3);
samplerate  = 44100;
tonelength  = 44100;
cAudioFilePath = 'H:\Docs\repository\private.git\classes\MUSI6001-Slides\audio\inharmonicvirtualpitch'
%cFigureFilePath = 'H:\Docs\repository\private.git\classes\MUSI6001-Slides\graph\highmelody'

audio = zeros(1,2.5*tonelength);
for (i=1:length(f))
    audio(1:tonelength) = audio(1:tonelength) + amplitude * sin(2*pi*(0:tonelength-1)/samplerate*f(i));
end
for (i=1:length(f))
    audio(1.5*tonelength+1:end) = audio(1.5*tonelength+1:end) + amplitude * sin(2*pi*(0:tonelength-1)/samplerate*(f(i)+40));
end

spectrogram(audio,1024,512,1024,samplerate,'yaxis')
audiowrite([cAudioFilePath '.wav'],audio,samplerate)
%print('-dpng', sprintf('%s.png',cFigureFilePath));