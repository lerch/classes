f_start     = 1500;
amplitude   = 1/sqrt(2);
samplerate  = 44100;
tonelength  = 44100;
semitones   = [0 2 4 5 7 5 4 2 0];
cAudioFilePath = 'H:\Docs\repository\private.git\classes\MUSI6001-Slides\audio\highmelody'
cFigureFilePath = 'H:\Docs\repository\private.git\classes\MUSI6001-Slides\graph\highmelody'

f_all = f_start * 2.^(semitones./12);

audio = [];
for (i=1:length(f_all))
    audio = [audio amplitude * sin(2*pi*(0:tonelength-1)/samplerate*f_all(i))];
end
for (i=1:length(f_all))
    audio = [audio amplitude * sin(2*pi*(0:tonelength-1)/samplerate*4*f_all(i))];
end

spectrogram(audio,1024,512,1024,samplerate,'yaxis')
audiowrite([cAudioFilePath '.wav'],audio,samplerate)
print('-dpng', sprintf('%s.png',cFigureFilePath));