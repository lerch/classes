function [t,x] = GenerateSine (fFrequency, fAmplitude, fLengthInS, fSampleRate, fPhase)

    if nargin < 5
        fPhase = 0;
    end
    iNumOfSamples   = round(fLengthInS * fSampleRate);
    t               = linspace(0, fLengthInS-1/fSampleRate,iNumOfSamples);
    
    x               = zeros(length(fFrequency),iNumOfSamples);
    for (i=1:length(fFrequency))
        x(i,:)          = fAmplitude*sin(2*pi*fFrequency(i) * t + fPhase);
    end
end