function GenSineFreqAudio ()
    fLength = 1;
    fs = 44100;
    fFreq = [33 66 131 524 4185];
    [t,x] = GenerateSine(fFreq,.4,fLength,fs);

    cPath = 'H:\Docs\repository\private.git\classes\MUSI6001-Slides\';

    for (i = 1:length(fFreq))
        audiowrite([cPath 'audio\sine' num2str(fFreq(i)) '.wav'], x(i,:),fs);
    end
end