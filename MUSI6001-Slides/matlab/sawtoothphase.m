function sawtoothphase ()
    fLength = 2;
    fs = 44100;
    fFreq = 100;
    fAmp = .3;
    for (i=1:20)
        [t,x(i,:)] = GenerateSine(i*fFreq,1/i*fAmp,fLength,fs);
    end
    for (i=1:20)
        [t,y(i,:)] = GenerateSine(i*fFreq,1/i*fAmp,fLength,fs,pi/2);
    end
    for (i=1:20)
        [t,w(i,:)] = GenerateSine(i*fFreq,1/i*fAmp,fLength,fs,pi);
    end
    for (i=1:20)
        [t,z(i,:)] = GenerateSine(i*fFreq,1/i*fAmp,fLength,fs,rand(1)*2*pi);
    end
    wsum = sum(w,1);
    xsum = sum(x,1);
    ysum = sum(y,1);
    zsum = sum(z,1);
    cPath = 'H:\Docs\repository\private.git\classes\MUSI6001-Slides\';

    audiowrite([cPath 'audio\saw0.wav'], xsum,fs);
    audiowrite([cPath 'audio\sawpi2.wav'], ysum,fs);
    audiowrite([cPath 'audio\sawrand.wav'], zsum,fs);
    audiowrite([cPath 'audio\sawpi.wav'], wsum,fs);
    
    hFigureHandle = figure;
    subplot(221),plot(xsum(1:1000)), grid on, axis([1 1000 -.8 .8]),title('phase: 0')
    subplot(222),plot(ysum(1:1000)), grid on, axis([1 1000 -.8 .8]),title('phase: pi/2')
    subplot(223),plot(wsum(1:1000)), grid on, axis([1 1000 -.8 .8]),title('phase: pi')
    subplot(224),plot(zsum(1:1000)), grid on, axis([1 1000 -.8 .8]),title('phase: rand')
    PrintFigure2File(hFigureHandle, [cPath 'graph\sawphase'])
end