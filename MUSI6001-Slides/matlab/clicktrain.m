click = hann(88)/sqrt(2);
fs = 44100;
f_start = 4;
f_stop = 50;


t_end = 30;
t = linspace(0,t_end-1/fs,t_end*fs);
f = linspace(f_start,f_stop, length(t));

audio = zeros(1,length(t));

index = 1;

while (round(index + fs/f(index)) < length(t)-length(click))
    audio(index:index+length(click)-1) = click;
    index = round(index + fs/f(index));
end

cAudioFilePath = 'H:\Docs\repository\private.git\classes\MUSI6001-Slides\audio\clicktrain'
audiowrite([cAudioFilePath '.wav'],audio,samplerate)