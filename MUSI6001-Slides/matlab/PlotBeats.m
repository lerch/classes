function PlotBeats ()
    fLength = 1;
    fPlotLength = .2;
    fs = 44100;
    [t,x088] = GenerateSine(88,.4,fLength,fs);
    [t,x092] = GenerateSine(96,.4,fLength,fs);
%     [t,x168] = GenerateSine(168,.4,fLength,44100);
%     [t,x172] = GenerateSine(172,.4,fLength,44100);
   
    iEndIdx = round(fPlotLength/fLength*length(t));
    y1 = 2*x088;
    y2 = x088 + x092;
%     y3 = x088 + x168;
%     y4 = x088 + x172;
    
    hFigureHandle = figure; 
    subplot(231),plot(t(1:iEndIdx),x088(1:iEndIdx)),grid on, title('f = 88Hz'), axis([t(1) t(iEndIdx) -1 1])
    subplot(234),plot(t(1:iEndIdx),x088(1:iEndIdx)),grid on, title('f = 88Hz'), axis([t(1) t(iEndIdx) -1 1])
%     subplot(437),plot(t,x088),grid on, title('f = 88Hz'), axis([t(1) t(end) -1 1])
%     subplot(4,3,10),plot(t,x088),grid on, title('f = 88Hz'), axis([t(1) t(end) -1 1])
    
    subplot(232),plot(t(1:iEndIdx),x088(1:iEndIdx)),grid on, title('f = 88Hz'), axis([t(1) t(iEndIdx) -1 1])
    subplot(233),plot(t(1:iEndIdx),y1(1:iEndIdx)),grid on, title('sum'), axis([t(1) t(iEndIdx) -1 1])

    subplot(235),plot(t(1:iEndIdx),x092(1:iEndIdx)),grid on, title('f = 96Hz'), axis([t(1) t(iEndIdx) -1 1])
    subplot(236),plot(t(1:iEndIdx),y2(1:iEndIdx)),grid on, title('sum'), axis([t(1) t(iEndIdx) -1 1])

%     subplot(438),plot(t,x168),grid on, title('f = 168Hz'), axis([t(1) t(end) -1 1])
%     subplot(439),plot(t,y3),grid on, title('sum'), axis([t(1) t(end) -1 1])
% 
%     subplot(4,3,11),plot(t,x172),grid on, title('f = 172Hz'), axis([t(1) t(end) -1 1])
%     subplot(4,3,12),plot(t,y4),grid on, title('sum'), axis([t(1) t(end) -1 1])

cPath = 'H:\Docs\repository\private.git\classes\MUSI6001-Slides\';
audiowrite([cPath 'audio\sine88.m4a'], x088,fs) 
audiowrite([cPath 'audio\sine96.m4a'], x092,fs) 
audiowrite([cPath 'audio\sinesum.m4a'], y2,fs) 
PrintFigure2File(hFigureHandle, [cPath 'graph\sine-addition'])
end