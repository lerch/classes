function GenLocalizationWave()
    click   = .5*hann(32);
    fs      = 44100;
    length  = 5;
    numclicks = 16;
    cOutputPath = 'H:\Docs\repository\private.git\classes\MUSI6001-Slides\audio/';
    
    xt      = zeros(fs*length, 2);
    xl      = xt;
    
    tclick  = fs*linspace(0,length,numclicks);
    difft   = fs*linspace(0,1.2e-3,numclicks);
    diffl   = linspace(0,-36,numclicks);
 
    for (i=1:numclicks)
        xl(tclick(i)+1:(tclick(i)+size(click,1)),:) = [click 10^(0.05*diffl(i))*click];
        
        xt(tclick(i)+1:(tclick(i)+size(click,1)),1)                     = click;
        xt((tclick(i)+1+round(difft(i))):(tclick(i)+size(click,1)+round(difft(i))),2) = click;
    end
    audiowrite ([cOutputPath 'localizationlevel.wav'], xl, fs);
    audiowrite ([cOutputPath 'localizationtime.wav'], xt, fs);
end