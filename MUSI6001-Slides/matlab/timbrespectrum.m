cAudioPath = 'H:\Docs\repository\private.git\classes\MUSI6001-Slides\audio/';
cFileNames = char('timbre.oboe.A5.trunc.wav',...
    'timbre.clarinet.A5.trunc.wav',...
    'timbre.violin.A5.trunc.wav',...
    'timbre.flute.A5.trunc.wav');

t_plot = [2048 4096];
figure(15)
for i=1:size(cFileNames,1)
    [x,fs] = audioread([cAudioPath deblank(cFileNames(i,:))]);
    x=x(:,1);
    t = (0:length(x)-1)/fs;
    subplot(4,2,2*i-1)
    plot(t(t_plot(1):t_plot(2)),x(t_plot(1):t_plot(2)),'k')
    grid on, axis([t(t_plot(1)) t(t_plot(2)) -.6 .6])
    ylabel('Amplitude')
 
    X = 10*log10(abs(fft(x,32768))/16386);
    X = X(1:end/2+1);
    f = linspace(0,fs/2,length(X));
    subplot(4,2,2*i)
    plot(f,X,'k')
    grid on, axis([f(1) f(end) -50 0])
    ylabel('Magnitude Spectrum')
end   