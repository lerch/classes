\subsection{introduction}
\begin{frame}{perception of loudness}{introduction}
            \begin{figure}
                \includegraphics[scale=.5]{graph/sinusoidal2}
            \end{figure}
    \begin{itemize}
        \item   relation of \textbf{physical properties} to \textbf{perceptual properties}
            \bigskip
            \begin{itemize}
                \item   \only<2>{\Large\textcolor{gtgold}}{waveform \textbf{amplitude} $\rightarrow$ \textbf{loudness}}\only<2>{\normalsize}
                    \begin{itemize}
                        \item[] the larger the louder
                    \end{itemize}
                \item   waveform \textbf{period} $\rightarrow$ \textbf{pitch}
                    \begin{itemize}
                        \item[] the longer the lower
                    \end{itemize}
                \item   waveform \textbf{shape} $\rightarrow$ \textbf{timbre}
                    \begin{itemize}
                        \item[] the further from sinusoidal the ``richer''
                    \end{itemize}
                
            \end{itemize}
    \end{itemize}
    \vspace{20mm}
\end{frame}

\begin{frame}{perception of loudness}{physical measures}
    \begin{itemize}
        \item   amplitude: \textit{instantaneous} sound pressure \[p(t)\]
        \pause
        \item   sound pressure: \[p_\mathrm{rms} = \sqrt{\frac{1}{T_\mathrm{int}}\int\limits_{T_\mathrm{int}}{p(t)^2}}\]
        \pause
        \item   intensity: \[I = \frac{p_\mathrm{rms}^2}{Z} = \frac{p_\mathrm{rms}^2}{c\cdot\rho}\ \quad \left[\unit{\frac{W}{m^2}}\right]\]
            \begin{itemize}
                \item   $c$: speed of sound
                \item   $\rho$: air density
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{perception of loudness}{loudness definition}
    \begin{block}{\textbf{definition} (Moore)}
        loudness is that attribute of auditory sensation in terms of which sound can be ordered on a scale extending from quiet to loud
    \end{block}
    \pause
    \begin{itemize}
        \item   loudness is \textbf{subjective quality} $\rightarrow$ cannot be measured directly!
        \pause
        \bigskip
        \item   remember:
            \begin{itemize}
                \item   Why is the pseudo-unit dB used?
                \pause
                    \begin{itemize}
                        \item   logarithmic compression of large range\\ \unit[80]{dBSPL}:\unit[0]{dBSPL} $\rightarrow$ 1,000,000,000,000:1
                        \item   laws of psychophysics (Weber/Fechner)
                    \end{itemize}
                \pause
                \smallskip
                \item   Why is dB not a loudness scale?
                    \pause
                    \begin{itemize}
                        \item   \unit[80]{dBSPL} not twice as loud as \unit[40]{dBSPL}
                    \end{itemize}
            \end{itemize}
    \end{itemize}
    \vspace{20mm}
\end{frame}

\subsection{area of hearing}
\begin{frame}{perception of loudness}{absolute threshold of hearing 1/2}
    \vspace{-5mm}
    \begin{block}{\textbf{absolute threshold of sound} (Moore)}
        The absolute threshold of sound is the minimum detectable level of that sound in the absence of any other external sounds.
    \end{block}
    \pause
    \begin{itemize}
        \item   methods of measurement
            \begin{itemize}
                \item \textbf{Minimum Audible Pressure (MAP)}
                    \begin{itemize}
                        \item   measures SPL close to ear drums
                        \item   produce sound with headphones
                        \item   monaural
                    \end{itemize}
                \pause
                \item   \textbf{Minimum Audible Field (MAF)}
                    \begin{itemize}
                        \item   measures SPL at listener position (without listener)
                        \item   produce sound with loudspeakers in anechoic room
                        \item   binaural
                    \end{itemize}
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{perception of loudness}{absolute threshold of hearing 2/2}
    \vspace{-5mm}
    \begin{columns}
        \column{.6\textwidth}
            \begin{itemize}
                \item   \textbf{discussion}
                    \begin{itemize}
                        \item   absolute level difference due to
                            \begin{itemize}
                                \item binaural vs. monaural ($\unit[2]{dB}$)
                                \item   physical noises (occluded ear)
                            \end{itemize}
                        \item   both: rapid increase for low/high frequencies (middle ear)
                        \item   MAF: dip \unit[3-4]{kHz}, \unit[8-9]{kHz} (head, pinna \& meatus)
                    \end{itemize}
                \uncover<2->{
                \smallskip
                \item   \textbf{general remarks}:
                    \begin{itemize}
                        \item   individual subjects may have deviations up to 20db and are still considered normal
                        \item   highest audible frequency varies considerably between adults and children
                        \item   ability to hear faint sounds also depends on level of ambient noise  
                    \end{itemize}
                    }
            \end{itemize}
        \column{.4\textwidth}
            \only<1->{
            \vspace{-20mm}
            \begin{figure}
                \includegraphics[scale=.2]{graph/ATH}
            \end{figure}
            }
    \end{columns}
\end{frame}

\begin{frame}{perception of loudness}{absolute threshold of hearing with age}
            \begin{figure}
                \includegraphics[scale=.8]{graph/ATHwithage}
            \end{figure}
\end{frame}

\begin{frame}{perception of loudness}{absolute threshold of pain}
    \begin{itemize}
        \item   generally between \unit[120-130]{dBSPL}
        \pause
        \smallskip
        \item   \textbf{huge variation} between subjects, not many data points
        \pause
        \smallskip
        \item   \textbf{young listeners usually more tolerant} because protective mechanisms are more effective
    \end{itemize}
\end{frame}

\begin{frame}{perception of loudness}{equal loudness contours}
    \vspace{-5mm}\begin{itemize}
        \item   \textbf{experiment}: 
            \begin{itemize}
                \item   \textit{reference stimuli}: pure tone (freq: \unit[1000]{Hz}, level: \unit[10,20,\ldots,110]{dBSPL}) 
                \item   \textit{test stimuli}: adjust level so that perceived loudness matches reference loudness
            \end{itemize}
    \end{itemize}
        \pause
        \only<2>{
        \vspace{-2mm}
        \begin{figure}
            \includegraphics[scale=.3]{graph/equalloudnesscontours}
        \end{figure}
        }
        \pause
        \vspace{-2mm}
        \begin{columns}
            \column{.6\textwidth}
                \begin{itemize}
                    \item   \textbf{discussion}
                \begin{itemize}
                    \uncover<3->{\item   similar shape as MAF, but flattened for higher levels}
                    \uncover<4->{\item   relative loudness of different frequency components changes as function of overall level}
                    \uncover<5->{\item   growth of loudness level greater for low frequencies}
                    \uncover<6->{\item   frequency balance changes with reproduction level}
                    \uncover<7->{\item   again: large variability between experiments}
                \end{itemize}
                \end{itemize}
            \column{.4\textwidth}
                \only<3->{
                \vspace{-20mm}
                \begin{figure}
                    \includegraphics[scale=.2]{graph/equalloudnesscontours}
                \end{figure}
                }
        \end{columns}
\end{frame}


\begin{frame}{perception of loudness}{loudness level}
    \vspace{-5mm}
    \begin{block}{\textbf{loudness level}}
        The loudness level in phon of a stimulus is equal to the \unit{dBSPL} of a \unit[1]{kHz} sinusoidal of the same preceived loudness.
    \end{block}
    
    \begin{columns}
        \column{.4\textwidth}
            \uncover<2->{
            \begin{itemize}
                \item   \textbf{not} a loudness scale:\\
                double phone does not double loudness!
            \end{itemize}
            }
        \column{.6\textwidth}
            \uncover<1->{
            \begin{figure}
                \includegraphics[scale=.3]{graph/equalloudnesscontours}
            \end{figure}
            }
    \end{columns}
\end{frame}
\begin{frame}{perception of loudness}{area of hearing}
    \begin{figure}
        \includegraphics[scale=.4]{graph/hearingarea}
    \end{figure}
\end{frame}

\begin{frame}{perception of loudness}{dynamic range of the auditory system}
    \vspace{-5mm}\begin{itemize}
        \item   dynamic range can be up to \unit[120]{dB}, \textbf{but}
            \begin{itemize}
                \item   nerve fibres with dynamic ranges higher than \unit[60]{dB} only 10\% of all fibers, others are saturated
                \item   firing rate does not significantly change above \unit[60]{dB}
            \end{itemize}
        \pause
        \bigskip
        \item   \textbf{possible explanations}:
            \begin{enumerate}
                \item   excitation patterns of stimulus spread with increasing intensity
                \begin{figure}
                    \includegraphics[scale=.1]{graph/excitationspread} 
                \end{figure}
                \pause
                \item   timing of neural impulses (CF neurons in phase, other neurons random --- but less random for higher levels)
                \pause
                \item   information from a small number of neurons might be sufficient to account for intensity discrimination (firing rates and variability)
            \end{enumerate}
    \end{itemize}
\end{frame}

\subsection{sone}
\begin{frame}{perception of loudness}{the scaling of loudness: sone 1/3}
    \begin{itemize}
        \item   \textbf{question: how to derive a loudness scale?}
        \pause
        \bigskip
        \begin{itemize}
            \item   \textbf{magnitude estimation}:\\ play sounds at different levels and let subjects assign a number
            \smallskip
            \item   \textbf{magnitude production}:\\ adjust level of sound until is has specific loudness (absolute or relative to a standard)
            \pause
            \bigskip
            \item   \textbf{problems}:
                \begin{itemize}
                    \item   large individual differences
                    \item   not really clear what, e.g., \textit{doubling} the loudness really means: not a typical human task
                \end{itemize}
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{perception of loudness}{the scaling of loudness: sone 2/3}
    \vspace{-5mm}
    \begin{figure}
        \includegraphics[scale=.5]{graph/doubleloudness}
    \end{figure}
    \begin{itemize}
        \item   Stevens: $N = k\cdot I^{0.3}$
        \pause
        \item[$\Rightarrow$] each \unit[10]{dB} doubles the loudness 
        \pause
        \item   \unit[1]{sone} = loudness of pure tone at \unit[1]{kHz}, \unit[40]{dBSPL}
    \end{itemize}
\end{frame}

\begin{frame}{perception of loudness}{the scaling of loudness: sone 3/3}
    \begin{figure}
        \includegraphics[scale=.5]{graph/sone}
    \end{figure}
\end{frame}

\begin{frame}{perception of loudness}{the scaling of loudness: problems of sone models}
    \begin{columns}
        \column{.5\textwidth}
             \begin{enumerate}
                \item   \textbf{level dependence}:\\ level increment necessary to produce a \textit{double} of the loudness ($\Rightarrow$ increase $\alpha$)
                \uncover<2->{
                \bigskip
                \bigskip
                \bigskip
                \bigskip
                \bigskip
                \item   \textbf{stimulus dependence}
                    \begin{itemize}
                        \item   sine: solid
                        \item   uniform exciting noise: dotted
                    \end{itemize}
                }
                \vspace{10mm}
            \end{enumerate}
       \column{.5\textwidth}
            \vspace{-5mm}
            \begin{figure}
                \includegraphics[scale=.3]{graph/sonefreq}
            \end{figure}
                \uncover<2->{
                    \begin{figure}
                        \includegraphics[scale=.3]{graph/sonestimulus}
                    \end{figure}
                }
    \end{columns}
\end{frame}

\begin{frame}{perception of loudness}{temporal integration}
    \vspace{-10mm}
    \begin{columns}
        \column{.5\textwidth}
            \begin{itemize}
                \item   loudness depends on \textbf{stimulus duration}: 
                    \begin{itemize}
                        \item   sounds shorter than about \unit[200]{ms} appear quieter
                        \item   no change for longer durations
                    \end{itemize}
            \end{itemize}
    \uncover<3>{
    \includeaudio{audio/zwicker-track-33.mp3}
    \begin{itemize}
        \item   pairs of \unit[3]{kHz} tones
        \item   length: \unit[1]{s}/ 1000,300,100,30,10,3\unit{ms}
        \item   each pair is presented twice
    \end{itemize}}
        \column{.5\textwidth}
                    \begin{figure}
                        \includegraphics[scale=.3]{graph/loudnessduration}
                    \end{figure}
                    \uncover<2->{
                    \begin{figure}
                        \includegraphics[scale=.3]{graph/loudnessrepetition}
                    \end{figure}
                    }
    \end{columns}
    \uncover<4->{
    \begin{itemize}
        \item   model: $ (I-I_L) \cdot t = I_L \cdot \tau = const.$
            \begin{itemize}
                \item   $I_L$: threshold intensity for long-duration tone pulse
                \item   $\tau$: constant representing the 'integration time' of the auditory system (150--400\unit{ms})
            \end{itemize}
    \end{itemize}
    }
\end{frame}

\begin{frame}{perception of loudness}{loudness: just noticeable difference}
    \hspace{50mm}\textbf{in-class listening 2}
    \vspace{-10mm}
    \begin{flushright}
        \includegraphics[scale=.25]{graph/listeningtest} 
    \end{flushright}
    \pause
    \begin{columns}
        \column{.6\textwidth}
        \begin{itemize}
            \uncover<2->{
            \item   Weber/Fechner: $\frac{\Delta I}{I}\approx const.$
            }
            \uncover<3->{
            \item   stimulus: \textbf{white-band noise}% (20--100\unit{dBSPL})
                \[ JNDL \approx 0.5-1 \unit{dB}\]
            }
            \uncover<4->{
            \item   stimulus:\textbf{ pure tone}
                \begin{itemize}
                    \item   JNDL decreases with Level
                    \item   so-called: \textit{near miss}
                \end{itemize}
            }
        \end{itemize}
        \column{.4\textwidth}
            \uncover<3->{
            \vspace{-3mm}
                \begin{flushright}
                    \includegraphics[scale=.4]{graph/JNDLnoise} 
                \end{flushright}
            }
            \uncover<4->{
            \vspace{-10mm}
                \begin{flushright}
                    \includegraphics[scale=.4]{graph/JNDLsine} 
                \end{flushright}
            }
    \end{columns}
\end{frame}

\begin{frame}{perception of loudness}{JNDL: explanation of near miss}
    \vspace{-5mm}
    Why does Weber's law not hold for the pure tones and narrow-band noise?
    \pause
    \begin{columns}
        \column{.65\textwidth}
            \begin{enumerate}
                \item   Weber's law holds but has to be applied to excitation patterns
                    \begin{itemize}
                        \item   change in intensity detectable if change of $>\unit[1]{dB}$ in any channel
                        \uncover<3->{
                        \item   high frequency side grows in nonlinear way with stimulus intensity
                        \item[$\Rightarrow$] pattern change might be greater than stimulus intensity change
                        }
                    \end{itemize}
                \uncover<4->{
                \item   information from all channels is combined 
                \item[$\Rightarrow$] intensity can be estimated from number of active channels
                }
            \end{enumerate}
        \column{.35\textwidth}
                \uncover<2->{
                \begin{flushright}
                    \includegraphics[width=40mm, height=60mm]{graph/excitationpatterns} 
                \end{flushright}
                }
    \end{columns}
\end{frame}

\begin{frame}{perception of loudness}{loudness adaptation and fatigue}
    \vspace{-5mm}
    \begin{itemize}
        \item   \textbf{fatigue}:\\
        measured after stimulus has been removed
            \begin{itemize}
                \item   \textit{temporary}: TTS --- temporary threshold shift \only<1>{(Fig.: \unit[3]{min})
                    
                    \begin{figure}
                        \includegraphics[scale=.2]{graph/TTS3min}
                    \end{figure}
                    }
                \pause
                \item   \textit{permanent}: \\ some evidence that music may be less damaging than industrial noise
                    \pause
                    \begin{itemize}
                        \item   possible sample bias: musicians with hearing loss stop practicing, musicians are more sensitive to issues with ears, etc.
                    \end{itemize}
            \end{itemize}
        \pause
        \item   \textbf{adaptation}:\\ response to a stimulus declines while the stimulus is active
            \pause
            \begin{itemize}
                \item   example:
            \begin{itemize}
                \item   play 80dB sine on one ear for several minutes
                \item   adjust level of second ear \pause $\rightarrow\approx 60-70dB$
            \end{itemize}
            \end{itemize}
        
    \end{itemize}
    \vspace{30mm}
\end{frame}

\begin{frame}{perception of loudness}{models of loudness: RMS-based models without loudness scale}
    \vspace{-5mm}
    \begin{itemize}
        \item   simple RMS: $v_\mathrm{RMS} = \sqrt{\frac{1}{k_2-k_1+1}\sum\limits_{k_1}^{k_2}{x(k)^2}}$
        \item   weighted RMS: dBA, BS.1770
    \end{itemize}
    \scalebox{0.8}
    {
    %\begin{figure}
        \input{pict/lowlevelfeatures_rmsblock}
    %\end{figure}
    }
    \pause
    \begin{figure}
        \includegraphics[scale=.7]{graph/loudnessweighting}
    \end{figure}
\end{frame}

\begin{frame}{perception of loudness}{models of loudness: EBU R128}
    \begin{itemize}
        \item   \textbf{purpose}:
            \begin{itemize}
                \item   international broadcast standard to normalize programme loudness
            \end{itemize}
        \pause
        \item   \textbf{implementation}:
            \begin{itemize}
                \item   use ITU-R BS.1770 as base measure
                \item   apply gating to discard low level frames when computing overall loudness
                \item   add True Peak meter to avoid clipping (\unit[-1]{dBTP})
            \end{itemize}
        \pause
        \item   \textbf{output}:
            \begin{itemize}
                \item   programme loudness
                    \begin{itemize}
                        \item   LKFS/LUFS: Loudness Units Full Scale
                        \item   LU: Loudness Unit $\unit[0]{LU}=\unit[-23]{LUFS}$
                    \end{itemize}
                \item   loudness range
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{perception of loudness}{models of loudness: loudness scale models}
		\scalebox{.8}
		{
%		\begin{figure}
			\centering
			\input{pict/lowlevelfeatures_zwicker}
%		\end{figure}
		}
		
		\uncover<2->{
		\begin{itemize}
			
			\item	
			\only<2>{ outer ear transfer function
			\begin{figure}
				\includegraphics[scale=.25]{graph/OETF}
			\end{figure}
			}
			
			\only<3>{	excitation patterns
			\begin{figure}
				\includegraphics[scale=.5]{graph/excitationpatterns2}
			\end{figure}
			}
			\only<4>{	specific loudness
			\begin{figure}
				\includegraphics[scale=.4]{graph/specificloudness}
			\end{figure}
			}
			\only<5>{	overall loudness
            \[ v_\mathrm{loud} = \sum_{\forall i} z_i\]
			}
		\end{itemize}
        \vspace{70mm}
		}
	\end{frame}

