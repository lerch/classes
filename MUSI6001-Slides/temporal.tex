        %pattern perception
        %deutsch: 13, pierce
        %meyer
        %moore
\subsection{introduction}
\begin{frame}{temporal processing}{introduction}
    \vspace{-5mm}
    \begin{itemize}
        \item   previously, we mostly looked at \textit{stationary sounds}, but:
            \begin{itemize}
                \item   almost all \textbf{sounds fluctuate over time}
                \item   much of the information in speech and music is \textbf{carried in the change} itself
            \end{itemize}
    \end{itemize}
    \pause
    \bigskip
    \begin{itemize}
        \item   when investigating the temporal processing in the auditory system, we are interested in 
            \begin{itemize}
                \item   \textbf{temporal resolution}: ability to detect changes in stimuli over time (gap between to stimuli, modulation, ...)
                    \begin{itemize}
                        \item   usually refers to detectable changes in the envelope, not the waveform itself
                    \end{itemize}
                \item   \textbf{temporal integration}: ability to accumulate information over time to enhance the detection/discrimination of stimuli
                    \begin{itemize}
                        \item   temporal masking
                        \item  dependence of pitch and loudness on duration
                    \end{itemize}
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{temporal processing}{introduction}
    \begin{itemize}
        \item   general \textbf{difficulty in experimental design}
            \begin{itemize}
                \item   changing the time pattern often results in changes in the (magnitude) spectrum $\Rightarrow$ danger of measuring spectral changes
            \end{itemize}
                \pause
                \bigskip
                \item   \textbf{example}: distinguish a single click from a pair of clicks (separated by short time interval)
                \pause
                \begin{itemize}
                    \item[$\rightarrow$]   \textbf{result}: subjects are able to distinguish clicks when gap is \textit{only few tens of microseconds}
                    \pause
                    \item   \textbf{but}: subjects detect \textit{spectral differences}
                    \pause
                    \item   \textbf{verification}: when $f > \unit[10]{kHz}$ are masked, gap threshold increases dramatically
                \end{itemize}
    \end{itemize}
\end{frame}

\subsection{experiments}
\begin{frame}{temporal processing}{discrimination of stimuli with identical magnitude spectra 1/3}
    \begin{itemize}
        \item   \textbf{stimulus}: long-term magnitude spectrum of \textit{broadband noise} remains the same if briefly interrupted
        \pause
        \bigskip
        \item   \textbf{experiment}: discriminate white noise burst from interrupted white noise burst
            \begin{itemize}
                \item   (typical method: 2AFC)
                \item   gap threshold is 2--3\unit{ms}
                \item   threshold is level invariant for moderate to high levels, increases for low levels
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{temporal processing}{discrimination of stimuli with identical magnitude spectra 2/3}
    \begin{itemize}
        \item   \textbf{stimulus}: long-term magnitude spectrum of \textit{time-reversed signals} remains the same
        \pause
        \bigskip
        \item   \textbf{experiment}: discriminate pairs of clicks with different amplitude (``A'' loud, ``B'' half amplitude)
            \begin{itemize}
                \item   can be distinguished for gaps down to 2--3\unit{ms}
                \pause
                \item   but: subjects \textbf{do not hear click pair, but different sound}
                \item   level dependent (both absolute and relative) thresholds vary from 0.25--3\unit{ms}
            \end{itemize}
       \item    similar experiments show that range is very stimulus dependent: 2--20\unit{ms}
    \end{itemize}
\end{frame}

\begin{frame}{temporal processing}{discrimination of stimuli with identical magnitude spectra 3/3}
    \vspace{-5mm}
    \begin{itemize}
        \item   \textbf{stimulus}: long-term magnitude spectrum of \textit{white noise with amplitude modulation} remains the same
        \item   \textbf{experiment}: find detection threshold
        \pause
        \vspace{-3mm}
        \begin{flushright}
            \includegraphics[scale=.25]{graph/tmtf}
        \end{flushright}
        \vspace{-32mm}
        \item[$\Rightarrow$] temporal modulation transfer function\\ (\textbf{TMTF})
        \pause
        \bigskip
        \item   \textbf{observations}
            \begin{itemize}
                \item   $f_\mathrm{m}<\unit[16]{Hz}$: threshold determined by\\ \textit{amplitude} resolution of the ear,\\ not temporal resolution
                \item   $f_\mathrm{m}>\unit[16]{Hz}$: threshold increases
                \item   $f_\mathrm{m}>\unit[1]{kHz}$: modulation cannot be detected
            \end{itemize}
        \pause
        \bigskip
        \item[$\Rightarrow$]    sensitivity to amplitude modulation decreases as rate increases
    \end{itemize}
    \vspace{50mm}
\end{frame}

\begin{frame}{temporal processing}{effects of center frequency on temporal resolution}
    \vspace{-5mm}
    \begin{itemize}
        \item   previous slides covered only \textit{broadband} signals
        \item   \textbf{hypothesis}: time resolution poorer at low frequencies (response time of auditory filters due to narrow bandwidths)
        \pause
        \item   \textbf{experiment 1}
            \begin{itemize}
                \item   stimuli: two sine bursts (time-reversed, 10dB level difference)
                \pause
                \item   \textit{result}: slightly higher time resolution for higher frequencies (\unit[1-2]{ms} @ \unit[2-4]{kHz}, \unit[2-4]{ms} @ \unit[1]{kHz})
            \end{itemize}
        \only<3>{
        \begin{figure}
            \includegraphics[scale=.5]{graph/temporalacuityfreq}
        \end{figure}
        }
        \pause
        \item   \textbf{experiment 2}
            \begin{itemize}
                \item   stimulus: narrow-band noise\\ with gap plus noise masker
                \pause
                \vspace{-13mm}
        \begin{flushright}
            \includegraphics[scale=.3]{graph/gapthreshnbnoise}
        \end{flushright}
        \vspace{-33mm}
                \item   result: \textit{not dependent}\\ on center frequency\\ but on bandwidth
            \end{itemize}
    \end{itemize}
    \vspace{50mm}
\end{frame}

\begin{frame}{temporal processing}{detection of temporal gaps in sinusoidals}
    \vspace{-5mm}
    \begin{itemize}
        \item   \textbf{experiment}:
        \begin{flushright}
            \includegraphics[scale=.18]{graph/sinusgapdesign}
        \end{flushright}
        \vspace{-23mm}
            \begin{itemize}
                \item   stimulus:\\ sinusoidal (\unit[400]{Hz}) $+$ masking noise
                \item   3 phase ``conditions'':\\ standard, reversed, preserved
            \end{itemize}
        \pause
        \item   \textbf{results}
        \begin{flushright}
            \includegraphics[scale=.25]{graph/sinusgap}
        \end{flushright}
        \pause
        \vspace{-45mm}
            \begin{itemize}
                \item   \textit{standard phase}: 
                    \begin{itemize}
                        \item gap difficult to detect when\\ integer multiple of the period
                        \item   easy to detect between periods
                    \end{itemize}
                \item   \textit{reversed phase}: 
                    \begin{itemize}
                        \item vice versa
                    \end{itemize}
                \item   possible explanation:\\ filter ringing in phase/out-of-phase
                \item   \textit{preserved phase}: 
                    \begin{itemize}
                        \item threshold (75\%): \unit[4.4]{ms}
                        \item   increasing at low freqs \unit[6-8]{ms}
                    \end{itemize}
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{temporal processing}{duration discrimination}
    \vspace{-5mm}\begin{itemize}
        \item   \textbf{experiment 1}: detect longer stimulus (sinusoidal / NB noise)
        \item   \textbf{result}: smallest detectable increase in duration changes with stimulus length
            \begin{itemize}
                \item   $T=\unit[10]{ms} \rightarrow \Delta T=\unit[4]{ms}$
                \item   $T=\unit[100]{ms} \rightarrow \Delta T=\unit[15]{ms}$
                \item   $T=\unit[1000]{ms} \rightarrow \Delta T=\unit[60]{ms}$
                \pause
                \item[$\Rightarrow$] \textbf{Weber fraction decreases} with increasing T
            \end{itemize}
        \pause
        \item   \textbf{experiment 2}: detect pause duration
        \item   \textbf{result}: 
            \begin{itemize}
                \item   discrimination improved with\\ increasing level of 'markers'
                \item   discrimination improved with training
                \item   \textbf{Weber fraction nearly constant}
        \vspace{-23mm}
        \begin{flushright}
            \includegraphics[scale=.19]{graph/gapdiscrim}
        \end{flushright}
            \end{itemize}
            \pause
            \vspace{-13mm}
        \item   \textbf{summary}
            \begin{itemize}
                \item   $T>10ms \rightarrow \Delta T$ increases with $T$
                \item   independent of spectral sound characteristics
                %\item   $\Delta T$ increases at low sound levels, and with different markers (level, freq)
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{temporal processing}{duration discrimination --- audio}
    \vspace{-10mm}
    \begin{flushright}
        \includegraphics[scale=.25]{graph/listeningtest} 
    \end{flushright}
    \includeaudio{audio/zwicker-track-40.mp3}
    
    \begin{itemize}
        \item   describe the duration of the gaps as compared to the short tone burst
        \pause
        \item   \textbf{example 1}: tone -- \unit[150]{ms}, gap -- \unit[150]{ms}
        \item   \textbf{example 2}: tone -- \unit[40]{ms}, gap -- \unit[40]{ms}
        \item   \textbf{example 3}: tone -- \unit[40]{ms}, gap -- \unit[150]{ms}
        \pause
        \item   last example has equal perceptual length (?)
    \end{itemize}
\end{frame}

\begin{frame}{temporal processing}{perceptual attack time}
    \vspace{-8mm}\begin{columns}
    \column{.4\textwidth}
    %\begin{flushright}
        \includegraphics[scale=.45]{graph/pat} 
    %\end{flushright}
    \column{.6\textwidth}
        \begin{itemize}
            \item   sound bursts of equal temporal spacing elicit sensation of \textbf{uniform rhythm}
            \item   \textbf{but}: only true with steep attack envelopes
            \item   \textbf{note}: \textit{perceptual attack time} is not \textit{perceptual onset time}
            \bigskip
            \pause
            \item   typical initial transient lengths for musical instruments?
            \pause
            \begin{itemize}
                \item   percussive: 2--20\unit{ms}
                \item   woodwind: up to 300\unit{ms}
            \end{itemize}
        \end{itemize}
    \end{columns}
\end{frame}
