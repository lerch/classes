\subsection{critical bands}
\begin{frame}{frequency selectivity}{critical bands 1/3}
    \vspace{-5mm}
    \begin{itemize}
        \item   threshold of sinusoidal (\unit[2]{kHz}) as function of bandwidth of bandpass noise masker (constant power density, centered at \unit[2]{kHz})
            \pause
            \begin{figure}
                \includegraphics[scale=.25]{graph/fletchersbandwidening}
            \end{figure}
        \pause
        \item   description:
            \begin{itemize}
                \item   threshold first increases with with noise bandwidth
                \item   up to a bandwidth, further bandwidth increases do not change threshold
            \end{itemize}
   \end{itemize}
\end{frame}

\begin{frame}{frequency selectivity}{critical bands 2/3}
    \vspace{-5mm}
    \begin{itemize}
        \item   discussion:
            \begin{itemize}
                \item   auditory system behaves as bank of overlapping bandpass filters
                \item   basilar membrane provides the basis for these auditory filters
                \pause
                \smallskip
                \item   when detecting a signal in noise background, we make use of a filter with a center frequency close to the signal frequency
                \item   this filter passes the signal but removes a lot of noise
                \item[$\Rightarrow$] only noise components within one band have any masking effect
            \end{itemize}
        \pause
        \bigskip
        \item   detected bandwidth is called \textbf{critical bandwidth}
        \item   critical band: assumption of hypothetical rectangular filter shape
   \end{itemize}
\end{frame}

\begin{frame}{frequency selectivity}{critical bands 3/3}
    \vspace{-5mm}
    \begin{itemize}
        \item   rough estimate for critical bandwidth: \[\Delta f_{CB} = \frac{P_{sine}}{const. \cdot N_0}\]
            \begin{itemize}
                \item   const between 0.3 and 1
            \end{itemize}
    \end{itemize}
    \begin{figure}
        \includegraphics[scale=.5]{graph/criticalbandratescale}
    \end{figure}
\end{frame}

\begin{frame}{frequency selectivity}{loudness revisited}
    \begin{figure}
        \includegraphics[scale=.5]{graph/cblevel}
    \end{figure}
    \begin{itemize}
        \item   loudness of band-limited noise (constant SPL)
            \begin{itemize}
                \item   bandwidth $<\Delta f_{CB}$ 
                \item[$\rightarrow$] sound about as loud as pure tone of equal intensity
                \pause
                \item   bandwidth $>\Delta f_{CB}$  
                \item[$\rightarrow$] loudness increases
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{frequency selectivity}{critical bands: other ways of measurement 1/2}
    \vspace{-5mm}
    \begin{itemize}
        \item   comb of pure tones (distance \unit[20]{Hz}), measure THQ
            \begin{figure}
                \includegraphics[scale=.35]{graph/cbexp1}
            \end{figure}
            \begin{itemize}
                \item[]   \vspace{-3mm}
                    \begin{enumerate}
                        \item   one test tone at $\unit[920]{Hz}   \rightarrow$ threshold at \unit[3]{dB}
                        \item   two test tones $\unit[920,940]{Hz}   \rightarrow$  threshold at \unit[0]{dB}
                        \item   \ldots
                        \item   after $\Delta f_{CB}$ is passed, threshold stays the same
                    \end{enumerate}
                \item[$\rightarrow$] THQ is determined by sound intensity of the complex (as long as components of this complex fall into one critical band)
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{frequency selectivity}{critical bands: other ways of measurement 2/2}
    \vspace{-5mm}
    \begin{itemize}
        \item   masking threshold in frequency gaps
        \begin{columns}
        \column{0.5\textwidth}
            \begin{figure}
                \includegraphics[scale=.5]{graph/cbexp2}
            \end{figure}
            \vspace{3mm}
        \column{0.5\textwidth}
            \begin{figure}
                \includegraphics[scale=.5]{graph/cbexp2a}
            \end{figure}
        \end{columns}
    \end{itemize}
\end{frame}


\begin{frame}{frequency selectivity}{discrimination of partials}
    \vspace{-5mm}
    \begin{itemize}
        \item   experiment description
            \begin{itemize}
                \item   stimuli: complex (periodic) sound (12 partials), pure tone
                \item   discrimination experiment: is pure tone part of complex or not?
            \end{itemize}
        \pause
        \begin{figure}
            \includegraphics[scale=.2]{graph/discriminationpartials}
        \end{figure}
        \pause
        \vspace{-5mm}
        \item   results
            \begin{itemize}
                \item   use discrimination threshold as bandwidth estimate:
                \item[$\rightarrow$] partial will only be distinguished when more than one critical band apart
                \pause
                \item   results match  $\Delta f_{CB}$ estimates for $f>\unit[1]{kHz}$
                \item   follow newer critical bandwidth estimates for lower frequencies
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{frequency selectivity}{Bark and ERB}
    \vspace{-5mm}
    \begin{figure}
        \includegraphics[scale=.4]{graph/barkerb}
    \end{figure}
    \pause
    \begin{eqnarray}
        \Delta f_{CB} &=& 25 + 75\left(1 + 1.4\cdot\left(\nicefrac{f_{CB}}{\unit{kHz}}\right)^2\right)^{0.69}\\
        z &=& 13\cdot \atan{\left(0.76\nicefrac{f_{CB}}{\unit{kHz}}\right)} + 3.5\cdot\atan{\left(\nicefrac{f_{CB}}{\unit[7.5]{kHz}}\right)}^2\\
        \pause
        \Delta f_{ERB} &=& 24.7\cdot\left(4.37\nicefrac{f_{CB}}{\unit{kHz}} +1\right)\\
        z_{ERB} &=& 21.4\log_{10}\left(4.37\nicefrac{f_{CB}}{\unit{kHz}} +1\right)
    \end{eqnarray}
\end{frame}

\begin{frame}{frequency selectivity}{summary: basilar membrane and critical bandwidth}
    \begin{figure}
        \includegraphics[scale=.4]{graph/cbbm}
    \end{figure}
    \begin{itemize}
        \item   CB/ERB of the auditory filter corresponds to constant distance on BM (1 ERB about 0.9mm)
        \item   frequency selectivity is largely determined in the cochlea
        \item   note: despite models and scales --- there is \textbf{no fixed number of bands}
    \end{itemize}
\end{frame}

\begin{frame}{frequency selectivity}{auditory filter shape}
    \vspace{-15mm}
    \begin{columns}
        \column{.6\textwidth}      
            \begin{itemize}
                \item   symmetric at moderate sound levels
                \item   low frequency side becoming shallower towards higher levels
            \end{itemize}
        \column{.4\textwidth}
            \begin{figure}
                \includegraphics[scale=.38]{graph/auditoryfiltershape}
            \end{figure}
    \end{columns}
\end{frame}

