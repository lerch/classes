\subsection{introduction}
\begin{frame}{timbre}{introduction}
            \begin{figure}
                \includegraphics[scale=.5]{graph/sinusoidal2}
            \end{figure}
    \begin{itemize}
        \item   relation of \textbf{physical properties} to \textbf{perceptual properties}
            \bigskip
            \begin{itemize}
                \item   {waveform \textbf{amplitude} $\rightarrow$ \textbf{loudness}}
                    \begin{itemize}
                        \item[] the larger the louder
                    \end{itemize}
                \item   waveform \textbf{period} $\rightarrow$ \textbf{pitch}
                    \begin{itemize}
                        \item[] the longer the lower
                    \end{itemize}
                \item   \only<2>{\Large\textcolor{gtgold}}{waveform \textbf{shape} $\rightarrow$ \textbf{timbre}}\only<2>{\normalsize}
                    \begin{itemize}
                        \item[] the further from sinusoidal the ``richer''
                    \end{itemize}
                
            \end{itemize}
    \end{itemize}
    \vspace{20mm}
\end{frame}

\begin{frame}{timbre}{definition}
    \vspace{-5mm}
    \begin{block}{\textbf{definition (American Standards Association)}}
        ...that attribute of sensation in terms of which a listener can judge that two sounds having the same loudness and pitch are dissimilar
    \end{block}
    \pause
    \begin{flushright}
         \includegraphics[scale=.08]{Graph/question-mark}
    \end{flushright}
    \vspace{-8mm}
    What is the problem with this definition? \pause (Bregman)
            \begin{enumerate}
                \item   does not attempt to explain what timbre is, but only what timbre is \textit{not}
                \item   implies that timbre only exists for sound with a pitch, implicating that, e.g., percussive instruments don't have timbre
            \end{enumerate}
    \pause
    \bigskip
    \begin{itemize}
        \item[$\rightarrow$]   McAdams, Bregman: timbre is... "\textit{...the psychoacoustician's multidimensional waste-basket category for everything that cannot be labeled pitch or loudness.}"
    \end{itemize}
\end{frame}

\begin{frame}{timbre}{general observations}
    \begin{itemize}
        \item   timbre allows us to differentiate
            \begin{itemize}
                \item   piano from violin (playing the same note/same loudness)\\ $\rightarrow$ \textbf{timbre quality}
                \item   different violins or pianos $\rightarrow$\\ \textbf{timbre identity}
            \end{itemize}
        \pause
        \bigskip
        \item   timbre is influenced by numerous factors
            \begin{itemize}
                \item   score: instrumentation, instrument characteristics
                \item   performance: playing techniques
                \item   other: e.g., room acoustics
            \end{itemize}
        \pause
        \bigskip
        \item   synonymous terms: sound/tone quality/color (Helmholtz: Klangfarbe)
        \pause
        \bigskip
        \item   vocabulary to describe timbre is all ``borrowed'': bright, sharp, full, \ldots 
    \end{itemize}
\end{frame}

\begin{frame}{timbre}{understanding timbre 1/2}
    \hspace{50mm}\textbf{listening \& discussion}
    \vspace{-10mm}
    \begin{flushright}
        \includegraphics[scale=.25]{graph/listeningtest} 
    \end{flushright}
    \vspace{-4mm}
            \begin{itemize}
                \item   \textbf{example 1}: \includeaudio{audio/ASA-track-53.mp3}\\partials are added in successive steps
                
                
                \item   \textbf{example 2}: \begin{scriptsize}\url{http://www.animations.physics.unsw.edu.au/jw/timbre-envelope.htm}\end{scriptsize}\\recording of crab canon played backwards
            \end{itemize}
\end{frame}

\begin{frame}{timbre}{understanding timbre 2/2}
    \begin{flushright}
         \includegraphics[scale=.08]{Graph/question-mark}
    \end{flushright}
    \vspace{-8mm}
    What can we learn from these examples about timbre? 
    \pause
        \bigskip
    \begin{itemize}
        \item   timbre is 
            \smallskip
            \begin{enumerate}
                \item   a function of \textbf{spectral distribution}
                    \begin{itemize}
                        \item   number of partials
                        \item   energy distribution of partials
                    \end{itemize}
                    \smallskip
                \item   a function of \textbf{temporal envelope}
            \end{enumerate}
    \end{itemize}
\end{frame}
%http://acousticslab.org/psychoacoustics/PMFiles/Module06.htm

\subsection{spectrum}

\begin{frame}{timbre}{spectral distribution}
    \begin{columns}
    \column{.5\textwidth}
    \begin{itemize}
        \item   blending of individual harmonics on timbre 
        
                \includeaudio{audio/ASA-track-01.mp3}
        \pause
                \bigskip
        \item   \textbf{sharpness}
    \begin{itemize}
        \item   Zwicker's model:
            \[S = const.\frac{\int{N'g(z)z\,dz}}{\int{N'\,dz}}\]
                \begin{itemize}
                    \item[] N': specific loudness, z: Bark,\\ g(z): weighting factor
                \end{itemize}
    \end{itemize}
    \end{itemize}
    \column{.5\textwidth}
                    \includegraphics[scale=.4]{graph/sharpness}
                \begin{itemize}
                    \item[] sharpness results for noise (CB-wide (solid), band-pass $f_\mathrm{x}$--\unit[10]{kHz}, , band-pass \unit[0.2]{kHz}--$f_\mathrm{x}$)
                \end{itemize}
                    
    \end{columns}
\end{frame}

\begin{frame}{timbre}{spectral distribution: dependence on pitch}
    \begin{itemize}
        \item   one instrument has not only \textit{one} timbre
            \begin{itemize}
                \item   \textbf{example 1}: \includeaudio{audio/ASA-track-57.mp3}\\
                        bassoon (3 octaves), transpositions of the highest tone
                \pause
                \bigskip
                \item   \textbf{example 2}: pitch shifting (factor 1.33) with/without spectral envelope preservation
                    \begin{itemize}
                        \item   ori \includeaudio{audio/cathy.mp3}
                        \item   shift \includeaudio{audio/cathyPropitch.mp3}
                        \item   preserving shift \includeaudio{audio/cathyPropitchf.mp3}
                    \end{itemize}
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{timbre}{vocal spectrum: formants}
    \begin{figure}
        \includegraphics[scale=.5]{graph/formants}
    \end{figure}
\end{frame}

\subsection{time envelope}
\begin{frame}{timbre}{time envelope: roughness}
    \begin{itemize}
        \item   \includeaudio{audio/zwicker-track-38.mp3}\\ amplitude modulated \unit[1]{kHz}, mod frequency \unit[70]{Hz}, degree of modulation 1,0.7,0.4,0.25,0.125,0.1,0
    \begin{figure}
        \includegraphics[scale=.5]{graph/roughness}
        \includegraphics[scale=.5]{graph/roughness2}
    \end{figure}
        \pause
        \item   Zwicker's model for roughness
            \[R \sim f_\mathrm{mod}\Delta L\]
    \end{itemize}
\end{frame}

\begin{frame}{timbre}{time envelope: ADSR 1/2}
    \hspace{50mm}\textbf{listening test}
    \vspace{-10mm}
    \begin{flushright}
        \includegraphics[scale=.25]{graph/listeningtest} 
    \end{flushright}
    \vspace{-9mm}
            \hspace{3mm}\includeaudio{audio/timbre.oboe.A5.trunc.mp3}\quad\includeaudio{audio/timbre.clarinet.A5.trunc.mp3}\quad\includeaudio{audio/timbre.violin.A5.trunc.mp3}\quad\includeaudio{audio/timbre.flute.A5.trunc.mp3}
        \begin{itemize}
            \item   4 signal snippets (A5): violin\! [1], oboe\! [2], flute\! [3], clarinet\! [4]
            \item   which is which?
            \pause
            \only<2>{
            \vspace{-5mm}
            \begin{figure}
                \includegraphics[scale=.2]{graph/transienttest}
            \end{figure}
            }
            \pause
            \only<3>{
            \vspace{-5mm}
            \begin{figure}
                \includegraphics[scale=.2]{graph/transienttest-2}
            \end{figure}
            \vspace{-50mm}
            \begin{flushright}    
                \includeaudio{audio/timbre.oboe.A5.mp3}
                
                \vspace{6mm}
                \includeaudio{audio/timbre.clarinet.A5.mp3}
                
                \vspace{6mm}
                \includeaudio{audio/timbre.violin.A5.mp3}
                
                \vspace{6mm}
                \includeaudio{audio/timbre.flute.A5.mp3}
            \end{flushright}
            }
        \end{itemize}
\end{frame}

\begin{frame}{timbre}{time envelope: ADSR 2/2}
    \begin{figure}
        \includegraphics[scale=.5]{graph/ADSR2}
    \end{figure}
\end{frame}

\begin{frame}{timbre}{time envelope: attack}
    \begin{itemize}
        \item   attack phase depends on
            \begin{itemize}
                \item   \textbf{how energy builds up} in a resonating system
                \item   \textbf{excitation}: bow, hammer, pluck
            \end{itemize}
    \end{itemize}
    \pause
    \begin{columns}
    \column{.6\textwidth}
    \begin{itemize}
        \item   different attacks change timbre sensation
        \item   believed to convey much of the source information
        \item   most important properties
            \begin{itemize}
                \item   length
                \item   inharmonic content
            \end{itemize}
    \end{itemize}
    \column{.4\textwidth}
    \begin{figure}
        \includegraphics[scale=.28]{graph/attack}
    \end{figure}
    \end{columns}
\end{frame}

\begin{frame}{timbre}{Bismarck's timbre dimensions}
    Bismarck (1974) identified 4 scales that humans use to describe timbre:
    \begin{itemize}
        \item   dull --- sharp
        \item   compact --- scattered
        \item   full --- empty
        \item   colorful --- colorless
    \end{itemize}
\end{frame}

\begin{frame}{timbre}{Grey's timbre space}
    \vspace{-7mm}
    \begin{figure}
        \includegraphics[scale=.45]{graph/grey_timbrespace}
    \end{figure}
    \vspace{-3mm}
    Main differences of flute (FL), horn (FH), oboe (O2), clarinet (C2)
\end{frame}

\begin{frame}{timbre}{McAdams' timbre space}
     \vspace{-7mm}
   \begin{figure}
        \includegraphics[scale=.35]{graph/mcadams_timbrespace}
        \includegraphics[scale=.3]{graph/mcadams_timbrespacelabels}
    \end{figure}   
\end{frame}

\begin{frame}{timbre}{summary}
    \vspace{-5mm}
    \begin{itemize}
        \item   timbre is a \textbf{multidimensional property}
        \item   its \textbf{dimensions are not completely identified}, but the following technical factors impact the timbre sensation
        \pause
        \begin{itemize}
            \item   spectral envelope
            \item   presence of inharmonic content/noise
            \item   spectral change
            \item   even/odd harmonic ratio (audio: \url{https://ccrma.stanford.edu/~malcolm/correlograms/index.html?McAdamsExample.html})
            \item   time envelope change
            \item   attack phase: length
            \item   attack and decay
        \end{itemize}
        \pause
        \item   the following features are often seen as \textbf{acoustic correlates} of timbre dimensions
            \begin{enumerate}
                \item   spectral centroid (brightness/sharpness)
                \item   (log) attack time
                \item   spectral flux (roughness)
                \item   many other features w/o clear relationship
            \end{enumerate}
        
        \smallskip
        \pause
        \item   time varying timbre and timbre of complex mixtures has not yet been studied extensively
    \end{itemize}
\end{frame}
