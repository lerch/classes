\section[tests]{testing}
\subsection{introduction}
    \begin{frame}\frametitle{testing}\framesubtitle{introduction 1/2}
        \begin{block}{\textbf{why tests} (Code Simplicity: The Fundamentals of Software)}
            \begin{center}
                \textbf{The degree to which you know how your software behaves is the degree to which you have accurately tested it.}
            \end{center}
        \end{block}
        \bigskip
        \pause
        \begin{itemize}
            \item   \textbf{law of conservation of bugs}:\\ number of bugs in a large system is proportional the the number of fixed bugs
            \item   \textbf{ripple effect}:\\ high probability that the efforts to remove defects add new defects
        \end{itemize}
    \end{frame}
    
    \begin{frame}\frametitle{testing}\framesubtitle{introduction 2/2}
        \begin{block}{hunt for defects early}
            \begin{center}
                \textbf{The earlier you identify defects, the less expensive is the fix.}
            \end{center}
            \begin{figure}
                \centering
                    \includegraphics[scale=.75]{graph/relative_cost_to_fix}
                \label{fig:fix}
            \end{figure}
        \end{block}
    \end{frame}
    
    \begin{frame}\frametitle{testing}\framesubtitle{introduction 2/2}
        \begin{figure}
            \centering
                \includegraphics[scale=.4]{graph/computer_problems}
            \label{fig:computer_problems}
        \end{figure}
    \end{frame}

\subsection{definitions}
    \begin{frame}\frametitle{testing}\framesubtitle{test types}
        \vspace{-5mm}
        \begin{itemize}
            \item   \textbf{static testing} (code is analyzed)
                \begin{itemize}
                    \item   static code analysis
                    \item   code reviews/walk-throughs/inspections
                        \begin{itemize}
                            \item   difficult API/algorithm
                            \item   inexperienced programmers
                            \item   area with catastrophic results in case of defects
                        \end{itemize}
                \end{itemize}
            \smallskip
            \pause
            \item   \textbf{dynamic testing} (program is executed)
                \begin{itemize}
                    \item   \textit{unit test}: functionality of section of code (class level) 
                    \pause
                    \item   \textit{integration test}: interaction between integrated components (interface level)
                    \pause
                    \item   \textit{system test}: overall specifications/requirements (system level)
                    \pause
                    \item   \textit{regression test}: finding defects after code changes
                    \pause
                    \item   \textit{alpha test}: simulated or actual operational testing by potential users
                    \pause
                    \item   \textit{beta test}: user acceptance testing
                    \pause
                    \item   (\textit{acceptance testing}: performed by the customer)
                \end{itemize}
        \end{itemize}
    \end{frame}

    \begin{frame}\frametitle{testing}\framesubtitle{test categories}
        \begin{itemize}
            \item   \textbf{functional testing}
                    \begin{itemize}
                        \item   \textit{verification}: have we built the software right?
                        \pause
                        \item   \textit{validation}: have we built the right software?
                     \end{itemize}  
            \smallskip
            \pause
            \item   \textbf{non-functional testing}
                \begin{itemize}
                    \item   \textit{stability testing}:
                        \begin{itemize}
                            \item   destructive testing: intention of making the software crash/misbehave --- invalid inputs
                            \item   long-term testing: run for extended periods of time
                            \item   scalability testing: how does the software behave with many users/on slow machines, what about workload peaks
                        \end{itemize}
                    \pause
                    \item   \textit{security testing}:\\ how accessible are data/transmissions/internal states
                    \pause
                    \item   \textit{usability testing}:\\ how easy to use and understand is the interface
                    \pause
                    \item   internationalization/localization
                \end{itemize}    
        \end{itemize}
    \end{frame}

    \begin{frame}\frametitle{testing}\framesubtitle{testing methods}
        \vspace{-5mm}
        \begin{itemize}
            \item   \textbf{black box}: testers outside $\rightarrow$ focus on \textit{input and output}
                \begin{itemize}
                    \item   functionality
                    \item   user input validation
                    \item   output results (numerical values)
                    \item   state transitions
                    \item   disadvantage: unknown coverage
                    
                \end{itemize}
            \smallskip
            \pause
            \item   \textbf{white box}: tester inside $\rightarrow$ focus on \textit{implementation}
                \begin{itemize}
                    \item   coverage: all different branches
                    \item   error handling
                    \item   handling of resource constraints
                \end{itemize}
            \smallskip
            \pause
            \item   \textbf{gray box}: tester can peek under the covers  $\rightarrow$ focus on \textit{architecture}
                \begin{itemize}
                    \item   protocols and transmission data
                    \item   system-added information (checksums, hashes, time stamps)
                    \item   memory leaks, temporary files, etc.
                    
                \end{itemize}
        \end{itemize}
    \end{frame}

\subsection{rules and procedures}
    \begin{frame}\frametitle{testing}\framesubtitle{rules \& procedures 1/2}
        \vspace{-5mm}
        \begin{itemize}
            \item   \textbf{specification}
                \begin{itemize}
                    \item   avoid \textit{trivial} tests
                    \pause
                    \item   keep tests \textit{short and simple}
                    \pause
                    \item   avoid inter-test \textit{dependencies}
                    \pause
                    \item   ask a very \textit{precise questions} and verify a very precise answer
                    \pause
                    \item   avoid \textit{inaccurate} tests: must not imply that software behaves properly when it is not
                \end{itemize}
            \smallskip
            \pause
            \item   \textbf{concepts}
                \begin{itemize}
                    \item   try to cover as much of your code as possible (\textbf{coverage})
                    \pause
                    \item   remember: test code at least as buggy as real code
                    \pause
                    \item   rule of thumb: ``every line of code takes 3--5 lines of test code''
                    \pause
                    \item   failing tests have highest priority
                \end{itemize}
            \smallskip
            \pause
            \item   \textbf{targets}
                \begin{itemize}
                    \item   functionality/output
                    \pause
                    \item   invalid API parameters, data, and initialization
                    \pause
                    \item   boundary and unusual cases (channel config, block lengths)
                    \pause
                    \item   race conditions: events do not happen in the anticipated order
                    \pause
                    \item   (defects in handling stress)
                \end{itemize}
         \end{itemize}
    \end{frame}

    \begin{frame}\frametitle{testing}\framesubtitle{rules \& procedures 2/2}
        \begin{block}{}
            \begin{center}
                \textbf{Successful testing is no guarantee that the system will work!}
            \end{center}
        \end{block}
    \end{frame}

    \begin{frame}\frametitle{testing}\framesubtitle{execution}
        \begin{itemize}
            \item \textbf{when}
                \begin{itemize}
                    \item   execute after each code modification
                \end{itemize}
            \bigskip
            \pause
            \item   \textbf{where}
                \begin{itemize}
                    \item   different compilers
                    \item   different hardware/OS
                    \item   different environments (e.g., plugin hosts)
                \end{itemize}
            \bigskip
            \pause
            \item   \textbf{how}
                        \begin{itemize}
                            \item   \textit{manually}
                                \begin{itemize}
                                    \item   slow 
                                    \item   error prone/incomplete
                                    \item   difficult to repeat
                                \end{itemize}
                            \pause
                            \item   \textbf{automatic}
                                \begin{itemize}
                                    \item   weekly/daily/hourly
                                    \item   continuous
                                    \item   more development effort
                                \end{itemize}
                        \end{itemize}
        \end{itemize}
    \end{frame}

\subsection{TDD}
    \begin{frame}\frametitle{testing}\framesubtitle{test-driven development}
        \vspace{-5mm}
        \begin{itemize}
            \item   \textbf{procedure}
                \begin{enumerate}
                    \item   break down the task (specification) into sub-tasks
                    \pause
                    \item   specify \& write tests for all sub-tasks
                    \pause
                    \item   ensure your tests fail
                    \pause
                    \item   write the code to make your test pass
                    \pause
                    \item   refactor
                    \pause
                    \item   task completion: you have all the tests you need, and they all pass
                \end{enumerate}
        \end{itemize}
        \pause
        \begin{block}{rules}
                \begin{enumerate}
                    \item   your test should \textbf{fail} before your implement the code
                    \item   implement the \textbf{simplest code possible} to make your tests pass
                    \item   anything beyond the tested functionality is \textbf{not important}
                \end{enumerate}
        \end{block}
        
    \end{frame}
