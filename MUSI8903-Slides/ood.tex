\section[ood]{object orientated design}
\subsection{introduction}
    \begin{frame}\frametitle{object oriented design}\framesubtitle{introduction: procedural vs. object oriented}
        \begin{itemize}
            \item   \textbf{procedural}: separate data and operations
                \begin{itemize}
                    \item   focus on the best algorithm to perform and operation
                    \item   a program is based on performing a set of operations on the data
                    \item   pass data to function and return result
                \end{itemize}
            \pause
            \bigskip
            \item   \textbf{object oriented}: encapsulate both data and operations, build hierarchy
                \begin{itemize}
                    \item   focus on relationships between sets of objects
                    \item   allow re-using code
                    \item   use object hierarchies and inheritance
                    \item   object is accessed via a simple interface
                    \item   hierarchical structure allows to inherit common properties
                \end{itemize}
        \end{itemize}
    \end{frame}
    \begin{frame}\frametitle{object oriented design}\framesubtitle{terminology}
        \begin{itemize}
            \item   \textbf{modularity}:\\ reusable objects with data and operations
            \pause
            \item   \textbf{data abstraction}:\\ define interface to access data independent of internal representation
            \pause
            \item   \textbf{procedural abstraction}:\\ focus on the actions and not how they are implemented
            \pause
            \item   \textbf{hierarchy}:\\ identify common operations/data between objects
        \end{itemize}
    \end{frame}
    
    \begin{frame}\frametitle{object oriented design}\framesubtitle{example}
        \begin{figure}
            \centering
                \includegraphics[scale=.4]{graph/ood_example.png}
            \label{fig:ood_example}
        \end{figure}
    \end{frame}
    
    \begin{frame}\frametitle{object oriented design}\framesubtitle{steps}
        \begin{enumerate}
            \item   \textbf{identify} objects with data/operations and hierarchy
            \pause
            \bigskip
            \item   \textbf{define} public interface: 
                \begin{itemize}
                    \item   should remain as constant as possible
                    \item   how to access the data
                    \item   which functions should be (pure) virtual
                    \item   base class should not include functionality that subclasses do not need (bird class with fly function)
                \end{itemize}
        \end{enumerate}
    \end{frame}
    
    \begin{frame}\frametitle{object oriented design}\framesubtitle{principles \& rules}
        \begin{block}{isa rule}
            Class A can only be a valid subclass of class B if it makes sense to say, ``an A is a B''
        \end{block}
        \pause
        \begin{block}{single responsibility principle}
            The \textit{class name} \textit{method name} itself.
        \end{block}
            \pause
            example:
            \begin{itemize}
                \item   The car starts itself. (+)
                \pause
                \item   The car drives itself. (--)
            \end{itemize}
        \pause
        \begin{block}{dry: don't repeat yourself}
            Each piece of information and behavior is in a \textbf{single, sensible place}.
        \end{block}
    \end{frame}
    
\subsection{design patterns}    
    \begin{frame}\frametitle{object oriented design}\framesubtitle{design patterns 1/2}
        \begin{itemize}
            \item   \textbf{containers}:\\ object that holds other objects (e.g., an array)
            %\item   iterators: means by which objects in a container can be accessed one at a time (iterators share the same interface)
            \smallskip\pause
            \item   \textbf{visitor}:\\ \url{https://stackoverflow.com/questions/255214/when-should-i-use-the-visitor-design-pattern}
            \smallskip\pause
            \item   \textbf{adapters}:\\ converts the interface of one class into the interface expected by the user
            \smallskip\pause
            \item   \textbf{facade/wrapper}:\\ provides a simplified interface to multiple classes
        \end{itemize}
    \end{frame}
    
    \begin{frame}\frametitle{object oriented design}\framesubtitle{design patterns 2/2}
        \begin{itemize}
            \item   \textbf{singleton}:\\ a class of which there is only one instance
                \begin{itemize}
                    \item   controls concurrent access to a shared resource
                    \item   access to the resource will be requested from multiple, disparate parts of the system
                    \item   there can be only one object
                \end{itemize}
                \pause
                \lstinputlisting[language=C++]{src/singleton.cpp}
        \end{itemize}
    \end{frame}
    
\subsection{polymorphism}    
    \begin{frame}\frametitle{object oriented design}\framesubtitle{polymorphism}
        \textbf{polymorphism}: 
        
        code or operations or objects behave differently in different contexts
        \bigskip
        \lstinputlisting[language=C++]{src/polymorphism.cpp}
    \end{frame}
    
    \begin{frame}\frametitle{object oriented design}\framesubtitle{polymorphism example: employee (base) \& manager 1/3}
       
        \lstinputlisting[language=C++]{src/polymorphism_example.cpp}    
    \end{frame}
    
    \begin{frame}\frametitle{object oriented design}\framesubtitle{polymorphism example: employee (base) \& manager 2/3}
        \begin{itemize}
            \item   allows to use base class pointer
            \lstinputlisting[language=C++]{src/polymorphism_example_1.cpp}
            \pause
            \bigskip
            \item   but: 
            \lstinputlisting[language=C++]{src/polymorphism_example_2.cpp}
        \end{itemize}
    \end{frame}
    
    \begin{frame}\frametitle{object oriented design}\framesubtitle{polymorphism example: employee (base) \& manager 3/3}
        \begin{itemize}
            \item   use keyword \textbf{virtual} in base class
            \pause
            \item   pitfalls:
            \lstinputlisting[language=C++]{src/polymorphism_example_3.cpp}
            \pause
            \item[$\Rightarrow$] use identifier \textbf{override}
            \pause
            \item   difference between \textit{overriding} and \textit{overloading}?
        \end{itemize}
    \end{frame}

    \begin{frame}\frametitle{object oriented design}\framesubtitle{side note: copy constructor}
        \begin{itemize}
            \item   compiler generates copy constructor which copies members
            \pause  
            \item [$\rightarrow$]  allocated buffer content will not be copied, only pointer address
            \pause
            \item [$\rightarrow$]  either make your class uncopyable or explicitly define copy semantics
        \end{itemize}
        \only<4>{\lstinputlisting[language=C++]{src/copy_constructor_1.cpp}}
        \only<5>{\lstinputlisting[language=C++]{src/copy_constructor_2.cpp}}
        \vspace{50mm}
    \end{frame}

    \begin{frame}\frametitle{object oriented design}\framesubtitle{polymorphism: operator overloading}
        \lstinputlisting[language=C++]{src/operator_overloading.cpp}
        \pause
        \begin{itemize}
            \item   operator definition is global, otherwise first operand has to be of class type
            \pause  
            \item   since operator definition is global, we have to make it a friend to allow access to private members
        \end{itemize}
    \end{frame}

    \begin{frame}\frametitle{object oriented design}\framesubtitle{operator overloading example: Scoped Pointer (RAII)}
        \vspace{-3mm}\begin{tiny}\lstinputlisting[language=C++]{src/scopedpointer.cpp}\end{tiny}
    \end{frame}

    \begin{frame}\frametitle{object oriented design}\framesubtitle{abstract classes}
        \begin{itemize}
            \item   any class with one or more pure virtual functions is an \textbf{abstract base class}
            \pause  
            \bigskip
            \item   a \textbf{pure abstract base class} or an \textbf{interface} contains only pure virtual functions
            \pause
            \bigskip
            \item   cannot create an object of an abstract class type
            \item   but: pointers and references to an abstract class
            \bigskip
            \item   decide carefully what functions will be virtual or not
        \end{itemize}
    \end{frame}
