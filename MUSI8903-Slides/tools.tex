\section[tools]{development tools}
\begin{frame}\frametitle{development tools}\framesubtitle{intro}
    \begin{figure}
        \centering
            \includegraphics[scale=.4]{graph/real_programmers}
    \end{figure}
\end{frame}

\subsection{debugger}
\begin{frame}{development tools}{debugger --- introduction}
    \vspace{-3mm}
    \begin{itemize}
        \item   layers of bug finding
            \begin{enumerate}
                \item   compiler (look at the warnings)
                \item   (linker)
                \item   asserts
                \item   \only<2->{\textbf}{debugging}
                \item   logs/print-outs
                \item   static code analysis
                \item   testing
            \end{enumerate}
        \pause
        \bigskip
        \item   what is a debugger?
            \begin{itemize}
                \item   tool to examine program code/states \textbf{at runtime}
                \item   allows to pause program execution 
            \end{itemize}
        \pause
        \item   when to use a debugger?
            \begin{itemize}
                \item   \textbf{ALWAYS!} --- debugging will take half of your development time
                    \begin{itemize}
                        \item   after code compiles and links ($\rightarrow$ compare with Matlab)
                        \item   in case of any problem (wrong output, crash, \ldots)
                    \end{itemize}
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{development tools}{debugger --- breakpoints}
    \begin{itemize}
        \item   \textbf{pause the code execution} at an arbitrary point
        \item   \textbf{monitor variables}, buffers, \& function return values at runtime
        \item   \textbf{step through code}, into functions, \ldots
        \item   \textbf{modify variables}
    \end{itemize}
\end{frame}

\subsection{version control}
\begin{frame}{development tools}{version control --- introduction}
    \begin{itemize}
        \item   manage changes to a project without overwriting anything
        \bigskip
        \item   allows you to
            \begin{itemize}
                \item   \textbf{keep track} of modifications through logs
                \item   \textbf{review changes} by date and developer
                \item   \textbf{revert} files to a previous state
                \item   work on the same file \textbf{in a team}
                \item   \textbf{implement alternative versions }(branch)
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{development tools}{version control --- usage}
    \begin{itemize}
        \item   commit \textbf{frequently}
        \item   document your changes \textbf{thoroughly}
        \item   always \textbf{commit compiling code} --- never check-in code that breaks the project
        \item   keep it \textbf{clean} --- don't commit temporary files
        \bigskip
        \item   always \textbf{pull the latest revision} before starting to work
        \item   use the log to \textbf{review changes} of other team members
    \end{itemize}
\end{frame}

\begin{frame}{development tools}{version control --- concepts}
    \begin{columns}
    \column{.6\linewidth}
    \begin{enumerate}
        \item<1->   \only<1>{\textbf}{local} version control systems
        \item<2->   \only<2>{\textbf}{centralized} version control systems
        \item<3->   \only<3>{\textbf}{distributed} version control systems
    \end{enumerate}
    \column{.4\linewidth}
	\setcounter{i}{1}
	\whiledo{\value{i}<4}	
	{
		\only<\value{beamerpauses}>
		{
			\begin{figure}
			\centering
                \includegraphics[scale=.3]{graph/git010\arabic{i}-tn.png}
			\end{figure}
		}
		\ifthenelse{\value{i}<3}{\pause}{}
		\stepcounter{i} 
	}	
    \end{columns}
\end{frame}

\begin{frame}{development tools}{version control --- concepts}
    \vspace{-3mm}
    \begin{figure}
        \centering
        \includegraphics[scale=.45]{graph/git}
    \end{figure}
\end{frame}

\subsection{issue tracking}

\begin{frame}{development tools}{issue tracking --- introduction}
    \vspace{-3mm}
    \begin{itemize}
        \item   use issue tracking to
            \begin{itemize}
                \item   record and communicate \textbf{priorities}
                \item   keep track of \textbf{comments, discussions, decisions}
                \item   view \textbf{project status}
                \item   \textbf{analyze project state} (number/origin of bugs, open bugs vs. closed bugs)
            \end{itemize}
        \pause
        \item   process of issue tracking
            \begin{enumerate}
                \item   \textbf{identify} a bug/issue/feature
                \item   \textbf{submit}
                    \begin{itemize}
                        \item   summary, expectation vs. system behavior
                        \item   how to reproduce
                        \item   severity and priority
                    \end{itemize}
                \item   \textbf{assign} responsibility
                \item   create test to \textbf{reproduce}
                \item   \textbf{fix}
                \item   assign tester and \textbf{confirm}
                \item   \textbf{close}
            \end{enumerate}
    \end{itemize}
\end{frame}

\subsection{project generation}
\begin{frame}{development tools}{CMake --- introduction}
    \begin{itemize}
        \item   \textbf{project configuration} in script file
        \smallskip
        \item<2->   project/build file generation
            \begin{itemize}
                \item   automatic configuration (compiler, OS, CPU), compilation, linking (local and system libs)
                \item   platform independent (XCode, Visual Studio, Eclipse, \ldots)
                \item   automated testing
                \item   automated distribution (installer, zip)
            \end{itemize}
        \bigskip
        \item<3->    minimal CMake project:\\
            \begin{footnotesize}
            \begin{texttt}
                project (Test)\\
                add\_executable(Test TestMain.cpp)
            \end{texttt}
            \end{footnotesize}
    \end{itemize}
\end{frame}


\subsection{static code analysis}
\begin{frame}{development tools}{static code analysis}
    \begin{itemize}
        \item   identify possible vulnerabilities
        \item   look beyond 'legal' C++
        \item   analyze all code paths
        \item   frequent false positives
        \item   unable to identify configuration problems
    \end{itemize}
\end{frame}

\subsection{profiling}
\begin{frame}{development tools}{profiling}
    \begin{itemize}
        \item   tool for dynamic program analysis
        \item   measure the complexity of particular instructions/functions
        \pause
        \item   approaches:
            \begin{itemize}
                \item   sampling
                \item   instrumentation
            \end{itemize}
        \pause
        \item   examples
            \begin{itemize}
                \item   Visual Studio/XCode
                \item   valgrind
                \item   Intel VTune
                \item   Shark
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{development tools}{refactoring}
    \begin{itemize}
        \item   \textbf{uses}
            \begin{itemize}
                \item   rename variables/functions
                \item   move code between header and source file
                \item   extract methods from code
                \item   code generation wizards
            \end{itemize}
        \bigskip
        \pause
        \item   \textbf{strategies}
            \begin{itemize}
                \item   don't hesitate to refactor when it makes code more readable
                \item   hesitate in interfaces --- does it break compatibility?
                \item   don't add/fix code at the same time --- commit refactor-only
                \item   have (regression) tests to verify that functionality is not changed by refactoring
            \end{itemize}
        
    \end{itemize}
\end{frame}

\subsection{documentation}
\begin{frame}{development tools}{comments 1/2}
    \begin{itemize}
        \item   \textbf{programming style as documentation}
            \begin{itemize}
                \item   good program structure
                \item   easily understandable approaches
                \item   good variable/function/class names
                \item   name constants instead of literals
                \item   clear layout
            \end{itemize}
            \only<1>{\lstinputlisting[language=C++]{src/comment.cpp}}
        \pause
        \bigskip
        \item   \textbf{comment categories}
            \begin{itemize}
                \item   repeat of code (does not give additional information)
                \item   explanation of code (means that the code is complicated)
                \item   marker in the code, to do, etc. (means the code is not finished)
                \item   \only<3->{\textbf}{summary of the code (allows quick navigation)}
                \item   \only<3->{\textbf}{description of code intent (most helpful)}
            \end{itemize}
    \end{itemize}
    \vspace{50mm}
\end{frame}
\begin{frame}{development tools}{comments 2/2}
    \begin{itemize}
        \item   \textbf{problems} with in-code comments
            \begin{itemize}
                \item   comments \textit{repeat} the code in a language less precise than programming language
                \item   comments \textit{get out-of-date} as the code changes
                \item   comments \textit{increase source size} $\rightarrow$ more to read
                \item   comments of 'tricky' code \textit{require} the user to read it
                \pause
                \bigskip
                \item[$\Rightarrow$]   \textbf{not done right, comments are worse than useless}
            \end{itemize}
        \bigskip
        \item   comments \textbf{done right}
            \begin{itemize}
                \item   ease maintainability
                \item   allow more efficient collaboration
            \end{itemize}
    \end{itemize}
    \vspace{50mm}
\end{frame}
\begin{frame}{development tools}{comments strategies \& techniques}
    \begin{itemize}
        \item   \textbf{commenting strategies}
            \begin{itemize}
                \item   use comment pseudo-code as an outline before you write the code
                \item   comment (and update comments) when you write the code, not when you are 'finished'
            \end{itemize}
        \bigskip
        \item   \textbf{commenting techniques}
            \begin{itemize}
                \item   individual lines
                    \begin{itemize}
                        \item   only if line is very complicated, or a fix is documented
                    \end{itemize}
                \pause
                \item   endline comments
                    \begin{itemize}
                        \item   hard to format/maintain, good, e.g., for variable declarations
                    \end{itemize}
                \pause
                \item   paragraph comments
                    \begin{itemize}
                        \item   intent level
                    \end{itemize}
            \end{itemize}
    \end{itemize}
\end{frame}
\begin{frame}{development tools}{comments}
    \begin{figure}%
        \includegraphics[scale=.4]{future_self}%
    \end{figure}
\end{frame}
\begin{frame}{development tools}{documentation}
    \begin{itemize}
        \item   don't overrate automatic code documentation
        \pause
        \item   doxygen example
            \lstinputlisting[language=C++]{src/doxygen.cpp}
    \end{itemize}

\end{frame}
