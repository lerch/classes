% move all configuration stuff into includes file so we can focus on the content
\input{include/common}
\input{include/titledef}
\input{include/definitions}


\subtitle{Real-time systems and audio}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
    % generate title page
	\input{include/titlepage}



\section{definition}
    \begin{frame}{real-time}{definition}
        \question{what is a real-time system}

        \bigskip
        \bigskip
		\begin{block}{real-time system: definition}
			``A computer system that responds to input signals fast enough to keep an operation moving at its required speed''
		\end{block}
    \end{frame}
\section{considerations}
    \begin{frame}{real-time}{audio 1/3}
        \begin{itemize}
            \item   digital audio \textbf{playback}:\\ constant stream of audio samples to your sound device 
                \begin{itemize}
                    \item   the time to output one sample must not exceed $\nicefrac{1}{f_\mathrm{S}}$
                    \item   otherwise: drop-out/glitch
                \end{itemize}
            \smallskip
            \item   digital audio \textbf{recording}:\\ constant stream of audio samples from your sound device 
                \begin{itemize}
                    \item   the time to capture one sample must not exceed $\nicefrac{1}{f_\mathrm{S}}$
                    \item   otherwise: drop-out/glitch
                \end{itemize}
            \bigskip
            \pause
            \item[$\Rightarrow$] example: time available to process one sample
                \begin{equation*}
                    \frac{1}{\unit[44100]{Hz}} = \unit[22.7]{\mu s}
                \end{equation*}
        \end{itemize}
    \end{frame}
    \begin{frame}{real-time}{audio 2/3}
         \begin{itemize}
            \item   it is usually too inefficient to operate sample by sample
            \item<2->[$\Rightarrow$] operating systems will work with \textbf{blocks of audio samples}
            \bigskip
            \item<3->[$\Rightarrow$] time available to process $N$ samples
                \begin{equation*}
                    \frac{N}{\unit[44100]{Hz}} = N\cdot\unit[22.7]{\mu s}
                \end{equation*}
            \item<3-> example: $N=1024$
                \begin{equation*}
                    \frac{1024}{\unit[44100]{Hz}} = \unit[23.2]{ms}
                \end{equation*}
            \smallskip
            \item<4->   this time is the \textit{theoretical maximum} time available per block and is a hard requirement, the \textit{realistic} available time is lower, due to
                \begin{itemize}
                    \item   other processes/threads running
                    \item   timing variations in delivery of blocks of samples
                \end{itemize}
        \end{itemize}
    \end{frame}
    \begin{frame}{real-time}{audio 3/3}
        \begin{itemize}
            \bigskip
            \item   no real-time system can take into account sample values that are yet to come/that it has not seen yet
            \bigskip
            \item   only past samples can be used for processing/parameter adjustment
        \end{itemize}
    \end{frame}
	\begin{frame}{real-time systems}{properties}
        \begin{itemize}
			\item	\textbf{performance}:
				\begin{itemize}
					\item	processing time for one block $\leq$ block length!
				\end{itemize}
			\bigskip
			\item<2->	\textbf{latency}:
				\begin{itemize}
					\item	delay of a system between the stimulus and the response to this stimulus
						\begin{itemize}
							\item	\textit{algorithmic delay}: (FFT-Processing, Look-Ahead, \ldots)
							\item	\textit{interface delay}: (OS block length, ad/da conversion)
                            \item   what are acceptable latencies in what scenarios?
						\end{itemize}
				\end{itemize}
			\bigskip
			\item<3->	\textbf{causality}:
				\begin{itemize}
					\item	\textit{no} knowledge of future samples
				\end{itemize}
		\end{itemize}
	\end{frame}


\section{examples}
    \begin{frame}{real-time systems}{examples}
        \question{Which of the following systems are real-time}
        
        \begin{itemize}
            \item   \textbf{audio file normalization}
                \begin{itemize}
                    \item   functionality: divide each sample by the absolute maximum
                    \item   workload: $\leq \unit[0.01]{\mu s}$ per sample\footnote{all these numbers are not measured, just uninformed guesses}
                \end{itemize}
            \item<2->   \textbf{equalization}
                \begin{itemize}
                    \item   functionality: apply single-pole low pass filter to audio stream
                    \item   workload: $\leq \unit[0.1]{\mu s}$ per sample
                \end{itemize}
            \item<3->   \textbf{pitch-shifting}
                \begin{itemize}
                    \item   functionality: change the pitch of a sound without changing the tempo
                    \item   workload: $\leq \unit[10]{\mu s}$ per sample
                \end{itemize}
            \item<4->   \textbf{time-compression}
                \begin{itemize}
                    \item   functionality: increase the tempo of a sound without changing the pitch
                    \item   workload: $\leq \unit[10]{\mu s}$ per sample
                \end{itemize}
        \end{itemize}
 	\end{frame}

\section{audio plugins}
    \begin{frame}{real-time systems}{audio plugins}
        \begin{itemize}
            \item   standard\textbf{ audio plugins are real-time} systems
                \begin{itemize}
                    \item   no file access, no understand of future values
                    \item   host (DAW) calls to deliver a block of samples, plugin has to deliver and output block of the same size
                \end{itemize}
            \smallskip
            \item<2->   \textbf{challenge}:
                \begin{itemize}
                    \item   host is allowed to change block size on the fly. Typical range: $32$--$4096$
                    \item[$\Rightarrow$]   if plugin uses internal block size (e.g., FFT processing), buffering needs to be implemented (increasing latency!)
                    \bigskip
                    \item   WHY?
                \end{itemize}
        \end{itemize}
 	\end{frame}

    \begin{frame}{real-time systems}{audio glitches}
        typical sources of audio glitches:
        \begin{itemize}
            \item   \textbf{any operating system call with unbounded execution time} (file IO, network, memory allocation, etc.) in the audio thread can break real-time requirements
            \smallskip
            \item   polling too frequently for user input, updating parameters \textbf{synchronously with user input} (GUI thread impacts audio thread)
            \smallskip
            \item   \textbf{blocked execution} (mutex, semaphores, waiting for another thread, waiting for disk reading, waiting for network responses)
            \smallskip
            \item   \textbf{estimating average workload instead of peak workload}, especially in case of blocked processing and/or iterative algorithms
                \begin{itemize}
                    \item   imagine i/o block size $32$ but the plugin does FFT processing with blocks of 4096: what happens?
                \end{itemize}
        \end{itemize}
  	\end{frame}

    \begin{frame}{real-time systems}{implementation}
        \begin{itemize}
            \item   audio driver periodically sends/requests audio buffer
            \item[$\Rightarrow$] needs to be delivered in the given time constraints, no exception!
            \bigskip
            \item   Why not push samples at regular intervals to the sound card?
            \item<2->[$\Rightarrow$] if you push blocks of samples (e.g., every \unit[23.2]{ms}), your time measurement will diverge from the sample clock, leading to buffer overflows and underflows
            \item<2->[$\Rightarrow$]  sample clock of sound hardware\textbf{ has to be master}
            \bigskip
            \item<3->   file level playback functions are useless for anything timing-related, e.g., you cannot properly mix two sounds by starting playing back two files (WHY?)
            
        \end{itemize}
        
  	\end{frame}

    \begin{frame}{real-time systems}{online resources}
        \begin{itemize}
            \item \href{http://www.rossbencina.com/code/real-time-audio-programming-101-time-waits-for-nothing}{http://www.rossbencina.com/code/real-time-audio-programming-101-time-waits-for-nothing}
            \item \href{https://www.quora.com/What-is-a-callback-in-driver-programming}{https://www.quora.com/What-is-a-callback-in-driver-programming}
        \end{itemize}
  	\end{frame}


\end{document}
